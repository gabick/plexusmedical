<?php
require_once( 'code/services-shortcode.php' );
require_once( 'code/added-social-icons.php' );
require_once( 'includes/admin-login/admin-login.php' );
function my_enqueue_assets() {
    wp_enqueue_style( 'parent-main', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'gabick-main', '/pub/build/styles/main.css' );
    wp_enqueue_style( 'gabick-copyright', '/pub/build/styles/copyright.css' );
    wp_enqueue_style( 'gabick-mobile', '/pub/build/styles/better-mobile-menu.css' );
    wp_enqueue_style( 'gabick-modules', '/pub/build/styles/modules.css' );
//    wp_enqueue_style( 'mobile-slide-menu', get_stylesheet_directory_uri().'/includes/mobile-menu-slide./css/mobile-menu-slide.css' );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_assets' );

function my_scripts_method()
{
    wp_enqueue_script(
        'nwayo-dependencies',
        '/pub/build/scripts/dependencies-head-sync.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'nwayo-dependencies-sync',
        '/pub/build/scripts/dependencies.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'nwayo-main',
        '/pub/build/scripts/main.js',
        array('jquery'),
        true
    );
    wp_enqueue_script(
        'tedsadsad',
        '/pub/build/scripts/better-mobile-menu.js',
        array('jquery'),
        true
    );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_method', 15 );

function ds_tutorial_links_editor($wp_customize) {

    $wp_customize->add_setting( 'et_divi[header_address]', array(
        'type'			=> 'option',
        'capability'	=> 'edit_theme_options',
        'transport'		=> 'postMessage',
        'sanitize_callback' => 'et_sanitize_html_input_text',
        'priority' => 100
    ) );

    $wp_customize->add_control( 'et_divi[header_address]', array(
        'label'		=> esc_html__( 'Address', 'Divi' ),
        'section'	=> 'et_divi_header_information',
        'type'      => 'text',
        'priority' => 100
    ) );
}

add_action('customize_register', 'ds_tutorial_links_editor');

/* ------- Line Break Shortcode --------*/
function line_break_shortcode() {
    return '<br />';
}
add_shortcode( 'br', 'line_break_shortcode' );

// Contact form 7 Email Configuration Validator
add_filter( 'wpcf7_validate_configuration', '__return_false' );

// Add specific CSS class by filter
add_filter('body_class','my_class_names');
function my_class_names($classes) {
    // add 'class-name' to the $classes array
    if(ICL_LANGUAGE_CODE == 'en'){
        $classes[] = 'wpml-en';
    }elseif(ICL_LANGUAGE_CODE == 'fr'){
        $classes[] = 'wpml-fr';
    }
    // return the $classes array
    return $classes;
}

?>
