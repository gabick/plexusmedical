<?php

function serviceListShortcode() {
    global $post;

    $html = "";

    $my_query = new WP_Query( array(
        'post_type' => 'services',
        'posts_per_page' => 12
    ));
    if( $my_query->have_posts()) :
        $html .= "<div class='flex-container'>";
        while( $my_query->have_posts() ) : $my_query->the_post();
            $image = get_field('icone');
            $fontAwesomeIcon = get_field('icone_font_awesome_select');

            $html .= "<div class='service-items'>";
            if(!empty($image)) {
                $html .= '<div class="service-icone">';
                $html .= "<img src=".$image['url']." alt=".$image['alt']. "/>";
                $html .= '</div>';
            }
            else if (!empty($fontAwesomeIcon)) {
                $html .= '<div class="service-icone">';
                $html .= "<span class=\"fa-stack fa-2x\"><i class=\"fas fa-circle fa-stack-2x\"></i><i class=\"fas fa-flag fa-stack-1x fa-inverse\"></i></span>";
                $html .= '</div>';
            }
            $html .= "<div class='service-text-container'>";
            $html .= "<h3 class='service-title'>" . get_the_title() . " </h3>";
            $html .= "<p class='service-text'>" . get_the_excerpt() . " </p>";
            $html .= "</div>"; // service-text-container
            $html .= "</div>"; // service-itemsx
        endwhile;
        wp_reset_postdata();
        $html .= "</div>";
    endif;

    return $html;
}

function register_shortcodes(){
    add_shortcode('ServicesList', 'serviceListShortcode');
}
add_action( 'init', 'register_shortcodes');
