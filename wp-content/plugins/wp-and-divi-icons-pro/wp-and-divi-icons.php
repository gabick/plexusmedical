<?php
/**
 * Plugin Name: WP and Divi Icons Pro
 * Description: Adds 2600+ new icons to the WordPress editor and the Divi & Extra framework, helping you build standout WordPress web designs.
 * Version: 1.1.5
 * Author: Aspen Grove Studios
 * Author URI: https://aspengrovestudios.com/?utm_source=wp-and-divi-icons-pro&utm_medium=plugin-credit-link&utm_content=plugin-file-author-uri
 * License: GNU General Public License version 3
 * License URI: https://www.gnu.org/licenses/gpl.html
 */
 
/*

WP and Divi Icons plugin - copyright 2019 Aspen Grove Studios - see license.txt

========

Credits:

This plugin includes code copied from and based on parts of the Divi theme and/or the
Divi Builder, copyright Elegant Themes, licensed GPLv2. Used in this project under GPLv3 by
special permission to Aspen Grove Studios.

This plugin includes code copied from and based on parts of WordPress, copyright 2011-2019
by the contributors, licensed GPLv2+ (see license.txt file in the plugin's root directory
for GPLv3).

This plugin includes code copied from and based on parts of TinyMCE, copyright
Ephox Corporation, licensed by this project under the GPLv3 (see license.txt file
in the plugin's root directory).

This plugin includes the jquery-base64 library; see copyright and
licensing info in js/fallback.js.

This plugin includes the Modernizr library; see copyright and licensing
info in js/icons.js.

This plugin includes the phpColors library; see licensing info in
vendor/phpColors/README.md.

=======

Note:

Divi is a registered trademark of Elegant Themes, Inc. This product is
not affiliated with nor endorsed by Elegant Themes.

*/

class AGS_Divi_Icons {
	// Following constants must be HTML safe
	//const PLUGIN_NAME = 'Divi Icon Expansion Pack';
	const PLUGIN_NAME = 'WP and Divi Icons';
	const PLUGIN_SLUG = 'wp-and-divi-icons-pro';
	const PLUGIN_AUTHOR = 'Aspen Grove Studios';
	const PLUGIN_AUTHOR_URL = 'https://divi.space/';
//	const PLUGIN_AUTHOR_URL = 'https://aspengrovestudios.com/';
	const PLUGIN_VERSION = '1.1.5';
	const WPORG = false;
	
	public static $pluginFile, $pluginDir, $pluginDirUrl;
	
	public static function init() {
		self::$pluginFile = __FILE__;
		self::$pluginDir = dirname(__FILE__).'/';
		self::$pluginDirUrl = plugins_url('', __FILE__);
		
		@include(self::$pluginDir.'updater/updater.php');
		
		$actionClassName = defined(AGS_DIVI_ICONS_PRO) ? 'AGS_Divi_Icons' : 'AGS_Divi_Icons_Pro';
		
		add_action('admin_menu', array('AGS_Divi_Icons', 'adminMenu'), 11);
		add_action('load-plugins.php', array('AGS_Divi_Icons', 'onLoadPluginsPhp'));
		add_action('admin_enqueue_scripts', array('AGS_Divi_Icons', 'adminScripts'));
		add_action('wp_ajax_agsdi_get_icons', array('AGS_Divi_Icons', 'getOrderedIconsAjax'));
		
		$isAdmin = is_admin();
		if ($isAdmin) {
			$settings = get_option('agsdi-icon-expansion');
		}
		
		if (self::WPORG || ds_icon_expansion_has_license_key()) {
			
			add_action('et_fb_framework_loaded', array('AGS_Divi_Icons', 'adminScripts'));
			add_filter('et_pb_font_icon_symbols', array('AGS_Divi_Icons', 'addIcons'));
			add_filter('mce_external_plugins', array('AGS_Divi_Icons', 'mcePlugins'));
			add_filter('mce_buttons', array('AGS_Divi_Icons', 'mceButtons'));
			add_filter('mce_css', array($actionClassName, 'mceStyles'));
			
			wp_enqueue_style('ags-divi-icons', self::$pluginDirUrl.'/css/icons.css', array(), self::PLUGIN_VERSION);
			wp_enqueue_script('ags-divi-icons', self::$pluginDirUrl.'/js/icons.js', array('jquery'), self::PLUGIN_VERSION);
			
			$ags_divi_icons_config = array(
				'pluginDirUrl' => self::$pluginDirUrl
			);
			
			if (!empty($_GET['et_fb'])) { // in frontend builder
				wp_enqueue_script('ags-divi-icons-editor', self::$pluginDirUrl.'/js/tinymce-plugin.js', array('jquery', 'react-tiny-mce'), self::PLUGIN_VERSION); // redid second dependency 2019-10-11, good
				$ags_divi_icons_config['mceStyles'] = $actionClassName::mceStyles('');
			}
			
			
			wp_enqueue_style('ags-divi-icons-fontawesome', self::$pluginDirUrl.'/icon-packs/fontawesome/css/all-agsdi.min.css', array(), '5.2.0');
			wp_localize_script('ags-divi-icons', 'ags_divi_icons_config', $ags_divi_icons_config);
			
			// Review notice code from Donations for WooCommerce plugin
			if ($isAdmin && empty($settings['notice_instruction_hidden'])) {
				add_action('admin_notices', array('AGS_Divi_Icons', 'instructionNotice'));
			}
			add_action('wp_ajax_ds-icon-expansion_instruction_notice_hide', array('AGS_Divi_Icons', 'instructionNoticeHide'));
	
		} else {
			add_action('admin_notices', array('AGS_Divi_Icons', 'activateNotice'));
		}
		
		
		if (self::WPORG) {
			// Review notice code from Donations for WooCommerce plugin
			if ($isAdmin && empty($settings['notice_review_hidden']) && time() - get_option('ds-icon-expansion_first_activate') >= (14*86400)) {
				// Temporarily disabled:
				// add_action('admin_notices', array('AGS_Divi_Icons', 'reviewNotice'));
			}
			add_action('wp_ajax_ds-icon-expansion_review_notice_hide', array('AGS_Divi_Icons', 'reviewNoticeHide'));
		}
	}
	
	public static function adminMenu() {
		add_submenu_page('admin.php', self::PLUGIN_NAME, self::PLUGIN_NAME,
							'install_plugins', 'ds-icon-expansion', array('AGS_Divi_Icons', 'adminPage'));
		add_submenu_page('et_divi_options', self::PLUGIN_NAME, self::PLUGIN_NAME,
							'install_plugins', 'ds-icon-expansion', array('AGS_Divi_Icons', 'adminPage'));
		add_submenu_page('et_extra_options', self::PLUGIN_NAME, self::PLUGIN_NAME,
							'install_plugins', 'ds-icon-expansion', array('AGS_Divi_Icons', 'adminPage'));
	}
	
	public static function adminPage() {
		include(self::$pluginDir.'admin/admin.php');
	}

	// Add settings link on plugin page
	public static function pluginActionLinks($links) {
	  array_unshift($links, '<a href="admin.php?page=ds-icon-expansion">'.(self::WPORG || ds_icon_expansion_has_license_key() ? 'Instructions' : 'Activate License Key').'</a>'); 
	  return $links;
	}

	public static function onLoadPluginsPhp() {
		$plugin = plugin_basename(__FILE__); 
		add_filter('plugin_action_links_'.$plugin, array('AGS_Divi_Icons', 'pluginActionLinks'));
	}

	public static function activateNotice() {
		echo('<div class="notice notice-warning"><p>'
				// translators: the first two %s substitutions are opening and closing <strong> tags; the second two %s substitutions are opening and closing <a> tags to link the "activate your license key" text
				.sprintf(esc_html__('You haven\'t activated your %s license key yet. Please %sactivate your license key%s to use over 2600 icons in the WordPress editor and your Divi modules!'),
					'<strong>'.self::PLUGIN_NAME.'</strong>',
					'<a href="'.esc_url(admin_url('admin.php?page=ds-icon-expansion')).'">',
					'</a>'
				)
			.'</p></div>');
	}
	
	public static function addIcons($existingIcons) {
		return array_merge($existingIcons, self::getOrderedIcons());
	}
	
	public static function getOrderedIconsAjax() {
		wp_send_json_success(self::getOrderedIcons());
	}
	
	public static function getOrderedIcons() {
		$icons = self::getIcons();
		$proIcons = self::getProIcons();
		return array_merge($icons, $proIcons);
	}
	
	public static function getIcons() {
		return array(
			'agsdi-aspengrovestudios','agsdi-wpgears','agsdi-message','agsdi-location',
			'agsdi-message-2','agsdi-message-3','agsdi-mail','agsdi-gear','agsdi-zoom',
			'agsdi-zoom-in','agsdi-zoom-out','agsdi-time','agsdi-wallet','agsdi-world',
			'agsdi-bulb','agsdi-bulb-flash','agsdi-bulb-options','agsdi-calendar','agsdi-chat',
			'agsdi-music','agsdi-video','agsdi-security-camera','agsdi-sound','agsdi-music-play',
			'agsdi-video-play','agsdi-microphone','agsdi-cd','agsdi-coffee','agsdi-gift',
			'agsdi-printer','agsdi-hand-watch','agsdi-alarm','agsdi-alarm-2',
			'agsdi-calendar-check','agsdi-code','agsdi-learn','agsdi-globe',
			'agsdi-warning','agsdi-cancel','agsdi-question','agsdi-error','agsdi-check-circle',
			'agsdi-arrow-left-circle','agsdi-arrow-right-circle','agsdi-arrow-up-circle',
			'agsdi-arrow-down-circle','agsdi-refresh','agsdi-share','agsdi-tag',
			'agsdi-bookmark','agsdi-bookmark-star','agsdi-briefcase','agsdi-calculator',
			'agsdi-id-card','agsdi-credit-card','agsdi-shop','agsdi-tshirt','agsdi-handbag',
			'agsdi-clothing-handbag','agsdi-analysis','agsdi-chat-gear','agsdi-certificate','agsdi-medal',
			'agsdi-ribbon','agsdi-star','agsdi-bullhorn','agsdi-target','agsdi-pie-chart',
			'agsdi-bar-chart','agsdi-bar-chart-2','agsdi-bar-chart-3','agsdi-bar-chart-4','agsdi-bar-chart-5',
			'agsdi-income','agsdi-piggy-bank','agsdi-bitcoin','agsdi-bitcoin-circle',
			'agsdi-bitcoin-mining','agsdi-mining','agsdi-dollar','agsdi-dollar-circle',
			'agsdi-dollar-bill','agsdi-binders','agsdi-house','agsdi-padlock','agsdi-padlock-open',
			'agsdi-house-padlock','agsdi-cloud-padlock','agsdi-key','agsdi-keys','agsdi-eye',
			'agsdi-eye-closed','agsdi-champagne','agsdi-rocket','agsdi-rocket-2','agsdi-rocket-3',
			'agsdi-flag','agsdi-flag-2','agsdi-flag-3','agsdi-drop','agsdi-sun',
			'agsdi-sun-cloud','agsdi-thermometer','agsdi-celsius','agsdi-sun-2','agsdi-cloud',
			'agsdi-upload','agsdi-cloud-computing','agsdi-cloud-download','agsdi-cloud-check',
			'agsdi-cursor','agsdi-mobile','agsdi-monitor','agsdi-browser','agsdi-laptop',
			'agsdi-hamburger-menu','agsdi-hamburger-menu-circle','agsdi-download','agsdi-image',
			'agsdi-file','agsdi-file-error','agsdi-file-add','agsdi-file-check',
			'agsdi-file-download','agsdi-file-question','agsdi-file-cursor','agsdi-file-padlock',
			'agsdi-file-heart','agsdi-file-jpg','agsdi-file-png','agsdi-file-pdf','agsdi-file-zip',
			'agsdi-file-ai','agsdi-file-ps','agsdi-delete','agsdi-notebook','agsdi-notebook-2',
			'agsdi-documents','agsdi-brochure','agsdi-clip','agsdi-align-center',
			'agsdi-align-left','agsdi-align-justify','agsdi-align-right','agsdi-portrait',
			'agsdi-landscape','agsdi-portrait-2','agsdi-wedding','agsdi-billboard','agsdi-flash',
			'agsdi-crop','agsdi-message-heart','agsdi-adjust-square-vert',
			'agsdi-adjust-circle-vert','agsdi-camera','agsdi-grid','agsdi-grid-2','agsdi-layers',
			'agsdi-ruler','agsdi-eyedropper','agsdi-aperture','agsdi-macro','agsdi-pin',
			'agsdi-contrast','agsdi-battery-level-empty','agsdi-battery-level1',
			'agsdi-battery-level2','agsdi-battery-level3','agsdi-usb-stick','agsdi-sd-card',
			'agsdi-stethoscope','agsdi-vaccine','agsdi-hospital','agsdi-pills','agsdi-heart',
			'agsdi-heartbeat','agsdi-hearts','agsdi-heart-leaf','agsdi-heart-leaf-2',
			'agsdi-coffee-2','agsdi-hands','agsdi-book','agsdi-food-heart','agsdi-soup-heart',
			'agsdi-food','agsdi-soup','agsdi-pencil','agsdi-people','agsdi-money-bag',
			'agsdi-world-heart','agsdi-doctor','agsdi-person','agsdi-water-cycle','agsdi-sign',
			'agsdi-hand-leaf','agsdi-gift-heart','agsdi-sleep','agsdi-hand-heart',
			'agsdi-calendar-heart','agsdi-book-heart','agsdi-list','agsdi-leaves','agsdi-bread',
			'agsdi-bread-heart','agsdi-animal-hands','agsdi-animal-heart','agsdi-dog',
			'agsdi-cat','agsdi-bird','agsdi-dog-2','agsdi-cat-2','agsdi-transporter',
			'agsdi-adjust-square-horiz','agsdi-adjust-circle-horiz','agsdi-square',
			'agsdi-circle','agsdi-triangle','agsdi-pentagon','agsdi-hexagon','agsdi-heptagon',
			'agsdi-refresh-2','agsdi-pause','agsdi-play','agsdi-fast-forward','agsdi-rewind',
			'agsdi-previous','agsdi-next','agsdi-stop','agsdi-arrow-left','agsdi-arrow-right',
			'agsdi-arrow-up','agsdi-arrow-down','agsdi-face-sad','agsdi-face-happy',
			'agsdi-face-neutral','agsdi-messenger','agsdi-facebook','agsdi-facebook-like',
			'agsdi-twitter','agsdi-google-plus','agsdi-linkedin','agsdi-pinterest',
			'agsdi-tumblr','agsdi-instagram','agsdi-skype','agsdi-flickr','agsdi-myspace',
			'agsdi-dribble','agsdi-vimeo','agsdi-500px','agsdi-behance','agsdi-bitbucket',
			'agsdi-deviantart','agsdi-github','agsdi-github-2','agsdi-medium','agsdi-medium-2',
			'agsdi-meetup','agsdi-meetup-2','agsdi-slack','agsdi-slack-2','agsdi-snapchat',
			'agsdi-twitch','agsdi-rss','agsdi-rss-2','agsdi-paypal','agsdi-stripe',
			'agsdi-youtube','agsdi-facebook-2','agsdi-twitter-2','agsdi-linkedin-2',
			'agsdi-tumblr-2','agsdi-myspace-2','agsdi-slack-3','agsdi-github-3','agsdi-vimeo-2',
			'agsdi-behance-2','agsdi-apple','agsdi-quora','agsdi-trello','agsdi-amazon',
			'agsdi-reddit','agsdi-windows','agsdi-wordpress','agsdi-patreon','agsdi-patreon-2',
			'agsdi-soundcloud','agsdi-spotify','agsdi-google-hangout','agsdi-dropbox',
			'agsdi-tinder','agsdi-whatsapp','agsdi-adobe-cc','agsdi-android','agsdi-html5',
			'agsdi-google-drive','agsdi-pinterest-2','agsdi-gmail','agsdi-google-wallet',
			'agsdi-google-sheets','agsdi-twitch-2');
	}
	
	
	
	public static function getProIcons() {
		return array_merge(
			self::getMultiColorIconsColorized(),
			self::getFontAwesomeIcons(),
			self::getMaterialIcons(),
			self::getAgsAngularIcons()
		);
	}
	
	public static function getMultiColorIcons() {
		return array('agsdix-smc-design-tool','agsdix-smc-website','agsdix-smc-mobile','agsdix-smc-email','agsdix-smc-phone','agsdix-smc-localization','agsdix-smc-font','agsdix-smc-responsive','agsdix-smc-form','agsdix-smc-delivery-time','agsdix-smc-warehouse','agsdix-smc-upload','agsdix-smc-image','agsdix-smc-mouse','agsdix-smc-code','agsdix-smc-price-tag','agsdix-smc-guarantee','agsdix-smc-search-item','agsdix-smc-notepad','agsdix-smc-tablet','agsdix-smc-report','agsdix-smc-folder','agsdix-smc-wallet','agsdix-smc-database','agsdix-smc-question','agsdix-smc-calendar','agsdix-smc-map','agsdix-smc-shopping-list','agsdix-smc-featured','agsdix-smc-sale','agsdix-smc-credit-card','agsdix-smc-bank-account','agsdix-smc-back-up','agsdix-smc-refund','agsdix-smc-shipment','agsdix-smc-online-purchase','agsdix-smc-server','agsdix-smc-settings','agsdix-smc-cloud','agsdix-smc-share','agsdix-smc-retail-shop','agsdix-smc-empty-cart','agsdix-smc-cache','agsdix-smc-eye-dropper','agsdix-smc-browser','agsdix-smc-full-cart','agsdix-smc-donation','agsdix-smc-currency');
	}
	
	public static function getAgsAngularIcons() {
		return array('agsdix-sao-phone','agsdix-sao-email','agsdix-sao-refund','agsdix-sao-calendar','agsdix-sao-map','agsdix-sao-info','agsdix-sao-question','agsdix-sao-location','agsdix-sao-hamburger-menu','agsdix-sao-user','agsdix-sao-arrow-left','agsdix-sao-arrow-right','agsdix-sao-check','agsdix-sao-search-item','agsdix-sao-star','agsdix-sao-filter','agsdix-sao-empty-cart','agsdix-sao-add-to-cart','agsdix-sao-purchase','agsdix-sao-price-tag','agsdix-sao-featured','agsdix-sao-shipping','agsdix-sao-time','agsdix-sao-shopping-list','agsdix-sao-online-purchase','agsdix-sao-retail-shop','agsdix-sao-currency','agsdix-sao-warehouse','agsdix-sao-guarantee','agsdix-sao-donation','agsdix-sao-wallet','agsdix-sao-money','agsdix-sao-bank-vault','agsdix-sao-credit-card','agsdix-sao-sun','agsdix-sao-wind','agsdix-sao-cloudy','agsdix-sao-rain','agsdix-sao-facebook','agsdix-sao-instagram','agsdix-sao-twitter','agsdix-sao-youtube','agsdix-sao-percent-off-sale','agsdix-sao-open-book','agsdix-sao-school-pencil','agsdix-sao-motherboard','agsdix-sao-exam','agsdix-sao-library-books','agsdix-sao-diploma','agsdix-sao-student','agsdix-sao-blackboard','agsdix-sao-bell','agsdix-sao-abacus-counting-frame','agsdix-sao-idea','agsdix-sao-chemistry','agsdix-sao-calculator','agsdix-sao-browser','agsdix-sao-seo','agsdix-sao-marketing','agsdix-sao-targeting','agsdix-sao-stats','agsdix-sao-analytics','agsdix-sao-responsive','agsdix-sao-design','agsdix-sao-link','agsdix-sao-social-media','agsdix-sao-speedometer','agsdix-sao-backup','agsdix-sao-briefcase','agsdix-sao-coffee-cup','agsdix-sao-puzzle','agsdix-sao-clock','agsdix-sao-chat','agsdix-sao-divi','agsdix-sao-wordpress','agsdix-sao-camera','agsdix-sao-plane','agsdix-sao-meal','agsdix-sao-car','agsdix-sao-plus','agsdix-sao-minus','agsdix-sao-x-multiply','agsdix-sao-fish','agsdix-sao-house','agsdix-sao-community','agsdix-sao-voting','agsdix-sao-advertisement','agsdix-sao-promotion','agsdix-sao-office','agsdix-sao-support-assistant','agsdix-sao-branding','agsdix-sao-price-comparison','agsdix-sao-hashtag','agsdix-sao-billboard','agsdix-sao-content','agsdix-sao-laptop','agsdix-sao-energy','agsdix-sao-disabled','agsdix-sao-music','agsdix-sao-signal-range');
		return array('agsdix-sao-phone','agsdix-saf-phone','agsdix-sao-email','agsdix-saf-email','agsdix-sao-refund','agsdix-saf-refund','agsdix-sao-calendar','agsdix-saf-calendar','agsdix-sao-map','agsdix-saf-map','agsdix-sao-info','agsdix-saf-info','agsdix-sao-question','agsdix-saf-question','agsdix-sao-location','agsdix-saf-location','agsdix-sao-hamburger-menu','agsdix-saf-hamburger-menu','agsdix-sao-user','agsdix-saf-user','agsdix-sao-arrow-left','agsdix-saf-arrow-left','agsdix-sao-arrow-right','agsdix-saf-arrow-right','agsdix-sao-check','agsdix-saf-check','agsdix-sao-search-item','agsdix-saf-search-item','agsdix-sao-star','agsdix-saf-star','agsdix-sao-filter','agsdix-saf-filter','agsdix-sao-empty-cart','agsdix-saf-empty-cart','agsdix-sao-add-to-cart','agsdix-saf-add-to-cart','agsdix-sao-purchase','agsdix-saf-purchase','agsdix-sao-price-tag','agsdix-saf-price-tag','agsdix-sao-featured','agsdix-saf-featured','agsdix-sao-shipping','agsdix-saf-shipping','agsdix-sao-time','agsdix-saf-time','agsdix-sao-shopping-list','agsdix-saf-shopping-list','agsdix-sao-online-purchase','agsdix-saf-online-purchase','agsdix-sao-retail-shop','agsdix-saf-retail-shop','agsdix-sao-currency','agsdix-saf-currency','agsdix-sao-warehouse','agsdix-saf-warehouse','agsdix-sao-guarantee','agsdix-saf-guarantee','agsdix-sao-donation','agsdix-saf-donation','agsdix-sao-wallet','agsdix-saf-wallet','agsdix-sao-money','agsdix-saf-money','agsdix-sao-bank-vault','agsdix-saf-bank-vault','agsdix-sao-credit-card','agsdix-saf-credit-card','agsdix-sao-sun','agsdix-saf-sun','agsdix-sao-wind','agsdix-saf-wind','agsdix-sao-cloudy','agsdix-saf-cloudy','agsdix-sao-rain','agsdix-saf-rain','agsdix-sao-facebook','agsdix-saf-facebook','agsdix-sao-instagram','agsdix-saf-instagram','agsdix-sao-twitter','agsdix-saf-twitter','agsdix-sao-youtube','agsdix-saf-youtube','agsdix-sao-percent-off-sale','agsdix-saf-percent-off-sale','agsdix-sao-open-book','agsdix-saf-open-book','agsdix-sao-school-pencil','agsdix-saf-school-pencil','agsdix-sao-motherboard','agsdix-saf-motherboard','agsdix-sao-exam','agsdix-saf-exam','agsdix-sao-library-books','agsdix-saf-library-books','agsdix-sao-diploma','agsdix-saf-diploma','agsdix-sao-student','agsdix-saf-student','agsdix-sao-blackboard','agsdix-saf-blackboard','agsdix-sao-bell','agsdix-saf-bell','agsdix-sao-abacus-counting-frame','agsdix-saf-abacus-counting-frame','agsdix-sao-idea','agsdix-saf-idea','agsdix-sao-chemistry','agsdix-saf-chemistry','agsdix-sao-calculator','agsdix-saf-calculator','agsdix-sao-browser','agsdix-saf-browser','agsdix-sao-seo','agsdix-saf-seo','agsdix-sao-marketing','agsdix-saf-marketing','agsdix-sao-targeting','agsdix-saf-targeting','agsdix-sao-stats','agsdix-saf-stats','agsdix-sao-analytics','agsdix-saf-analytics','agsdix-sao-responsive','agsdix-saf-responsive','agsdix-sao-design','agsdix-saf-design','agsdix-sao-link','agsdix-saf-link','agsdix-sao-social-media','agsdix-saf-social-media','agsdix-sao-speedometer','agsdix-saf-speedometer','agsdix-sao-backup','agsdix-saf-backup','agsdix-sao-briefcase','agsdix-saf-briefcase','agsdix-sao-coffee-cup','agsdix-saf-coffee-cup','agsdix-sao-puzzle','agsdix-saf-puzzle','agsdix-sao-clock','agsdix-saf-clock','agsdix-sao-chat','agsdix-saf-chat','agsdix-sao-divi','agsdix-saf-divi','agsdix-sao-wordpress','agsdix-saf-wordpress','agsdix-sao-camera','agsdix-saf-camera','agsdix-sao-plane','agsdix-saf-plane','agsdix-sao-meal','agsdix-saf-meal','agsdix-sao-car','agsdix-saf-car','agsdix-sao-plus','agsdix-saf-plus','agsdix-sao-minus','agsdix-saf-minus','agsdix-sao-x-multiply','agsdix-saf-x-multiply','agsdix-sao-fish','agsdix-saf-fish','agsdix-sao-house','agsdix-saf-house','agsdix-sao-community','agsdix-saf-community','agsdix-sao-voting','agsdix-saf-voting','agsdix-sao-advertisement','agsdix-saf-advertisement','agsdix-sao-promotion','agsdix-saf-promotion','agsdix-sao-office','agsdix-saf-office','agsdix-sao-support-assistant','agsdix-saf-support-assistant','agsdix-sao-branding','agsdix-saf-branding','agsdix-sao-price-comparison','agsdix-saf-price-comparison','agsdix-sao-hashtag','agsdix-saf-hashtag','agsdix-sao-billboard','agsdix-saf-billboard','agsdix-sao-content','agsdix-saf-content','agsdix-sao-laptop','agsdix-saf-laptop','agsdix-sao-energy','agsdix-saf-energy','agsdix-sao-disabled','agsdix-saf-disabled','agsdix-sao-music','agsdix-saf-music','agsdix-sao-signal-range','agsdix-saf-signal-range');
	}
	
	public static function getMaterialIcons() {
		// Icon names copied from Material icon pack, see icon-packs/material/LICENSE for license details
		return array('agsdix-smt1-3d-rotation','agsdix-smt1-ac-unit','agsdix-smt1-access-alarm','agsdix-smt1-access-alarms','agsdix-smt1-access-time','agsdix-smt1-accessibility','agsdix-smt1-accessible','agsdix-smt1-account-balance','agsdix-smt1-account-balance-wallet','agsdix-smt1-account-box','agsdix-smt1-account-circle','agsdix-smt1-adb','agsdix-smt1-add','agsdix-smt1-add-a-photo','agsdix-smt1-add-alarm','agsdix-smt1-add-alert','agsdix-smt1-add-box','agsdix-smt1-add-circle','agsdix-smt1-add-circle-outline','agsdix-smt1-add-location','agsdix-smt1-add-shopping-cart','agsdix-smt1-add-to-photos','agsdix-smt1-add-to-queue','agsdix-smt1-adjust','agsdix-smt1-airline-seat-flat','agsdix-smt1-airline-seat-flat-angled','agsdix-smt1-airline-seat-individual-suite','agsdix-smt1-airline-seat-legroom-extra','agsdix-smt1-airline-seat-legroom-normal','agsdix-smt1-airline-seat-legroom-reduced','agsdix-smt1-airline-seat-recline-extra','agsdix-smt1-airline-seat-recline-normal','agsdix-smt1-airplanemode-active','agsdix-smt1-airplanemode-inactive','agsdix-smt1-airplay','agsdix-smt1-airport-shuttle','agsdix-smt1-alarm','agsdix-smt1-alarm-add','agsdix-smt1-alarm-off','agsdix-smt1-alarm-on','agsdix-smt1-album','agsdix-smt1-all-inclusive','agsdix-smt1-all-out','agsdix-smt1-android','agsdix-smt1-announcement','agsdix-smt1-apps','agsdix-smt1-archive','agsdix-smt1-arrow-back','agsdix-smt1-arrow-downward','agsdix-smt1-arrow-drop-down','agsdix-smt1-arrow-drop-down-circle','agsdix-smt1-arrow-drop-up','agsdix-smt1-arrow-forward','agsdix-smt1-arrow-upward','agsdix-smt1-art-track','agsdix-smt1-aspect-ratio','agsdix-smt1-assessment','agsdix-smt1-assignment','agsdix-smt1-assignment-ind','agsdix-smt1-assignment-late','agsdix-smt1-assignment-return','agsdix-smt1-assignment-returned','agsdix-smt1-assignment-turned-in','agsdix-smt1-assistant','agsdix-smt1-assistant-photo','agsdix-smt1-attach-file','agsdix-smt1-attach-money','agsdix-smt1-attachment','agsdix-smt1-audiotrack','agsdix-smt1-autorenew','agsdix-smt1-av-timer','agsdix-smt1-backspace','agsdix-smt1-backup','agsdix-smt1-battery-alert','agsdix-smt1-battery-charging-full','agsdix-smt1-battery-full','agsdix-smt1-battery-std','agsdix-smt1-battery-unknown','agsdix-smt1-beach-access','agsdix-smt1-beenhere','agsdix-smt1-block','agsdix-smt1-bluetooth','agsdix-smt1-bluetooth-audio','agsdix-smt1-bluetooth-connected','agsdix-smt1-bluetooth-disabled','agsdix-smt1-bluetooth-searching','agsdix-smt1-blur-circular','agsdix-smt1-blur-linear','agsdix-smt1-blur-off','agsdix-smt1-blur-on','agsdix-smt1-book','agsdix-smt1-bookmark','agsdix-smt1-bookmark-border','agsdix-smt1-border-all','agsdix-smt1-border-bottom','agsdix-smt1-border-clear','agsdix-smt1-border-color','agsdix-smt1-border-horizontal','agsdix-smt1-border-inner','agsdix-smt1-border-left','agsdix-smt1-border-outer','agsdix-smt1-border-right','agsdix-smt1-border-style','agsdix-smt1-border-top','agsdix-smt1-border-vertical','agsdix-smt1-branding-watermark','agsdix-smt1-brightness-1','agsdix-smt1-brightness-2','agsdix-smt1-brightness-3','agsdix-smt1-brightness-4','agsdix-smt1-brightness-5','agsdix-smt1-brightness-6','agsdix-smt1-brightness-7','agsdix-smt1-brightness-auto','agsdix-smt1-brightness-high','agsdix-smt1-brightness-low','agsdix-smt1-brightness-medium','agsdix-smt1-broken-image','agsdix-smt1-brush','agsdix-smt1-bubble-chart','agsdix-smt1-bug-report','agsdix-smt1-build','agsdix-smt1-burst-mode','agsdix-smt1-business','agsdix-smt1-business-center','agsdix-smt1-cached','agsdix-smt1-cake','agsdix-smt1-call','agsdix-smt1-call-end','agsdix-smt1-call-made','agsdix-smt1-call-merge','agsdix-smt1-call-missed','agsdix-smt1-call-missed-outgoing','agsdix-smt1-call-received','agsdix-smt1-call-split','agsdix-smt1-call-to-action','agsdix-smt1-camera','agsdix-smt1-camera-alt','agsdix-smt1-camera-enhance','agsdix-smt1-camera-front','agsdix-smt1-camera-rear','agsdix-smt1-camera-roll','agsdix-smt1-cancel','agsdix-smt1-card-giftcard','agsdix-smt1-card-membership','agsdix-smt1-card-travel','agsdix-smt1-casino','agsdix-smt1-cast','agsdix-smt1-cast-connected','agsdix-smt1-center-focus-strong','agsdix-smt1-center-focus-weak','agsdix-smt1-change-history','agsdix-smt1-chat','agsdix-smt1-chat-bubble','agsdix-smt1-chat-bubble-outline','agsdix-smt1-check','agsdix-smt1-check-box','agsdix-smt1-check-box-outline-blank','agsdix-smt1-check-circle','agsdix-smt1-chevron-left','agsdix-smt1-chevron-right','agsdix-smt1-child-care','agsdix-smt1-child-friendly','agsdix-smt1-chrome-reader-mode','agsdix-smt1-class','agsdix-smt1-clear','agsdix-smt1-clear-all','agsdix-smt1-close','agsdix-smt1-closed-caption','agsdix-smt1-cloud','agsdix-smt1-cloud-circle','agsdix-smt1-cloud-done','agsdix-smt1-cloud-download','agsdix-smt1-cloud-off','agsdix-smt1-cloud-queue','agsdix-smt1-cloud-upload','agsdix-smt1-code','agsdix-smt1-collections','agsdix-smt1-collections-bookmark','agsdix-smt1-color-lens','agsdix-smt1-colorize','agsdix-smt1-comment','agsdix-smt1-compare','agsdix-smt1-compare-arrows','agsdix-smt1-computer','agsdix-smt1-confirmation-number','agsdix-smt1-contact-mail','agsdix-smt1-contact-phone','agsdix-smt1-contacts','agsdix-smt1-content-copy','agsdix-smt1-content-cut','agsdix-smt1-content-paste','agsdix-smt1-control-point','agsdix-smt1-control-point-duplicate','agsdix-smt1-copyright','agsdix-smt1-create','agsdix-smt1-create-new-folder','agsdix-smt1-credit-card','agsdix-smt1-crop','agsdix-smt1-crop-16-9','agsdix-smt1-crop-3-2','agsdix-smt1-crop-5-4','agsdix-smt1-crop-7-5','agsdix-smt1-crop-din','agsdix-smt1-crop-free','agsdix-smt1-crop-landscape','agsdix-smt1-crop-original','agsdix-smt1-crop-portrait','agsdix-smt1-crop-rotate','agsdix-smt1-crop-square','agsdix-smt1-dashboard','agsdix-smt1-data-usage','agsdix-smt1-date-range','agsdix-smt1-dehaze','agsdix-smt1-delete','agsdix-smt1-delete-forever','agsdix-smt1-delete-sweep','agsdix-smt1-description','agsdix-smt1-desktop-mac','agsdix-smt1-desktop-windows','agsdix-smt1-details','agsdix-smt1-developer-board','agsdix-smt1-developer-mode','agsdix-smt1-device-hub','agsdix-smt1-devices','agsdix-smt1-devices-other','agsdix-smt1-dialer-sip','agsdix-smt1-dialpad','agsdix-smt1-directions','agsdix-smt1-directions-bike','agsdix-smt1-directions-boat','agsdix-smt1-directions-bus','agsdix-smt1-directions-car','agsdix-smt1-directions-railway','agsdix-smt1-directions-run','agsdix-smt1-directions-subway','agsdix-smt1-directions-transit','agsdix-smt1-directions-walk','agsdix-smt1-disc-full','agsdix-smt1-dns','agsdix-smt1-do-not-disturb','agsdix-smt1-do-not-disturb-alt','agsdix-smt1-do-not-disturb-off','agsdix-smt1-do-not-disturb-on','agsdix-smt1-dock','agsdix-smt1-domain','agsdix-smt1-done','agsdix-smt1-done-all','agsdix-smt1-donut-large','agsdix-smt1-donut-small','agsdix-smt1-drafts','agsdix-smt1-drag-handle','agsdix-smt1-drive-eta','agsdix-smt1-dvr','agsdix-smt1-edit','agsdix-smt1-edit-location','agsdix-smt1-eject','agsdix-smt1-email','agsdix-smt1-enhanced-encryption','agsdix-smt1-equalizer','agsdix-smt1-error','agsdix-smt1-error-outline','agsdix-smt1-euro-symbol','agsdix-smt1-ev-station','agsdix-smt1-event','agsdix-smt1-event-available','agsdix-smt1-event-busy','agsdix-smt1-event-note','agsdix-smt1-event-seat','agsdix-smt1-exit-to-app','agsdix-smt1-expand-less','agsdix-smt1-expand-more','agsdix-smt1-explicit','agsdix-smt1-explore','agsdix-smt1-exposure','agsdix-smt1-exposure-neg-1','agsdix-smt1-exposure-neg-2','agsdix-smt1-exposure-plus-1','agsdix-smt1-exposure-plus-2','agsdix-smt1-exposure-zero','agsdix-smt1-extension','agsdix-smt1-face','agsdix-smt1-fast-forward','agsdix-smt1-fast-rewind','agsdix-smt1-favorite','agsdix-smt1-favorite-border','agsdix-smt1-featured-play-list','agsdix-smt1-featured-video','agsdix-smt1-feedback','agsdix-smt1-fiber-dvr','agsdix-smt1-fiber-manual-record','agsdix-smt1-fiber-new','agsdix-smt1-fiber-pin','agsdix-smt1-fiber-smart-record','agsdix-smt1-file-download','agsdix-smt1-file-upload','agsdix-smt1-filter','agsdix-smt1-filter-1','agsdix-smt1-filter-2','agsdix-smt1-filter-3','agsdix-smt1-filter-4','agsdix-smt1-filter-5','agsdix-smt1-filter-6','agsdix-smt1-filter-7','agsdix-smt1-filter-8','agsdix-smt1-filter-9','agsdix-smt1-filter-9-plus','agsdix-smt1-filter-b-and-w','agsdix-smt1-filter-center-focus','agsdix-smt1-filter-drama','agsdix-smt1-filter-frames','agsdix-smt1-filter-hdr','agsdix-smt1-filter-list','agsdix-smt1-filter-none','agsdix-smt1-filter-tilt-shift','agsdix-smt1-filter-vintage','agsdix-smt1-find-in-page','agsdix-smt1-find-replace','agsdix-smt1-fingerprint','agsdix-smt1-first-page','agsdix-smt1-fitness-center','agsdix-smt1-flag','agsdix-smt1-flare','agsdix-smt1-flash-auto','agsdix-smt2-flash-off','agsdix-smt2-flash-on','agsdix-smt2-flight','agsdix-smt2-flight-land','agsdix-smt2-flight-takeoff','agsdix-smt2-flip','agsdix-smt2-flip-to-back','agsdix-smt2-flip-to-front','agsdix-smt2-folder','agsdix-smt2-folder-open','agsdix-smt2-folder-shared','agsdix-smt2-folder-special','agsdix-smt2-font-download','agsdix-smt2-format-align-center','agsdix-smt2-format-align-justify','agsdix-smt2-format-align-left','agsdix-smt2-format-align-right','agsdix-smt2-format-bold','agsdix-smt2-format-clear','agsdix-smt2-format-color-fill','agsdix-smt2-format-color-reset','agsdix-smt2-format-color-text','agsdix-smt2-format-indent-decrease','agsdix-smt2-format-indent-increase','agsdix-smt2-format-italic','agsdix-smt2-format-line-spacing','agsdix-smt2-format-list-bulleted','agsdix-smt2-format-list-numbered','agsdix-smt2-format-paint','agsdix-smt2-format-quote','agsdix-smt2-format-shapes','agsdix-smt2-format-size','agsdix-smt2-format-strikethrough','agsdix-smt2-format-textdirection-l-to-r','agsdix-smt2-format-textdirection-r-to-l','agsdix-smt2-format-underlined','agsdix-smt2-forum','agsdix-smt2-forward','agsdix-smt2-forward-10','agsdix-smt2-forward-30','agsdix-smt2-forward-5','agsdix-smt2-free-breakfast','agsdix-smt2-fullscreen','agsdix-smt2-fullscreen-exit','agsdix-smt2-functions','agsdix-smt2-g-translate','agsdix-smt2-gamepad','agsdix-smt2-games','agsdix-smt2-gavel','agsdix-smt2-gesture','agsdix-smt2-get-app','agsdix-smt2-gif','agsdix-smt2-goat','agsdix-smt2-golf-course','agsdix-smt2-gps-fixed','agsdix-smt2-gps-not-fixed','agsdix-smt2-gps-off','agsdix-smt2-grade','agsdix-smt2-gradient','agsdix-smt2-grain','agsdix-smt2-graphic-eq','agsdix-smt2-grid-off','agsdix-smt2-grid-on','agsdix-smt2-group','agsdix-smt2-group-add','agsdix-smt2-group-work','agsdix-smt2-hd','agsdix-smt2-hdr-off','agsdix-smt2-hdr-on','agsdix-smt2-hdr-strong','agsdix-smt2-hdr-weak','agsdix-smt2-headset','agsdix-smt2-headset-mic','agsdix-smt2-healing','agsdix-smt2-hearing','agsdix-smt2-help','agsdix-smt2-help-outline','agsdix-smt2-high-quality','agsdix-smt2-highlight','agsdix-smt2-highlight-off','agsdix-smt2-history','agsdix-smt2-home','agsdix-smt2-hot-tub','agsdix-smt2-hotel','agsdix-smt2-hourglass-empty','agsdix-smt2-hourglass-full','agsdix-smt2-http','agsdix-smt2-https','agsdix-smt2-image','agsdix-smt2-image-aspect-ratio','agsdix-smt2-import-contacts','agsdix-smt2-import-export','agsdix-smt2-important-devices','agsdix-smt2-inbox','agsdix-smt2-indeterminate-check-box','agsdix-smt2-info','agsdix-smt2-info-outline','agsdix-smt2-input','agsdix-smt2-insert-chart','agsdix-smt2-insert-comment','agsdix-smt2-insert-drive-file','agsdix-smt2-insert-emoticon','agsdix-smt2-insert-invitation','agsdix-smt2-insert-link','agsdix-smt2-insert-photo','agsdix-smt2-invert-colors','agsdix-smt2-invert-colors-off','agsdix-smt2-iso','agsdix-smt2-keyboard','agsdix-smt2-keyboard-arrow-down','agsdix-smt2-keyboard-arrow-left','agsdix-smt2-keyboard-arrow-right','agsdix-smt2-keyboard-arrow-up','agsdix-smt2-keyboard-backspace','agsdix-smt2-keyboard-capslock','agsdix-smt2-keyboard-hide','agsdix-smt2-keyboard-return','agsdix-smt2-keyboard-tab','agsdix-smt2-keyboard-voice','agsdix-smt2-kitchen','agsdix-smt2-label','agsdix-smt2-label-outline','agsdix-smt2-landscape','agsdix-smt2-language','agsdix-smt2-laptop','agsdix-smt2-laptop-chromebook','agsdix-smt2-laptop-mac','agsdix-smt2-laptop-windows','agsdix-smt2-last-page','agsdix-smt2-launch','agsdix-smt2-layers','agsdix-smt2-layers-clear','agsdix-smt2-leak-add','agsdix-smt2-leak-remove','agsdix-smt2-lens','agsdix-smt2-library-add','agsdix-smt2-library-books','agsdix-smt2-library-music','agsdix-smt2-lightbulb-outline','agsdix-smt2-line-style','agsdix-smt2-line-weight','agsdix-smt2-linear-scale','agsdix-smt2-link','agsdix-smt2-linked-camera','agsdix-smt2-list','agsdix-smt2-live-help','agsdix-smt2-live-tv','agsdix-smt2-local-activity','agsdix-smt2-local-airport','agsdix-smt2-local-atm','agsdix-smt2-local-bar','agsdix-smt2-local-cafe','agsdix-smt2-local-car-wash','agsdix-smt2-local-convenience-store','agsdix-smt2-local-dining','agsdix-smt2-local-drink','agsdix-smt2-local-florist','agsdix-smt2-local-gas-station','agsdix-smt2-local-grocery-store','agsdix-smt2-local-hospital','agsdix-smt2-local-hotel','agsdix-smt2-local-laundry-service','agsdix-smt2-local-library','agsdix-smt2-local-mall','agsdix-smt2-local-movies','agsdix-smt2-local-offer','agsdix-smt2-local-parking','agsdix-smt2-local-pharmacy','agsdix-smt2-local-phone','agsdix-smt2-local-pizza','agsdix-smt2-local-play','agsdix-smt2-local-post-office','agsdix-smt2-local-printshop','agsdix-smt2-local-see','agsdix-smt2-local-shipping','agsdix-smt2-local-taxi','agsdix-smt2-location-city','agsdix-smt2-location-disabled','agsdix-smt2-location-off','agsdix-smt2-location-on','agsdix-smt2-location-searching','agsdix-smt2-lock','agsdix-smt2-lock-open','agsdix-smt2-lock-outline','agsdix-smt2-looks','agsdix-smt2-looks-3','agsdix-smt2-looks-4','agsdix-smt2-looks-5','agsdix-smt2-looks-6','agsdix-smt2-looks-one','agsdix-smt2-looks-two','agsdix-smt2-loop','agsdix-smt2-loupe','agsdix-smt2-low-priority','agsdix-smt2-loyalty','agsdix-smt2-mail','agsdix-smt2-mail-outline','agsdix-smt2-map','agsdix-smt2-markunread','agsdix-smt2-markunread-mailbox','agsdix-smt2-memory','agsdix-smt2-menu','agsdix-smt2-merge-type','agsdix-smt2-message','agsdix-smt2-mic','agsdix-smt2-mic-none','agsdix-smt2-mic-off','agsdix-smt2-mms','agsdix-smt2-mode-comment','agsdix-smt2-mode-edit','agsdix-smt2-monetization-on','agsdix-smt2-money-off','agsdix-smt2-monochrome-photos','agsdix-smt2-mood','agsdix-smt2-mood-bad','agsdix-smt2-more','agsdix-smt2-more-horiz','agsdix-smt2-more-vert','agsdix-smt2-motorcycle','agsdix-smt2-mouse','agsdix-smt2-move-to-inbox','agsdix-smt2-movie','agsdix-smt2-movie-creation','agsdix-smt2-movie-filter','agsdix-smt2-multiline-chart','agsdix-smt2-music-note','agsdix-smt2-music-video','agsdix-smt2-my-location','agsdix-smt2-nature','agsdix-smt2-nature-people','agsdix-smt2-navigate-before','agsdix-smt2-navigate-next','agsdix-smt2-navigation','agsdix-smt2-near-me','agsdix-smt2-network-cell','agsdix-smt2-network-check','agsdix-smt2-network-locked','agsdix-smt2-network-wifi','agsdix-smt2-new-releases','agsdix-smt2-next-week','agsdix-smt2-nfc','agsdix-smt2-no-encryption','agsdix-smt2-no-sim','agsdix-smt2-not-interested','agsdix-smt2-note','agsdix-smt2-note-add','agsdix-smt2-notifications','agsdix-smt2-notifications-active','agsdix-smt2-notifications-none','agsdix-smt2-notifications-off','agsdix-smt2-notifications-paused','agsdix-smt2-offline-pin','agsdix-smt2-ondemand-video','agsdix-smt2-opacity','agsdix-smt2-open-in-browser','agsdix-smt2-open-in-new','agsdix-smt2-open-with','agsdix-smt2-pages','agsdix-smt2-pageview','agsdix-smt2-palette','agsdix-smt2-pan-tool','agsdix-smt2-panorama','agsdix-smt2-panorama-fish-eye','agsdix-smt2-panorama-horizontal','agsdix-smt2-panorama-vertical','agsdix-smt2-panorama-wide-angle','agsdix-smt2-party-mode','agsdix-smt2-pause','agsdix-smt2-pause-circle-filled','agsdix-smt2-pause-circle-outline','agsdix-smt2-payment','agsdix-smt2-people','agsdix-smt2-people-outline','agsdix-smt2-perm-camera-mic','agsdix-smt2-perm-contact-calendar','agsdix-smt2-perm-data-setting','agsdix-smt2-perm-device-information','agsdix-smt2-perm-identity','agsdix-smt2-perm-media','agsdix-smt2-perm-phone-msg','agsdix-smt2-perm-scan-wifi','agsdix-smt2-person','agsdix-smt2-person-add','agsdix-smt2-person-outline','agsdix-smt2-person-pin','agsdix-smt2-person-pin-circle','agsdix-smt2-personal-video','agsdix-smt2-pets','agsdix-smt2-phone','agsdix-smt2-phone-android','agsdix-smt2-phone-bluetooth-speaker','agsdix-smt2-phone-forwarded','agsdix-smt2-phone-in-talk','agsdix-smt2-phone-iphone','agsdix-smt2-phone-locked','agsdix-smt2-phone-missed','agsdix-smt2-phone-paused','agsdix-smt2-phonelink','agsdix-smt2-phonelink-erase','agsdix-smt2-phonelink-lock','agsdix-smt2-phonelink-off','agsdix-smt2-phonelink-ring','agsdix-smt2-phonelink-setup','agsdix-smt2-photo','agsdix-smt2-photo-album','agsdix-smt2-photo-camera','agsdix-smt2-photo-filter','agsdix-smt2-photo-library','agsdix-smt2-photo-size-select-actual','agsdix-smt2-photo-size-select-large','agsdix-smt2-photo-size-select-small','agsdix-smt2-picture-as-pdf','agsdix-smt2-picture-in-picture','agsdix-smt2-picture-in-picture-alt','agsdix-smt2-pie-chart','agsdix-smt2-pie-chart-outlined','agsdix-smt2-pin-drop','agsdix-smt2-place','agsdix-smt2-play-arrow','agsdix-smt2-play-circle-filled','agsdix-smt2-play-circle-outline','agsdix-smt2-play-for-work','agsdix-smt2-playlist-add','agsdix-smt2-playlist-add-check','agsdix-smt3-playlist-play','agsdix-smt3-plus-one','agsdix-smt3-poll','agsdix-smt3-polymer','agsdix-smt3-pool','agsdix-smt3-portable-wifi-off','agsdix-smt3-portrait','agsdix-smt3-power','agsdix-smt3-power-input','agsdix-smt3-power-settings-new','agsdix-smt3-pregnant-woman','agsdix-smt3-present-to-all','agsdix-smt3-print','agsdix-smt3-priority-high','agsdix-smt3-public','agsdix-smt3-publish','agsdix-smt3-query-builder','agsdix-smt3-question-answer','agsdix-smt3-queue','agsdix-smt3-queue-music','agsdix-smt3-queue-play-next','agsdix-smt3-radio','agsdix-smt3-radio-button-checked','agsdix-smt3-radio-button-unchecked','agsdix-smt3-rate-review','agsdix-smt3-receipt','agsdix-smt3-recent-actors','agsdix-smt3-record-voice-over','agsdix-smt3-redeem','agsdix-smt3-redo','agsdix-smt3-refresh','agsdix-smt3-remove','agsdix-smt3-remove-circle','agsdix-smt3-remove-circle-outline','agsdix-smt3-remove-from-queue','agsdix-smt3-remove-red-eye','agsdix-smt3-remove-shopping-cart','agsdix-smt3-reorder','agsdix-smt3-repeat','agsdix-smt3-repeat-one','agsdix-smt3-replay','agsdix-smt3-replay-10','agsdix-smt3-replay-30','agsdix-smt3-replay-5','agsdix-smt3-reply','agsdix-smt3-reply-all','agsdix-smt3-report','agsdix-smt3-report-problem','agsdix-smt3-restaurant','agsdix-smt3-restaurant-menu','agsdix-smt3-restore','agsdix-smt3-restore-page','agsdix-smt3-ring-volume','agsdix-smt3-room','agsdix-smt3-room-service','agsdix-smt3-rotate-90-degrees-ccw','agsdix-smt3-rotate-left','agsdix-smt3-rotate-right','agsdix-smt3-rounded-corner','agsdix-smt3-router','agsdix-smt3-rowing','agsdix-smt3-rss-feed','agsdix-smt3-rv-hookup','agsdix-smt3-satellite','agsdix-smt3-save','agsdix-smt3-scanner','agsdix-smt3-schedule','agsdix-smt3-school','agsdix-smt3-screen-lock-landscape','agsdix-smt3-screen-lock-portrait','agsdix-smt3-screen-lock-rotation','agsdix-smt3-screen-rotation','agsdix-smt3-screen-share','agsdix-smt3-sd-card','agsdix-smt3-sd-storage','agsdix-smt3-search','agsdix-smt3-security','agsdix-smt3-select-all','agsdix-smt3-send','agsdix-smt3-sentiment-dissatisfied','agsdix-smt3-sentiment-neutral','agsdix-smt3-sentiment-satisfied','agsdix-smt3-sentiment-very-dissatisfied','agsdix-smt3-sentiment-very-satisfied','agsdix-smt3-settings','agsdix-smt3-settings-applications','agsdix-smt3-settings-backup-restore','agsdix-smt3-settings-bluetooth','agsdix-smt3-settings-brightness','agsdix-smt3-settings-cell','agsdix-smt3-settings-ethernet','agsdix-smt3-settings-input-antenna','agsdix-smt3-settings-input-component','agsdix-smt3-settings-input-composite','agsdix-smt3-settings-input-hdmi','agsdix-smt3-settings-input-svideo','agsdix-smt3-settings-overscan','agsdix-smt3-settings-phone','agsdix-smt3-settings-power','agsdix-smt3-settings-remote','agsdix-smt3-settings-system-daydream','agsdix-smt3-settings-voice','agsdix-smt3-share','agsdix-smt3-shop','agsdix-smt3-shop-two','agsdix-smt3-shopping-basket','agsdix-smt3-shopping-cart','agsdix-smt3-short-text','agsdix-smt3-show-chart','agsdix-smt3-shuffle','agsdix-smt3-signal-cellular-4-bar','agsdix-smt3-signal-cellular-connected-no-internet-4-bar','agsdix-smt3-signal-cellular-no-sim','agsdix-smt3-signal-cellular-null','agsdix-smt3-signal-cellular-off','agsdix-smt3-signal-wifi-4-bar','agsdix-smt3-signal-wifi-4-bar-lock','agsdix-smt3-signal-wifi-off','agsdix-smt3-sim-card','agsdix-smt3-sim-card-alert','agsdix-smt3-skip-next','agsdix-smt3-skip-previous','agsdix-smt3-slideshow','agsdix-smt3-slow-motion-video','agsdix-smt3-smartphone','agsdix-smt3-smoke-free','agsdix-smt3-smoking-rooms','agsdix-smt3-sms','agsdix-smt3-sms-failed','agsdix-smt3-snooze','agsdix-smt3-sort','agsdix-smt3-sort-by-alpha','agsdix-smt3-spa','agsdix-smt3-space-bar','agsdix-smt3-speaker','agsdix-smt3-speaker-group','agsdix-smt3-speaker-notes','agsdix-smt3-speaker-notes-off','agsdix-smt3-speaker-phone','agsdix-smt3-spellcheck','agsdix-smt3-star','agsdix-smt3-star-border','agsdix-smt3-star-half','agsdix-smt3-stars','agsdix-smt3-stay-current-landscape','agsdix-smt3-stay-current-portrait','agsdix-smt3-stay-primary-landscape','agsdix-smt3-stay-primary-portrait','agsdix-smt3-stop','agsdix-smt3-stop-screen-share','agsdix-smt3-storage','agsdix-smt3-store','agsdix-smt3-store-mall-directory','agsdix-smt3-straighten','agsdix-smt3-streetview','agsdix-smt3-strikethrough-s','agsdix-smt3-style','agsdix-smt3-subdirectory-arrow-left','agsdix-smt3-subdirectory-arrow-right','agsdix-smt3-subject','agsdix-smt3-subscriptions','agsdix-smt3-subtitles','agsdix-smt3-subway','agsdix-smt3-supervisor-account','agsdix-smt3-surround-sound','agsdix-smt3-swap-calls','agsdix-smt3-swap-horiz','agsdix-smt3-swap-vert','agsdix-smt3-swap-vertical-circle','agsdix-smt3-switch-camera','agsdix-smt3-switch-video','agsdix-smt3-sync','agsdix-smt3-sync-disabled','agsdix-smt3-sync-problem','agsdix-smt3-system-update','agsdix-smt3-system-update-alt','agsdix-smt3-tab','agsdix-smt3-tab-unselected','agsdix-smt3-tablet','agsdix-smt3-tablet-android','agsdix-smt3-tablet-mac','agsdix-smt3-tag-faces','agsdix-smt3-tap-and-play','agsdix-smt3-terrain','agsdix-smt3-text-fields','agsdix-smt3-text-format','agsdix-smt3-textsms','agsdix-smt3-texture','agsdix-smt3-theaters','agsdix-smt3-thumb-down','agsdix-smt3-thumb-up','agsdix-smt3-thumbs-up-down','agsdix-smt3-time-to-leave','agsdix-smt3-timelapse','agsdix-smt3-timeline','agsdix-smt3-timer','agsdix-smt3-timer-10','agsdix-smt3-timer-3','agsdix-smt3-timer-off','agsdix-smt3-title','agsdix-smt3-toc','agsdix-smt3-today','agsdix-smt3-toll','agsdix-smt3-tonality','agsdix-smt3-touch-app','agsdix-smt3-toys','agsdix-smt3-track-changes','agsdix-smt3-traffic','agsdix-smt3-train','agsdix-smt3-tram','agsdix-smt3-transfer-within-a-station','agsdix-smt3-transform','agsdix-smt3-translate','agsdix-smt3-trending-down','agsdix-smt3-trending-flat','agsdix-smt3-trending-up','agsdix-smt3-tune','agsdix-smt3-turned-in','agsdix-smt3-turned-in-not','agsdix-smt3-tv','agsdix-smt3-unarchive','agsdix-smt3-undo','agsdix-smt3-unfold-less','agsdix-smt3-unfold-more','agsdix-smt3-update','agsdix-smt3-usb','agsdix-smt3-verified-user','agsdix-smt3-vertical-align-bottom','agsdix-smt3-vertical-align-center','agsdix-smt3-vertical-align-top','agsdix-smt3-vibration','agsdix-smt3-video-call','agsdix-smt3-video-label','agsdix-smt3-video-library','agsdix-smt3-videocam','agsdix-smt3-videocam-off','agsdix-smt3-videogame-asset','agsdix-smt3-view-agenda','agsdix-smt3-view-array','agsdix-smt3-view-carousel','agsdix-smt3-view-column','agsdix-smt3-view-comfy','agsdix-smt3-view-compact','agsdix-smt3-view-day','agsdix-smt3-view-headline','agsdix-smt3-view-list','agsdix-smt3-view-module','agsdix-smt3-view-quilt','agsdix-smt3-view-stream','agsdix-smt3-view-week','agsdix-smt3-vignette','agsdix-smt3-visibility','agsdix-smt3-visibility-off','agsdix-smt3-voice-chat','agsdix-smt3-voicemail','agsdix-smt3-volume-down','agsdix-smt3-volume-mute','agsdix-smt3-volume-off','agsdix-smt3-volume-up','agsdix-smt3-vpn-key','agsdix-smt3-vpn-lock','agsdix-smt3-wallpaper','agsdix-smt3-warning','agsdix-smt3-watch','agsdix-smt3-watch-later','agsdix-smt3-wb-auto','agsdix-smt3-wb-cloudy','agsdix-smt3-wb-incandescent','agsdix-smt3-wb-iridescent','agsdix-smt3-wb-sunny','agsdix-smt3-wc','agsdix-smt3-web','agsdix-smt3-web-asset','agsdix-smt3-weekend','agsdix-smt3-whatshot','agsdix-smt3-widgets','agsdix-smt3-wifi','agsdix-smt3-wifi-lock','agsdix-smt3-wifi-tethering','agsdix-smt3-work','agsdix-smt3-wrap-text','agsdix-smt3-youtube-searched-for','agsdix-smt3-zoom-in','agsdix-smt3-zoom-out','agsdix-smt3-zoom-out-map');
	}
	
	public static function getMultiColorIconsColorized() {
		$icons = array();
		$colorSchemes = get_option('aspengrove_icons_colors', array());
		if (!empty($colorSchemes)) {
			foreach (self::getMultiColorIcons() as $icon) {
				foreach ($colorSchemes as $colorSchemeId => $colors) {
					$icons[] = $icon.'-'.$colorSchemeId;
				}
			}
		}
		return $icons;
	}
	
	public static function getFontAwesomeIcons() {
		// Icon IDs copied from Font Awesome JavaScript code; see icon-packs/fontawesome/LICENSE.txt for details
		return array('agsdix-fab fa-500px','agsdix-fab fa-accessible-icon','agsdix-fab fa-accusoft','agsdix-fas fa-address-book','agsdix-far fa-address-book','agsdix-fas fa-address-card','agsdix-far fa-address-card','agsdix-fas fa-adjust','agsdix-fab fa-adn','agsdix-fab fa-adversal','agsdix-fab fa-affiliatetheme','agsdix-fas fa-air-freshener','agsdix-fab fa-algolia','agsdix-fas fa-align-center','agsdix-fas fa-align-justify','agsdix-fas fa-align-left','agsdix-fas fa-align-right','agsdix-fas fa-allergies','agsdix-fab fa-amazon','agsdix-fab fa-amazon-pay','agsdix-fas fa-ambulance','agsdix-fas fa-american-sign-language-interpreting','agsdix-fab fa-amilia','agsdix-fas fa-anchor','agsdix-fab fa-android','agsdix-fab fa-angellist','agsdix-fas fa-angle-double-down','agsdix-fas fa-angle-double-left','agsdix-fas fa-angle-double-right','agsdix-fas fa-angle-double-up','agsdix-fas fa-angle-down','agsdix-fas fa-angle-left','agsdix-fas fa-angle-right','agsdix-fas fa-angle-up','agsdix-fas fa-angry','agsdix-far fa-angry','agsdix-fab fa-angrycreative','agsdix-fab fa-angular','agsdix-fab fa-app-store','agsdix-fab fa-app-store-ios','agsdix-fab fa-apper','agsdix-fab fa-apple','agsdix-fas fa-apple-alt','agsdix-fab fa-apple-pay','agsdix-fas fa-archive','agsdix-fas fa-archway','agsdix-fas fa-arrow-alt-circle-down','agsdix-far fa-arrow-alt-circle-down','agsdix-fas fa-arrow-alt-circle-left','agsdix-far fa-arrow-alt-circle-left','agsdix-fas fa-arrow-alt-circle-right','agsdix-far fa-arrow-alt-circle-right','agsdix-far fa-arrow-alt-circle-up','agsdix-fas fa-arrow-alt-circle-up','agsdix-fas fa-arrow-circle-down','agsdix-fas fa-arrow-circle-left','agsdix-fas fa-arrow-circle-right','agsdix-fas fa-arrow-circle-up','agsdix-fas fa-arrow-down','agsdix-fas fa-arrow-left','agsdix-fas fa-arrow-right','agsdix-fas fa-arrow-up','agsdix-fas fa-arrows-alt','agsdix-fas fa-arrows-alt-h','agsdix-fas fa-arrows-alt-v','agsdix-fas fa-assistive-listening-systems','agsdix-fas fa-asterisk','agsdix-fab fa-asymmetrik','agsdix-fas fa-at','agsdix-fas fa-atlas','agsdix-fas fa-atom','agsdix-fab fa-audible','agsdix-fas fa-audio-description','agsdix-fab fa-autoprefixer','agsdix-fab fa-avianex','agsdix-fab fa-aviato','agsdix-fas fa-award','agsdix-fab fa-aws','agsdix-fas fa-backspace','agsdix-fas fa-backward','agsdix-fas fa-balance-scale','agsdix-fas fa-ban','agsdix-fas fa-band-aid','agsdix-fab fa-bandcamp','agsdix-fas fa-barcode','agsdix-fas fa-bars','agsdix-fas fa-baseball-ball','agsdix-fas fa-basketball-ball','agsdix-fas fa-bath','agsdix-fas fa-battery-empty','agsdix-fas fa-battery-full','agsdix-fas fa-battery-half','agsdix-fas fa-battery-quarter','agsdix-fas fa-battery-three-quarters','agsdix-fas fa-bed','agsdix-fas fa-beer','agsdix-fab fa-behance','agsdix-fab fa-behance-square','agsdix-far fa-bell','agsdix-fas fa-bell','agsdix-fas fa-bell-slash','agsdix-far fa-bell-slash','agsdix-fas fa-bezier-curve','agsdix-fas fa-bicycle','agsdix-fab fa-bimobject','agsdix-fas fa-binoculars','agsdix-fas fa-birthday-cake','agsdix-fab fa-bitbucket','agsdix-fab fa-bitcoin','agsdix-fab fa-bity','agsdix-fab fa-black-tie','agsdix-fab fa-blackberry','agsdix-fas fa-blender','agsdix-fas fa-blind','agsdix-fab fa-blogger','agsdix-fab fa-blogger-b','agsdix-fab fa-bluetooth','agsdix-fab fa-bluetooth-b','agsdix-fas fa-bold','agsdix-fas fa-bolt','agsdix-fas fa-bomb','agsdix-fas fa-bone','agsdix-fas fa-bong','agsdix-fas fa-book','agsdix-fas fa-book-open','agsdix-fas fa-book-reader','agsdix-far fa-bookmark','agsdix-fas fa-bookmark','agsdix-fas fa-bowling-ball','agsdix-fas fa-box','agsdix-fas fa-box-open','agsdix-fas fa-boxes','agsdix-fas fa-braille','agsdix-fas fa-brain','agsdix-fas fa-briefcase','agsdix-fas fa-briefcase-medical','agsdix-fas fa-broadcast-tower','agsdix-fas fa-broom','agsdix-fas fa-brush','agsdix-fab fa-btc','agsdix-fas fa-bug','agsdix-far fa-building','agsdix-fas fa-building','agsdix-fas fa-bullhorn','agsdix-fas fa-bullseye','agsdix-fas fa-burn','agsdix-fab fa-buromobelexperte','agsdix-fas fa-bus','agsdix-fas fa-bus-alt','agsdix-fab fa-buysellads','agsdix-fas fa-calculator','agsdix-fas fa-calendar','agsdix-far fa-calendar','agsdix-fas fa-calendar-alt','agsdix-far fa-calendar-alt','agsdix-fas fa-calendar-check','agsdix-far fa-calendar-check','agsdix-far fa-calendar-minus','agsdix-fas fa-calendar-minus','agsdix-far fa-calendar-plus','agsdix-fas fa-calendar-plus','agsdix-far fa-calendar-times','agsdix-fas fa-calendar-times','agsdix-fas fa-camera','agsdix-fas fa-camera-retro','agsdix-fas fa-cannabis','agsdix-fas fa-capsules','agsdix-fas fa-car','agsdix-fas fa-car-alt','agsdix-fas fa-car-battery','agsdix-fas fa-car-crash','agsdix-fas fa-car-side','agsdix-fas fa-caret-down','agsdix-fas fa-caret-left','agsdix-fas fa-caret-right','agsdix-fas fa-caret-square-down','agsdix-far fa-caret-square-down','agsdix-far fa-caret-square-left','agsdix-fas fa-caret-square-left','agsdix-far fa-caret-square-right','agsdix-fas fa-caret-square-right','agsdix-far fa-caret-square-up','agsdix-fas fa-caret-square-up','agsdix-fas fa-caret-up','agsdix-fas fa-cart-arrow-down','agsdix-fas fa-cart-plus','agsdix-fab fa-cc-amazon-pay','agsdix-fab fa-cc-amex','agsdix-fab fa-cc-apple-pay','agsdix-fab fa-cc-diners-club','agsdix-fab fa-cc-discover','agsdix-fab fa-cc-jcb','agsdix-fab fa-cc-mastercard','agsdix-fab fa-cc-paypal','agsdix-fab fa-cc-stripe','agsdix-fab fa-cc-visa','agsdix-fab fa-centercode','agsdix-fas fa-certificate','agsdix-fas fa-chalkboard','agsdix-fas fa-chalkboard-teacher','agsdix-fas fa-charging-station','agsdix-fas fa-chart-area','agsdix-fas fa-chart-bar','agsdix-far fa-chart-bar','agsdix-fas fa-chart-line','agsdix-fas fa-chart-pie','agsdix-fas fa-check','agsdix-fas fa-check-circle','agsdix-far fa-check-circle','agsdix-fas fa-check-double','agsdix-far fa-check-square','agsdix-fas fa-check-square','agsdix-fas fa-chess','agsdix-fas fa-chess-bishop','agsdix-fas fa-chess-board','agsdix-fas fa-chess-king','agsdix-fas fa-chess-knight','agsdix-fas fa-chess-pawn','agsdix-fas fa-chess-queen','agsdix-fas fa-chess-rook','agsdix-fas fa-chevron-circle-down','agsdix-fas fa-chevron-circle-left','agsdix-fas fa-chevron-circle-right','agsdix-fas fa-chevron-circle-up','agsdix-fas fa-chevron-down','agsdix-fas fa-chevron-left','agsdix-fas fa-chevron-right','agsdix-fas fa-chevron-up','agsdix-fas fa-child','agsdix-fab fa-chrome','agsdix-fas fa-church','agsdix-fas fa-circle','agsdix-far fa-circle','agsdix-fas fa-circle-notch','agsdix-far fa-clipboard','agsdix-fas fa-clipboard','agsdix-fas fa-clipboard-check','agsdix-fas fa-clipboard-list','agsdix-far fa-clock','agsdix-fas fa-clock','agsdix-far fa-clone','agsdix-fas fa-clone','agsdix-fas fa-closed-captioning','agsdix-far fa-closed-captioning','agsdix-fas fa-cloud','agsdix-fas fa-cloud-download-alt','agsdix-fas fa-cloud-upload-alt','agsdix-fab fa-cloudscale','agsdix-fab fa-cloudsmith','agsdix-fab fa-cloudversify','agsdix-fas fa-cocktail','agsdix-fas fa-code','agsdix-fas fa-code-branch','agsdix-fab fa-codepen','agsdix-fab fa-codiepie','agsdix-fas fa-coffee','agsdix-fas fa-cog','agsdix-fas fa-cogs','agsdix-fas fa-coins','agsdix-fas fa-columns','agsdix-fas fa-comment','agsdix-far fa-comment','agsdix-far fa-comment-alt','agsdix-fas fa-comment-alt','agsdix-fas fa-comment-dots','agsdix-far fa-comment-dots','agsdix-fas fa-comment-slash','agsdix-far fa-comments','agsdix-fas fa-comments','agsdix-fas fa-compact-disc','agsdix-far fa-compass','agsdix-fas fa-compass','agsdix-fas fa-compress','agsdix-fas fa-concierge-bell','agsdix-fab fa-connectdevelop','agsdix-fab fa-contao','agsdix-fas fa-cookie','agsdix-fas fa-cookie-bite','agsdix-far fa-copy','agsdix-fas fa-copy','agsdix-far fa-copyright','agsdix-fas fa-copyright','agsdix-fas fa-couch','agsdix-fab fa-cpanel','agsdix-fab fa-creative-commons','agsdix-fab fa-creative-commons-by','agsdix-fab fa-creative-commons-nc','agsdix-fab fa-creative-commons-nc-eu','agsdix-fab fa-creative-commons-nc-jp','agsdix-fab fa-creative-commons-nd','agsdix-fab fa-creative-commons-pd','agsdix-fab fa-creative-commons-pd-alt','agsdix-fab fa-creative-commons-remix','agsdix-fab fa-creative-commons-sa','agsdix-fab fa-creative-commons-sampling','agsdix-fab fa-creative-commons-sampling-plus','agsdix-fab fa-creative-commons-share','agsdix-far fa-credit-card','agsdix-fas fa-credit-card','agsdix-fas fa-crop','agsdix-fas fa-crop-alt','agsdix-fas fa-crosshairs','agsdix-fas fa-crow','agsdix-fas fa-crown','agsdix-fab fa-css3','agsdix-fab fa-css3-alt','agsdix-fas fa-cube','agsdix-fas fa-cubes','agsdix-fas fa-cut','agsdix-fab fa-cuttlefish','agsdix-fab fa-d-and-d','agsdix-fab fa-dashcube','agsdix-fas fa-database','agsdix-fas fa-deaf','agsdix-fab fa-delicious','agsdix-fab fa-deploydog','agsdix-fab fa-deskpro','agsdix-fas fa-desktop','agsdix-fab fa-deviantart','agsdix-fas fa-diagnoses','agsdix-fas fa-dice','agsdix-fas fa-dice-five','agsdix-fas fa-dice-four','agsdix-fas fa-dice-one','agsdix-fas fa-dice-six','agsdix-fas fa-dice-three','agsdix-fas fa-dice-two','agsdix-fab fa-digg','agsdix-fab fa-digital-ocean','agsdix-fas fa-digital-tachograph','agsdix-fas fa-directions','agsdix-fab fa-discord','agsdix-fab fa-discourse','agsdix-fas fa-divide','agsdix-far fa-dizzy','agsdix-fas fa-dizzy','agsdix-fas fa-dna','agsdix-fab fa-dochub','agsdix-fab fa-docker','agsdix-fas fa-dollar-sign','agsdix-fas fa-dolly','agsdix-fas fa-dolly-flatbed','agsdix-fas fa-donate','agsdix-fas fa-door-closed','agsdix-fas fa-door-open','agsdix-far fa-dot-circle','agsdix-fas fa-dot-circle','agsdix-fas fa-dove','agsdix-fas fa-download','agsdix-fab fa-draft2digital','agsdix-fas fa-drafting-compass','agsdix-fas fa-draw-polygon','agsdix-fab fa-dribbble','agsdix-fab fa-dribbble-square','agsdix-fab fa-dropbox','agsdix-fas fa-drum','agsdix-fas fa-drum-steelpan','agsdix-fab fa-drupal','agsdix-fas fa-dumbbell','agsdix-fab fa-dyalog','agsdix-fab fa-earlybirds','agsdix-fab fa-ebay','agsdix-fab fa-edge','agsdix-fas fa-edit','agsdix-far fa-edit','agsdix-fas fa-eject','agsdix-fab fa-elementor','agsdix-fas fa-ellipsis-h','agsdix-fas fa-ellipsis-v','agsdix-fab fa-ello','agsdix-fab fa-ember','agsdix-fab fa-empire','agsdix-far fa-envelope','agsdix-fas fa-envelope','agsdix-far fa-envelope-open','agsdix-fas fa-envelope-open','agsdix-fas fa-envelope-square','agsdix-fab fa-envira','agsdix-fas fa-equals','agsdix-fas fa-eraser','agsdix-fab fa-erlang','agsdix-fab fa-ethereum','agsdix-fab fa-etsy','agsdix-fas fa-euro-sign','agsdix-fas fa-exchange-alt','agsdix-fas fa-exclamation','agsdix-fas fa-exclamation-circle','agsdix-fas fa-exclamation-triangle','agsdix-fas fa-expand','agsdix-fas fa-expand-arrows-alt','agsdix-fab fa-expeditedssl','agsdix-fas fa-external-link-alt','agsdix-fas fa-external-link-square-alt','agsdix-fas fa-eye','agsdix-far fa-eye','agsdix-fas fa-eye-dropper','agsdix-far fa-eye-slash','agsdix-fas fa-eye-slash','agsdix-fab fa-facebook','agsdix-fab fa-facebook-f','agsdix-fab fa-facebook-messenger','agsdix-fab fa-facebook-square','agsdix-fas fa-fast-backward','agsdix-fas fa-fast-forward','agsdix-fas fa-fax','agsdix-fas fa-feather','agsdix-fas fa-feather-alt','agsdix-fas fa-female','agsdix-fas fa-fighter-jet','agsdix-fas fa-file','agsdix-far fa-file','agsdix-fas fa-file-alt','agsdix-far fa-file-alt','agsdix-far fa-file-archive','agsdix-fas fa-file-archive','agsdix-far fa-file-audio','agsdix-fas fa-file-audio','agsdix-far fa-file-code','agsdix-fas fa-file-code','agsdix-fas fa-file-contract','agsdix-fas fa-file-download','agsdix-far fa-file-excel','agsdix-fas fa-file-excel','agsdix-fas fa-file-export','agsdix-fas fa-file-image','agsdix-far fa-file-image','agsdix-fas fa-file-import','agsdix-fas fa-file-invoice','agsdix-fas fa-file-invoice-dollar','agsdix-fas fa-file-medical','agsdix-fas fa-file-medical-alt','agsdix-fas fa-file-pdf','agsdix-far fa-file-pdf','agsdix-fas fa-file-powerpoint','agsdix-far fa-file-powerpoint','agsdix-fas fa-file-prescription','agsdix-fas fa-file-signature','agsdix-fas fa-file-upload','agsdix-fas fa-file-video','agsdix-far fa-file-video','agsdix-fas fa-file-word','agsdix-far fa-file-word','agsdix-fas fa-fill','agsdix-fas fa-fill-drip','agsdix-fas fa-film','agsdix-fas fa-filter','agsdix-fas fa-fingerprint','agsdix-fas fa-fire','agsdix-fas fa-fire-extinguisher','agsdix-fab fa-firefox','agsdix-fas fa-first-aid','agsdix-fab fa-first-order','agsdix-fab fa-first-order-alt','agsdix-fab fa-firstdraft','agsdix-fas fa-fish','agsdix-fas fa-flag','agsdix-far fa-flag','agsdix-fas fa-flag-checkered','agsdix-fas fa-flask','agsdix-fab fa-flickr','agsdix-fab fa-flipboard','agsdix-fas fa-flushed','agsdix-far fa-flushed','agsdix-fab fa-fly','agsdix-fas fa-folder','agsdix-far fa-folder','agsdix-far fa-folder-open','agsdix-fas fa-folder-open','agsdix-fas fa-font','agsdix-fab fa-font-awesome','agsdix-fab fa-font-awesome-alt','agsdix-fab fa-font-awesome-flag','agsdix-far fa-font-awesome-logo-full','agsdix-fas fa-font-awesome-logo-full','agsdix-fab fa-font-awesome-logo-full','agsdix-fab fa-fonticons','agsdix-fab fa-fonticons-fi','agsdix-fas fa-football-ball','agsdix-fab fa-fort-awesome','agsdix-fab fa-fort-awesome-alt','agsdix-fab fa-forumbee','agsdix-fas fa-forward','agsdix-fab fa-foursquare','agsdix-fab fa-free-code-camp','agsdix-fab fa-freebsd','agsdix-fas fa-frog','agsdix-far fa-frown','agsdix-fas fa-frown','agsdix-fas fa-frown-open','agsdix-far fa-frown-open','agsdix-fab fa-fulcrum','agsdix-fas fa-futbol','agsdix-far fa-futbol','agsdix-fab fa-galactic-republic','agsdix-fab fa-galactic-senate','agsdix-fas fa-gamepad','agsdix-fas fa-gas-pump','agsdix-fas fa-gavel','agsdix-far fa-gem','agsdix-fas fa-gem','agsdix-fas fa-genderless','agsdix-fab fa-get-pocket','agsdix-fab fa-gg','agsdix-fab fa-gg-circle','agsdix-fas fa-gift','agsdix-fab fa-git','agsdix-fab fa-git-square','agsdix-fab fa-github','agsdix-fab fa-github-alt','agsdix-fab fa-github-square','agsdix-fab fa-gitkraken','agsdix-fab fa-gitlab','agsdix-fab fa-gitter','agsdix-fas fa-glass-martini','agsdix-fas fa-glass-martini-alt','agsdix-fas fa-glasses','agsdix-fab fa-glide','agsdix-fab fa-glide-g','agsdix-fas fa-globe','agsdix-fas fa-globe-africa','agsdix-fas fa-globe-americas','agsdix-fas fa-globe-asia','agsdix-fab fa-gofore','agsdix-fas fa-golf-ball','agsdix-fab fa-goodreads','agsdix-fab fa-goodreads-g','agsdix-fab fa-google','agsdix-fab fa-google-drive','agsdix-fab fa-google-play','agsdix-fab fa-google-plus','agsdix-fab fa-google-plus-g','agsdix-fab fa-google-plus-square','agsdix-fab fa-google-wallet','agsdix-fas fa-graduation-cap','agsdix-fab fa-gratipay','agsdix-fab fa-grav','agsdix-fas fa-greater-than','agsdix-fas fa-greater-than-equal','agsdix-fas fa-grimace','agsdix-far fa-grimace','agsdix-far fa-grin','agsdix-fas fa-grin','agsdix-far fa-grin-alt','agsdix-fas fa-grin-alt','agsdix-fas fa-grin-beam','agsdix-far fa-grin-beam','agsdix-fas fa-grin-beam-sweat','agsdix-far fa-grin-beam-sweat','agsdix-far fa-grin-hearts','agsdix-fas fa-grin-hearts','agsdix-fas fa-grin-squint','agsdix-far fa-grin-squint','agsdix-far fa-grin-squint-tears','agsdix-fas fa-grin-squint-tears','agsdix-far fa-grin-stars','agsdix-fas fa-grin-stars','agsdix-far fa-grin-tears','agsdix-fas fa-grin-tears','agsdix-far fa-grin-tongue','agsdix-fas fa-grin-tongue','agsdix-far fa-grin-tongue-squint','agsdix-fas fa-grin-tongue-squint','agsdix-fas fa-grin-tongue-wink','agsdix-far fa-grin-tongue-wink','agsdix-far fa-grin-wink','agsdix-fas fa-grin-wink','agsdix-fas fa-grip-horizontal','agsdix-fas fa-grip-vertical','agsdix-fab fa-gripfire','agsdix-fab fa-grunt','agsdix-fab fa-gulp','agsdix-fas fa-h-square','agsdix-fab fa-hacker-news','agsdix-fab fa-hacker-news-square','agsdix-fab fa-hackerrank','agsdix-fas fa-hand-holding','agsdix-fas fa-hand-holding-heart','agsdix-fas fa-hand-holding-usd','agsdix-fas fa-hand-lizard','agsdix-far fa-hand-lizard','agsdix-fas fa-hand-paper','agsdix-far fa-hand-paper','agsdix-far fa-hand-peace','agsdix-fas fa-hand-peace','agsdix-fas fa-hand-point-down','agsdix-far fa-hand-point-down','agsdix-fas fa-hand-point-left','agsdix-far fa-hand-point-left','agsdix-fas fa-hand-point-right','agsdix-far fa-hand-point-right','agsdix-far fa-hand-point-up','agsdix-fas fa-hand-point-up','agsdix-fas fa-hand-pointer','agsdix-far fa-hand-pointer','agsdix-far fa-hand-rock','agsdix-fas fa-hand-rock','agsdix-far fa-hand-scissors','agsdix-fas fa-hand-scissors','agsdix-far fa-hand-spock','agsdix-fas fa-hand-spock','agsdix-fas fa-hands','agsdix-fas fa-hands-helping','agsdix-fas fa-handshake','agsdix-far fa-handshake','agsdix-fas fa-hashtag','agsdix-far fa-hdd','agsdix-fas fa-hdd','agsdix-fas fa-heading','agsdix-fas fa-headphones','agsdix-fas fa-headphones-alt','agsdix-fas fa-headset','agsdix-far fa-heart','agsdix-fas fa-heart','agsdix-fas fa-heartbeat','agsdix-fas fa-helicopter','agsdix-fas fa-highlighter','agsdix-fab fa-hips','agsdix-fab fa-hire-a-helper','agsdix-fas fa-history','agsdix-fas fa-hockey-puck','agsdix-fas fa-home','agsdix-fab fa-hooli','agsdix-fab fa-hornbill','agsdix-fas fa-hospital','agsdix-far fa-hospital','agsdix-fas fa-hospital-alt','agsdix-fas fa-hospital-symbol','agsdix-fas fa-hot-tub','agsdix-fas fa-hotel','agsdix-fab fa-hotjar','agsdix-fas fa-hourglass','agsdix-far fa-hourglass','agsdix-fas fa-hourglass-end','agsdix-fas fa-hourglass-half','agsdix-fas fa-hourglass-start','agsdix-fab fa-houzz','agsdix-fab fa-html5','agsdix-fab fa-hubspot','agsdix-fas fa-i-cursor','agsdix-far fa-id-badge','agsdix-fas fa-id-badge','agsdix-far fa-id-card','agsdix-fas fa-id-card','agsdix-fas fa-id-card-alt','agsdix-far fa-image','agsdix-fas fa-image','agsdix-fas fa-images','agsdix-far fa-images','agsdix-fab fa-imdb','agsdix-fas fa-inbox','agsdix-fas fa-indent','agsdix-fas fa-industry','agsdix-fas fa-infinity','agsdix-fas fa-info','agsdix-fas fa-info-circle','agsdix-fab fa-instagram','agsdix-fab fa-internet-explorer','agsdix-fab fa-ioxhost','agsdix-fas fa-italic','agsdix-fab fa-itunes','agsdix-fab fa-itunes-note','agsdix-fab fa-java','agsdix-fab fa-jedi-order','agsdix-fab fa-jenkins','agsdix-fab fa-joget','agsdix-fas fa-joint','agsdix-fab fa-joomla','agsdix-fab fa-js','agsdix-fab fa-js-square','agsdix-fab fa-jsfiddle','agsdix-fab fa-kaggle','agsdix-fas fa-key','agsdix-fab fa-keybase','agsdix-fas fa-keyboard','agsdix-far fa-keyboard','agsdix-fab fa-keycdn','agsdix-fab fa-kickstarter','agsdix-fab fa-kickstarter-k','agsdix-far fa-kiss','agsdix-fas fa-kiss','agsdix-fas fa-kiss-beam','agsdix-far fa-kiss-beam','agsdix-fas fa-kiss-wink-heart','agsdix-far fa-kiss-wink-heart','agsdix-fas fa-kiwi-bird','agsdix-fab fa-korvue','agsdix-fas fa-language','agsdix-fas fa-laptop','agsdix-fas fa-laptop-code','agsdix-fab fa-laravel','agsdix-fab fa-lastfm','agsdix-fab fa-lastfm-square','agsdix-far fa-laugh','agsdix-fas fa-laugh','agsdix-fas fa-laugh-beam','agsdix-far fa-laugh-beam','agsdix-far fa-laugh-squint','agsdix-fas fa-laugh-squint','agsdix-fas fa-laugh-wink','agsdix-far fa-laugh-wink','agsdix-fas fa-layer-group','agsdix-fas fa-leaf','agsdix-fab fa-leanpub','agsdix-far fa-lemon','agsdix-fas fa-lemon','agsdix-fab fa-less','agsdix-fas fa-less-than','agsdix-fas fa-less-than-equal','agsdix-fas fa-level-down-alt','agsdix-fas fa-level-up-alt','agsdix-far fa-life-ring','agsdix-fas fa-life-ring','agsdix-fas fa-lightbulb','agsdix-far fa-lightbulb','agsdix-fab fa-line','agsdix-fas fa-link','agsdix-fab fa-linkedin','agsdix-fab fa-linkedin-in','agsdix-fab fa-linode','agsdix-fab fa-linux','agsdix-fas fa-lira-sign','agsdix-fas fa-list','agsdix-far fa-list-alt','agsdix-fas fa-list-alt','agsdix-fas fa-list-ol','agsdix-fas fa-list-ul','agsdix-fas fa-location-arrow','agsdix-fas fa-lock','agsdix-fas fa-lock-open','agsdix-fas fa-long-arrow-alt-down','agsdix-fas fa-long-arrow-alt-left','agsdix-fas fa-long-arrow-alt-right','agsdix-fas fa-long-arrow-alt-up','agsdix-fas fa-low-vision','agsdix-fas fa-luggage-cart','agsdix-fab fa-lyft','agsdix-fab fa-magento','agsdix-fas fa-magic','agsdix-fas fa-magnet','agsdix-fab fa-mailchimp','agsdix-fas fa-male','agsdix-fab fa-mandalorian','agsdix-far fa-map','agsdix-fas fa-map','agsdix-fas fa-map-marked','agsdix-fas fa-map-marked-alt','agsdix-fas fa-map-marker','agsdix-fas fa-map-marker-alt','agsdix-fas fa-map-pin','agsdix-fas fa-map-signs','agsdix-fab fa-markdown','agsdix-fas fa-marker','agsdix-fas fa-mars','agsdix-fas fa-mars-double','agsdix-fas fa-mars-stroke','agsdix-fas fa-mars-stroke-h','agsdix-fas fa-mars-stroke-v','agsdix-fab fa-mastodon','agsdix-fab fa-maxcdn','agsdix-fas fa-medal','agsdix-fab fa-medapps','agsdix-fab fa-medium','agsdix-fab fa-medium-m','agsdix-fas fa-medkit','agsdix-fab fa-medrt','agsdix-fab fa-meetup','agsdix-fab fa-megaport','agsdix-fas fa-meh','agsdix-far fa-meh','agsdix-fas fa-meh-blank','agsdix-far fa-meh-blank','agsdix-far fa-meh-rolling-eyes','agsdix-fas fa-meh-rolling-eyes','agsdix-fas fa-memory','agsdix-fas fa-mercury','agsdix-fas fa-microchip','agsdix-fas fa-microphone','agsdix-fas fa-microphone-alt','agsdix-fas fa-microphone-alt-slash','agsdix-fas fa-microphone-slash','agsdix-fas fa-microscope','agsdix-fab fa-microsoft','agsdix-fas fa-minus','agsdix-fas fa-minus-circle','agsdix-far fa-minus-square','agsdix-fas fa-minus-square','agsdix-fab fa-mix','agsdix-fab fa-mixcloud','agsdix-fab fa-mizuni','agsdix-fas fa-mobile','agsdix-fas fa-mobile-alt','agsdix-fab fa-modx','agsdix-fab fa-monero','agsdix-fas fa-money-bill','agsdix-far fa-money-bill-alt','agsdix-fas fa-money-bill-alt','agsdix-fas fa-money-bill-wave','agsdix-fas fa-money-bill-wave-alt','agsdix-fas fa-money-check','agsdix-fas fa-money-check-alt','agsdix-fas fa-monument','agsdix-fas fa-moon','agsdix-far fa-moon','agsdix-fas fa-mortar-pestle','agsdix-fas fa-motorcycle','agsdix-fas fa-mouse-pointer','agsdix-fas fa-music','agsdix-fab fa-napster','agsdix-fab fa-neos','agsdix-fas fa-neuter','agsdix-far fa-newspaper','agsdix-fas fa-newspaper','agsdix-fab fa-nimblr','agsdix-fab fa-nintendo-switch','agsdix-fab fa-node','agsdix-fab fa-node-js','agsdix-fas fa-not-equal','agsdix-fas fa-notes-medical','agsdix-fab fa-npm','agsdix-fab fa-ns8','agsdix-fab fa-nutritionix','agsdix-far fa-object-group','agsdix-fas fa-object-group','agsdix-fas fa-object-ungroup','agsdix-far fa-object-ungroup','agsdix-fab fa-odnoklassniki','agsdix-fab fa-odnoklassniki-square','agsdix-fas fa-oil-can','agsdix-fab fa-old-republic','agsdix-fab fa-opencart','agsdix-fab fa-openid','agsdix-fab fa-opera','agsdix-fab fa-optin-monster','agsdix-fab fa-osi','agsdix-fas fa-outdent','agsdix-fab fa-page4','agsdix-fab fa-pagelines','agsdix-fas fa-paint-brush','agsdix-fas fa-paint-roller','agsdix-fas fa-palette','agsdix-fab fa-palfed','agsdix-fas fa-pallet','agsdix-fas fa-paper-plane','agsdix-far fa-paper-plane','agsdix-fas fa-paperclip','agsdix-fas fa-parachute-box','agsdix-fas fa-paragraph','agsdix-fas fa-parking','agsdix-fas fa-passport','agsdix-fas fa-paste','agsdix-fab fa-patreon','agsdix-fas fa-pause','agsdix-fas fa-pause-circle','agsdix-far fa-pause-circle','agsdix-fas fa-paw','agsdix-fab fa-paypal','agsdix-fas fa-pen','agsdix-fas fa-pen-alt','agsdix-fas fa-pen-fancy','agsdix-fas fa-pen-nib','agsdix-fas fa-pen-square','agsdix-fas fa-pencil-alt','agsdix-fas fa-pencil-ruler','agsdix-fas fa-people-carry','agsdix-fas fa-percent','agsdix-fas fa-percentage','agsdix-fab fa-periscope','agsdix-fab fa-phabricator','agsdix-fab fa-phoenix-framework','agsdix-fab fa-phoenix-squadron','agsdix-fas fa-phone','agsdix-fas fa-phone-slash','agsdix-fas fa-phone-square','agsdix-fas fa-phone-volume','agsdix-fab fa-php','agsdix-fab fa-pied-piper','agsdix-fab fa-pied-piper-alt','agsdix-fab fa-pied-piper-hat','agsdix-fab fa-pied-piper-pp','agsdix-fas fa-piggy-bank','agsdix-fas fa-pills','agsdix-fab fa-pinterest','agsdix-fab fa-pinterest-p','agsdix-fab fa-pinterest-square','agsdix-fas fa-plane','agsdix-fas fa-plane-arrival','agsdix-fas fa-plane-departure','agsdix-fas fa-play','agsdix-far fa-play-circle','agsdix-fas fa-play-circle','agsdix-fab fa-playstation','agsdix-fas fa-plug','agsdix-fas fa-plus','agsdix-fas fa-plus-circle','agsdix-fas fa-plus-square','agsdix-far fa-plus-square','agsdix-fas fa-podcast','agsdix-fas fa-poo','agsdix-fas fa-poop','agsdix-fas fa-portrait','agsdix-fas fa-pound-sign','agsdix-fas fa-power-off','agsdix-fas fa-prescription','agsdix-fas fa-prescription-bottle','agsdix-fas fa-prescription-bottle-alt','agsdix-fas fa-print','agsdix-fas fa-procedures','agsdix-fab fa-product-hunt','agsdix-fas fa-project-diagram','agsdix-fab fa-pushed','agsdix-fas fa-puzzle-piece','agsdix-fab fa-python','agsdix-fab fa-qq','agsdix-fas fa-qrcode','agsdix-fas fa-question','agsdix-far fa-question-circle','agsdix-fas fa-question-circle','agsdix-fas fa-quidditch','agsdix-fab fa-quinscape','agsdix-fab fa-quora','agsdix-fas fa-quote-left','agsdix-fas fa-quote-right','agsdix-fab fa-r-project','agsdix-fas fa-random','agsdix-fab fa-ravelry','agsdix-fab fa-react','agsdix-fab fa-readme','agsdix-fab fa-rebel','agsdix-fas fa-receipt','agsdix-fas fa-recycle','agsdix-fab fa-red-river','agsdix-fab fa-reddit','agsdix-fab fa-reddit-alien','agsdix-fab fa-reddit-square','agsdix-fas fa-redo','agsdix-fas fa-redo-alt','agsdix-fas fa-registered','agsdix-far fa-registered','agsdix-fab fa-rendact','agsdix-fab fa-renren','agsdix-fas fa-reply','agsdix-fas fa-reply-all','agsdix-fab fa-replyd','agsdix-fab fa-researchgate','agsdix-fab fa-resolving','agsdix-fas fa-retweet','agsdix-fab fa-rev','agsdix-fas fa-ribbon','agsdix-fas fa-road','agsdix-fas fa-robot','agsdix-fas fa-rocket','agsdix-fab fa-rocketchat','agsdix-fab fa-rockrms','agsdix-fas fa-route','agsdix-fas fa-rss','agsdix-fas fa-rss-square','agsdix-fas fa-ruble-sign','agsdix-fas fa-ruler','agsdix-fas fa-ruler-combined','agsdix-fas fa-ruler-horizontal','agsdix-fas fa-ruler-vertical','agsdix-fas fa-rupee-sign','agsdix-far fa-sad-cry','agsdix-fas fa-sad-cry','agsdix-fas fa-sad-tear','agsdix-far fa-sad-tear','agsdix-fab fa-safari','agsdix-fab fa-sass','agsdix-fas fa-save','agsdix-far fa-save','agsdix-fab fa-schlix','agsdix-fas fa-school','agsdix-fas fa-screwdriver','agsdix-fab fa-scribd','agsdix-fas fa-search','agsdix-fas fa-search-minus','agsdix-fas fa-search-plus','agsdix-fab fa-searchengin','agsdix-fas fa-seedling','agsdix-fab fa-sellcast','agsdix-fab fa-sellsy','agsdix-fas fa-server','agsdix-fab fa-servicestack','agsdix-fas fa-shapes','agsdix-fas fa-share','agsdix-fas fa-share-alt','agsdix-fas fa-share-alt-square','agsdix-far fa-share-square','agsdix-fas fa-share-square','agsdix-fas fa-shekel-sign','agsdix-fas fa-shield-alt','agsdix-fas fa-ship','agsdix-fas fa-shipping-fast','agsdix-fab fa-shirtsinbulk','agsdix-fas fa-shoe-prints','agsdix-fas fa-shopping-bag','agsdix-fas fa-shopping-basket','agsdix-fas fa-shopping-cart','agsdix-fab fa-shopware','agsdix-fas fa-shower','agsdix-fas fa-shuttle-van','agsdix-fas fa-sign','agsdix-fas fa-sign-in-alt','agsdix-fas fa-sign-language','agsdix-fas fa-sign-out-alt','agsdix-fas fa-signal','agsdix-fas fa-signature','agsdix-fab fa-simplybuilt','agsdix-fab fa-sistrix','agsdix-fas fa-sitemap','agsdix-fab fa-sith','agsdix-fas fa-skull','agsdix-fab fa-skyatlas','agsdix-fab fa-skype','agsdix-fab fa-slack','agsdix-fab fa-slack-hash','agsdix-fas fa-sliders-h','agsdix-fab fa-slideshare','agsdix-far fa-smile','agsdix-fas fa-smile','agsdix-far fa-smile-beam','agsdix-fas fa-smile-beam','agsdix-fas fa-smile-wink','agsdix-far fa-smile-wink','agsdix-fas fa-smoking','agsdix-fas fa-smoking-ban','agsdix-fab fa-snapchat','agsdix-fab fa-snapchat-ghost','agsdix-fab fa-snapchat-square','agsdix-far fa-snowflake','agsdix-fas fa-snowflake','agsdix-fas fa-solar-panel','agsdix-fas fa-sort','agsdix-fas fa-sort-alpha-down','agsdix-fas fa-sort-alpha-up','agsdix-fas fa-sort-amount-down','agsdix-fas fa-sort-amount-up','agsdix-fas fa-sort-down','agsdix-fas fa-sort-numeric-down','agsdix-fas fa-sort-numeric-up','agsdix-fas fa-sort-up','agsdix-fab fa-soundcloud','agsdix-fas fa-spa','agsdix-fas fa-space-shuttle','agsdix-fab fa-speakap','agsdix-fas fa-spinner','agsdix-fas fa-splotch','agsdix-fab fa-spotify','agsdix-fas fa-spray-can','agsdix-far fa-square','agsdix-fas fa-square','agsdix-fas fa-square-full','agsdix-fab fa-squarespace','agsdix-fab fa-stack-exchange','agsdix-fab fa-stack-overflow','agsdix-fas fa-stamp','agsdix-fas fa-star','agsdix-far fa-star','agsdix-fas fa-star-half','agsdix-far fa-star-half','agsdix-fas fa-star-half-alt','agsdix-fas fa-star-of-life','agsdix-fab fa-staylinked','agsdix-fab fa-steam','agsdix-fab fa-steam-square','agsdix-fab fa-steam-symbol','agsdix-fas fa-step-backward','agsdix-fas fa-step-forward','agsdix-fas fa-stethoscope','agsdix-fab fa-sticker-mule','agsdix-fas fa-sticky-note','agsdix-far fa-sticky-note','agsdix-fas fa-stop','agsdix-far fa-stop-circle','agsdix-fas fa-stop-circle','agsdix-fas fa-stopwatch','agsdix-fas fa-store','agsdix-fas fa-store-alt','agsdix-fab fa-strava','agsdix-fas fa-stream','agsdix-fas fa-street-view','agsdix-fas fa-strikethrough','agsdix-fab fa-stripe','agsdix-fab fa-stripe-s','agsdix-fas fa-stroopwafel','agsdix-fab fa-studiovinari','agsdix-fab fa-stumbleupon','agsdix-fab fa-stumbleupon-circle','agsdix-fas fa-subscript','agsdix-fas fa-subway','agsdix-fas fa-suitcase','agsdix-fas fa-suitcase-rolling','agsdix-fas fa-sun','agsdix-far fa-sun','agsdix-fab fa-superpowers','agsdix-fas fa-superscript','agsdix-fab fa-supple','agsdix-far fa-surprise','agsdix-fas fa-surprise','agsdix-fas fa-swatchbook','agsdix-fas fa-swimmer','agsdix-fas fa-swimming-pool','agsdix-fas fa-sync','agsdix-fas fa-sync-alt','agsdix-fas fa-syringe','agsdix-fas fa-table','agsdix-fas fa-table-tennis','agsdix-fas fa-tablet','agsdix-fas fa-tablet-alt','agsdix-fas fa-tablets','agsdix-fas fa-tachometer-alt','agsdix-fas fa-tag','agsdix-fas fa-tags','agsdix-fas fa-tape','agsdix-fas fa-tasks','agsdix-fas fa-taxi','agsdix-fab fa-teamspeak','agsdix-fas fa-teeth','agsdix-fas fa-teeth-open','agsdix-fab fa-telegram','agsdix-fab fa-telegram-plane','agsdix-fab fa-tencent-weibo','agsdix-fas fa-terminal','agsdix-fas fa-text-height','agsdix-fas fa-text-width','agsdix-fas fa-th','agsdix-fas fa-th-large','agsdix-fas fa-th-list','agsdix-fas fa-theater-masks','agsdix-fab fa-themeco','agsdix-fab fa-themeisle','agsdix-fas fa-thermometer','agsdix-fas fa-thermometer-empty','agsdix-fas fa-thermometer-full','agsdix-fas fa-thermometer-half','agsdix-fas fa-thermometer-quarter','agsdix-fas fa-thermometer-three-quarters','agsdix-fas fa-thumbs-down','agsdix-far fa-thumbs-down','agsdix-fas fa-thumbs-up','agsdix-far fa-thumbs-up','agsdix-fas fa-thumbtack','agsdix-fas fa-ticket-alt','agsdix-fas fa-times','agsdix-far fa-times-circle','agsdix-fas fa-times-circle','agsdix-fas fa-tint','agsdix-fas fa-tint-slash','agsdix-fas fa-tired','agsdix-far fa-tired','agsdix-fas fa-toggle-off','agsdix-fas fa-toggle-on','agsdix-fas fa-toolbox','agsdix-fas fa-tooth','agsdix-fab fa-trade-federation','agsdix-fas fa-trademark','agsdix-fas fa-traffic-light','agsdix-fas fa-train','agsdix-fas fa-transgender','agsdix-fas fa-transgender-alt','agsdix-fas fa-trash','agsdix-far fa-trash-alt','agsdix-fas fa-trash-alt','agsdix-fas fa-tree','agsdix-fab fa-trello','agsdix-fab fa-tripadvisor','agsdix-fas fa-trophy','agsdix-fas fa-truck','agsdix-fas fa-truck-loading','agsdix-fas fa-truck-monster','agsdix-fas fa-truck-moving','agsdix-fas fa-truck-pickup','agsdix-fas fa-tshirt','agsdix-fas fa-tty','agsdix-fab fa-tumblr','agsdix-fab fa-tumblr-square','agsdix-fas fa-tv','agsdix-fab fa-twitch','agsdix-fab fa-twitter','agsdix-fab fa-twitter-square','agsdix-fab fa-typo3','agsdix-fab fa-uber','agsdix-fab fa-uikit','agsdix-fas fa-umbrella','agsdix-fas fa-umbrella-beach','agsdix-fas fa-underline','agsdix-fas fa-undo','agsdix-fas fa-undo-alt','agsdix-fab fa-uniregistry','agsdix-fas fa-universal-access','agsdix-fas fa-university','agsdix-fas fa-unlink','agsdix-fas fa-unlock','agsdix-fas fa-unlock-alt','agsdix-fab fa-untappd','agsdix-fas fa-upload','agsdix-fab fa-usb','agsdix-far fa-user','agsdix-fas fa-user','agsdix-fas fa-user-alt','agsdix-fas fa-user-alt-slash','agsdix-fas fa-user-astronaut','agsdix-fas fa-user-check','agsdix-far fa-user-circle','agsdix-fas fa-user-circle','agsdix-fas fa-user-clock','agsdix-fas fa-user-cog','agsdix-fas fa-user-edit','agsdix-fas fa-user-friends','agsdix-fas fa-user-graduate','agsdix-fas fa-user-lock','agsdix-fas fa-user-md','agsdix-fas fa-user-minus','agsdix-fas fa-user-ninja','agsdix-fas fa-user-plus','agsdix-fas fa-user-secret','agsdix-fas fa-user-shield','agsdix-fas fa-user-slash','agsdix-fas fa-user-tag','agsdix-fas fa-user-tie','agsdix-fas fa-user-times','agsdix-fas fa-users','agsdix-fas fa-users-cog','agsdix-fab fa-ussunnah','agsdix-fas fa-utensil-spoon','agsdix-fas fa-utensils','agsdix-fab fa-vaadin','agsdix-fas fa-vector-square','agsdix-fas fa-venus','agsdix-fas fa-venus-double','agsdix-fas fa-venus-mars','agsdix-fab fa-viacoin','agsdix-fab fa-viadeo','agsdix-fab fa-viadeo-square','agsdix-fas fa-vial','agsdix-fas fa-vials','agsdix-fab fa-viber','agsdix-fas fa-video','agsdix-fas fa-video-slash','agsdix-fab fa-vimeo','agsdix-fab fa-vimeo-square','agsdix-fab fa-vimeo-v','agsdix-fab fa-vine','agsdix-fab fa-vk','agsdix-fab fa-vnv','agsdix-fas fa-volleyball-ball','agsdix-fas fa-volume-down','agsdix-fas fa-volume-off','agsdix-fas fa-volume-up','agsdix-fab fa-vuejs','agsdix-fas fa-walking','agsdix-fas fa-wallet','agsdix-fas fa-warehouse','agsdix-fab fa-weebly','agsdix-fab fa-weibo','agsdix-fas fa-weight','agsdix-fas fa-weight-hanging','agsdix-fab fa-weixin','agsdix-fab fa-whatsapp','agsdix-fab fa-whatsapp-square','agsdix-fas fa-wheelchair','agsdix-fab fa-whmcs','agsdix-fas fa-wifi','agsdix-fab fa-wikipedia-w','agsdix-fas fa-window-close','agsdix-far fa-window-close','agsdix-far fa-window-maximize','agsdix-fas fa-window-maximize','agsdix-fas fa-window-minimize','agsdix-far fa-window-minimize','agsdix-fas fa-window-restore','agsdix-far fa-window-restore','agsdix-fab fa-windows','agsdix-fas fa-wine-glass','agsdix-fas fa-wine-glass-alt','agsdix-fab fa-wix','agsdix-fab fa-wolf-pack-battalion','agsdix-fas fa-won-sign','agsdix-fab fa-wordpress','agsdix-fab fa-wordpress-simple','agsdix-fab fa-wpbeginner','agsdix-fab fa-wpexplorer','agsdix-fab fa-wpforms','agsdix-fas fa-wrench','agsdix-fas fa-x-ray','agsdix-fab fa-xbox','agsdix-fab fa-xing','agsdix-fab fa-xing-square','agsdix-fab fa-y-combinator','agsdix-fab fa-yahoo','agsdix-fab fa-yandex','agsdix-fab fa-yandex-international','agsdix-fab fa-yelp','agsdix-fas fa-yen-sign','agsdix-fab fa-yoast','agsdix-fab fa-youtube','agsdix-fab fa-youtube-square','agsdix-fab fa-zhihu');
	}
	
	public static function adminScripts() {
		wp_enqueue_style('ags-divi-icons-admin', self::$pluginDirUrl.'/css/admin.css', array(), self::PLUGIN_VERSION);
		wp_enqueue_script('ags-divi-icons-admin', self::$pluginDirUrl.'/js/admin.js', array('jquery'), self::PLUGIN_VERSION);
		//wp_enqueue_script('ags-divi-icons-tinymce', self::$pluginDirUrl.'/js/tinymce-plugin.js', array('tinymce'), self::PLUGIN_VERSION);
		wp_localize_script('ags-divi-icons-admin', 'ags_divi_icons_tinymce_config', array(
			'multiColorMessage' => sprintf(esc_html__('You have selected a multi-color icon, which cannot be colorized in the editor. Instead, you can define color schemes for multi-color icons %shere%s.', 'ds-icon-expansion'),
												'<a href="'.esc_url(admin_url('admin.php?page=ds-icon-expansion')).'" target="_blank">',
												'</a>'
											),
			'styleInheritMessage' => esc_html__('If you leave the color and/or size settings blank, the icon will derive its color and size from the surrounding text\'s color and size (based on the styling of the icon\'s parent element). This is not reflected in the icon preview.', 'ds-icon-expansion')
		));
		wp_localize_script('ags-divi-icons-admin', 'ags_divi_icons_credit_promos', self::getCreditPromos('icon-picker'));
		
		
		global $pagenow;
		if (isset($pagenow) && $pagenow == 'admin.php') {
			wp_enqueue_style('wp-color-picker');
			wp_enqueue_script('wp-color-picker');
		}
	}
	
	public static function mcePlugins($plugins) {
		$plugins['agsdi_icons'] = self::$pluginDirUrl.'/js/tinymce-plugin.js';
		return $plugins;
	}
	
	public static function mceButtons($toolbarButtons) {
		$toolbarButtons[] = 'agsdi_icons';
		return $toolbarButtons;
	}
	
	public static function mceStyles($styles) {
		$styles .= (empty($styles) ? '' : ',').self::$pluginDirUrl.'/css/icons.css,'.self::$pluginDirUrl.'/icon-packs/fontawesome/css/all-agsdi.min.css';
		return $styles;
	}
	
	public static function getCreditPromos($context, $all=false) {
		/*
		$creditPromos array format:
		First level of the array is requirements (promo only shown if true)
		Second level of the array is exclusions (promo only shown if false)
		Third level of the array is promos themselves
		
		Requirements/exclusions have the following format:
		*  - no requirement/exclusion
		p: - require active plugin / exclude if plugin installed
		t: - require active parent theme (case-insensitive) / exclude if theme installed (case-sensitive, does not check if theme is parent or child)
		c: - require active child theme (case-insensitive) / exclude if theme installed (case-sensitive, does not check if theme is parent or child)
	
		Promos may be specifed as single promo or array of promos
		*/
		$contextSafe = esc_attr($context);
		$utmVars = 'utm_source='.self::PLUGIN_SLUG.'&amp;utm_medium=plugin-ad&amp;utm_content='.$contextSafe.'&amp;utm_campaign=';
		
		$creditPromos = array(
			'*' => array(
				'*' => array(
					'<a href="https://aspengrovestudios.com/?'.$utmVars.'subscribe-general#main-footer" target="_blank">Subscribe</a> to Aspen Grove Studios emails for the latest news, updates, special offers, and more!',
				),
				'p:testify' => 'Create an engaging testimonial section for your website with <a href="https://divi.space/product/testify/?'.$utmVars.'testify" target="_blank">Testify</a>!'
			),
			't:Divi' => array(
				'*' => array(
					'<a href="https://divi.space/?'.$utmVars.'subscribe-general#main-footer" target="_blank">Sign up</a> for emails from <strong>Divi Space</strong> to receive news, updates, special offers, and more!',
					'Get child themes, must-have Divi plugins, &amp; exclusive content with the <a href="https://divi.space/product/annual-membership/?'.$utmVars.'annual-membership" target="_blank">Divi Space membership</a>!',
				),
				'p:divi-switch' => 'Take your Divi website to new heights with <a href="https://divi.space/product/divi-switch/?'.$utmVars.'divi-switch" target="_blank">Divi Switch</a>, the Swiss Army Knife for Divi!',
				'p:ds-divi-extras' => 'Get blog modules from the Extra theme in the Divi Builder with <a href="https://divi.space/product/divi-extras/?'.$utmVars.'divi-extras" target="_blank">Divi Extras</a>!',
				'c:diviecommerce' => 'Create an impactful online presence for your online store with the <a href="https://divi.space/product/divi-ecommerce/?'.$utmVars.'divi-ecommerce" target="_blank">divi ecommerce child theme</a>!',
				'c:divibusinesspro' => 'Showcase your business in a memorable &amp; engaging way with the <a href="https://divi.space/product/divi-business-pro/?'.$utmVars.'divi-business-pro" target="_blank">Divi Business Pro child theme</a>!',
				
			),
			'p:woocommerce' => array(
				'p:hm-product-sales-report-pro' => 'Need a powerful sales reporting tool for WooCommerce? Check out <a href="https://aspengrovestudios.com/product/product-sales-report-pro-for-woocommerce/?'.$utmVars.'product-sales-report-pro" target="_blank">Product Sales Report Pro</a>!',
			),
			'p:bbpress' => array(
				'p:image-upload-for-bbpress-pro' => 'Let your forum users upload images into their posts with <a href="https://aspengrovestudios.com/product/image-upload-for-bbpress-pro/?'.$utmVars.'image-upload-for-bbpress-pro" target="_blank">Image Upload for bbPress Pro</a>!',
			)
		);
		
		$myCreditPromos = array();
		if ($all) {
			$otherPromos = array();
		}
		
		foreach ($creditPromos as $require => $requirePromos) {
			unset($isOtherPromos);
			if ($require != '*') {
				switch ($require[0]) {
					case 'p':
						if (!is_plugin_active(substr($require, 2))) {
							if ($all) {
								$isOtherPromos = true;
							} else {
								continue 2;
							}
						}
						break;
					case 't':
						if (!isset($parentTheme)) {
							$parentTheme = get_template();
						}
						if (strcasecmp($parentTheme, substr($require, 2))) {
							if ($all) {
								$isOtherPromos = true;
							} else {
								continue 2;
							}
						}
						break;
					case 'c':
						if (!isset($childTheme)) {
							$childTheme = get_stylesheet();
						}
						if (strcasecmp($childTheme, substr($require, 2))) {
							if ($all) {
								$isOtherPromos = true;
							} else {
								continue 2;
							}
						}
						break;
					default:
						if ($all) {
							$isOtherPromos = true;
						} else {
							continue 2;
						}
				}
			}
			
			foreach ($requirePromos as $exclude => $promos) {
				if (empty($isOtherPromos)) {
					unset($isExcluded);
					if ($exclude != '*') {
						switch ($exclude[0]) {
							case 'p':
								if (is_dir(self::$pluginDir.'../'.substr($exclude, 2))) {
									if ($all) {
										$isExcluded = true;
									} else {
										continue 2;
									}
								}
								break;
							case 't':
							case 'c':
								if (!isset($themes)) {
									$themes = search_theme_directories();
								}
								if (isset($themes[substr($exclude, 2)])) {
									if ($all) {
										$isExcluded = true;
									} else {
										continue 2;
									}
								}
								break;
							default:
								if ($all) {
									$isExcluded = true;
								} else {
									continue 2;
								}
						}
					}
				}
				
				if (empty($isOtherPromos) && empty($isExcluded)) {
					if (is_array($promos)) {
						$myCreditPromos = array_merge($myCreditPromos, $promos);
					} else {
						$myCreditPromos[] = $promos;
					}
				} else {
					if (is_array($promos)) {
						$otherPromos = array_merge($otherPromos, $promos);
					} else {
						$otherPromos[] = $promos;
					}
				}
				
				
			}
		}
		return $all ? array_merge($myCreditPromos, $otherPromos) : $myCreditPromos;
	}
	
	public static function onPluginFirstActivate() {
		if (class_exists('AGS_Divi_Icons_Pro')) {
			AGS_Divi_Icons_Pro::onPluginFirstActivate();
		}
	}
	
	
	/* Review notice code from Donations for WooCommerce (also adapted for instruction notice) */
	public static function onPluginActivate() {
		$firstActivate = get_option('ds-icon-expansion_first_activate');
		if (empty($firstActivate)) {
			self::onPluginFirstActivate();
			update_option('ds-icon-expansion_first_activate', time());
		}
	}
	public static function instructionNotice() {
		global $pagenow;
		if (empty($pagenow) || $pagenow != 'admin.php' || empty($_GET['page']) || $_GET['page'] != 'ds-icon-expansion') {
			echo('
				<div id="ds-icon-expansion_instruction_notice" class="updated notice is-dismissible"><p>Thank you for installing the <strong>'.esc_html(self::PLUGIN_NAME).'</strong> plugin by <a href="'.esc_url(self::PLUGIN_AUTHOR_URL).'?utm_source=ds-icon-expansion&amp;utm_medium=plugin-credit-link&amp;utm_content=welcome-notice" target="_blank">'.esc_html(self::PLUGIN_AUTHOR).'</a>! <a href="'.esc_url(admin_url('admin.php?page=ds-icon-expansion')).'">Click here</a> for instructions to help you start using it.</p></div>
				<script>jQuery(document).ready(function($){$(\'#ds-icon-expansion_instruction_notice\').on(\'click\', \'.notice-dismiss\', function(){jQuery.post(ajaxurl, {action:\'ds-icon-expansion_instruction_notice_hide\'})});});</script>
			');
		}
	}
	public static function instructionNoticeHide() {
		$settings = get_option('agsdi-icon-expansion');
		if (empty($settings)) {
			$settings = array();
		}
		$settings['notice_instruction_hidden'] = 1;
		update_option('agsdi-icon-expansion', $settings, false);
	}
	public static function reviewNotice() {
		// To do: edit for this plugin
		echo('
			<div id="ds-icon-expansion_review_notice" class="updated notice is-dismissible"><p>Do you use the <strong>Donations for WooCommerce</strong> plugin?
			Please support our free plugin by <a href="https://wordpress.org/support/view/plugin-reviews/'.$slug.'" target="_blank">writing a review</a> and/or <a href="https://potentplugins.com/donate/?utm_source='.$slug.'&amp;utm_medium=link&amp;utm_campaign=wp-plugin-notice-donate-link" target="_blank">making a donation</a>!
			Thanks!</p></div>
			<script>jQuery(document).ready(function($){$(\'#'.$pre.'_review_notice\').on(\'click\', \'.notice-dismiss\', function(){jQuery.post(ajaxurl, {action:\'ds-icon-expansion_review_notice_hide\'})});});</script>
		');
	}
	public static function reviewNoticeHide() {
		$settings = get_option('agsdi-icon-expansion');
		if (empty($settings)) {
			$settings = array();
		}
		$settings['notice_review_hidden'] = 1;
		update_option('agsdi-icon-expansion', $settings, false);
	}
}

@include_once(AGS_Divi_Icons::$pluginDir.'pro/pro.php');

if (class_exists('AGS_Divi_Icons_Pro')) {
	add_action('init', array('AGS_Divi_Icons_Pro', 'init'));
	register_activation_hook(__FILE__, array('AGS_Divi_Icons_Pro', 'onPluginActivate'));
} else {
	add_action('init', array('AGS_Divi_Icons', 'init'));
	register_activation_hook(__FILE__, array('AGS_Divi_Icons', 'onPluginActivate'));
}





/*
Following code is copied from the Divi theme by Elegant Themes (v3.10): includes/builder/functions.php
Licensed under the GNU General Public License version 3 (see license.txt file in plugin root for license text)
Modified on July 13, 2018, in August 2018, and on September 4 and 11, 2018 by Aspen Grove Studios to add content below the icon list
*/
if ( ! function_exists( 'et_pb_get_font_icon_list' ) ) :
function et_pb_get_font_icon_list() {
	$output = is_customize_preview() ? et_pb_get_font_icon_list_items() : '<%= window.et_builder.font_icon_list_template() %>';
	
	$output = sprintf( '<ul class="et_font_icon">%1$s</ul>', $output );

	// Following lines were added
	$output =  '<input type="search" placeholder="Search icons..." class="agsdi-picker-search-divi" oninput="agsdi_search(this);">'
				.$output
				.'<span class="agsdi-picker-credit">'
				.(defined('AGS_DIVI_ICONS_PRO') ? 'With additional icons from <strong>'.AGS_Divi_Icons::PLUGIN_NAME.'</strong> by <a href="'.AGS_Divi_Icons::PLUGIN_AUTHOR_URL.'?utm_source='.AGS_Divi_Icons::PLUGIN_SLUG.'&amp;utm_medium=plugin-credit-link&amp;utm_content=divi-builder" target="_blank">'.AGS_Divi_Icons::PLUGIN_AUTHOR.'</a>' : 'With free icons by <a href="'.AGS_Divi_Icons::PLUGIN_AUTHOR_URL.'?utm_source='.AGS_Divi_Icons::PLUGIN_SLUG.'&amp;utm_medium=plugin-credit-link&amp;utm_content=divi-builder" target="_blank">'.AGS_Divi_Icons::PLUGIN_AUTHOR.'</a><span class="agsdi-picker-credit-promo"></span>')
				.'</span>';
	
	return $output;
}
endif;
/* End code copied from the Divi theme by Elegant Themes */