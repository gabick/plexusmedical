<?php
class AGS_Divi_Icons_Pro extends AGS_Divi_Icons {
	private static $multicolorCssPath, $multicolorCssUrl, $hasMulticolorCss;

	public static function init() {
		define('AGS_DIVI_ICONS_PRO', true);
		parent::init();
		
		self::$multicolorCssPath = self::getIconsUploadDir().'multicolor.css';
		self::$multicolorCssUrl = self::getIconsUploadDir(true).'multicolor.css';
		self::$hasMulticolorCss = file_exists(self::$multicolorCssPath);
		
		if (self::$hasMulticolorCss) {//JH good
			wp_enqueue_style('ags-divi-icons-multicolor', self::$multicolorCssUrl, array(), self::PLUGIN_VERSION);
		}
		
		add_action('wp_ajax_agsdi_colorize_preview', array('AGS_Divi_Icons_Pro', 'colorizePreviewAjax'));
	}
	
	public static function getReplacementColors($colorScheme) {
		require_once(AGS_Divi_Icons::$pluginDir.'vendor/phpColors/src/Mexitek/PHPColors/Color.php');
	
	
		$primary = new \Mexitek\PHPColors\Color($colorScheme[0]);
		$secondary = new \Mexitek\PHPColors\Color($colorScheme[1]);
		$tertiary = new \Mexitek\PHPColors\Color($colorScheme[2]);
		
		return array(
			'#3B42AE' => (string) self::changeColor($primary, array('l' => -0.2, 's' => -0.17)),
			'#6C75E2' => (string) $primary,
			'#7E89F1' => (string) self::changeColor($primary, array('l' => 0.07, 's' => 0.12)),
			'#D1D3D4' => (string) self::changeColor($tertiary, array('l' => -0.08, 's' => 0)),
			'#E6E7E8' => (string) $tertiary,
			'#EE907A' => (string) self::changeColor($secondary, array('l' => -0.2, 's' => -0.2)),
			'#F1F2F2' => (string) self::changeColor($tertiary, array('l' => 0.04, 's' => 0)),
			'#FEB480' => (string) $secondary,
			'#FFCD96' => (string) self::changeColor($secondary, array('l' => 0.05, 's' => 0.1)),
			'#FFFFFF' => '#FFFFFF'
		);
	}
	
	
	public static function changeColor($color, $parameters) {
		$newColor = new \Mexitek\PHPColors\Color((string) $color);
		foreach ($parameters as $parameter => $change) {
			$value = $newColor->$parameter;
			$value += $change;
			if ($value > 1) {
				$value = 1;
			} else if ($value < 0) {
				$value = 0;
			}
			$newColor->$parameter = $value;
		}
		return $newColor;
	}
	
	
	public static function getIconsUploadDir($url=false) {
		$uploadDir = wp_upload_dir(null, false);
		$iconsUploadDir = $uploadDir[$url ? 'baseurl' : 'basedir'].'/aspengrove-icons/';
		if (!$url && !is_dir($iconsUploadDir)) {
			mkdir($iconsUploadDir);
		}
		return $iconsUploadDir;
	}
	
	public static function colorizeSvg($colorScheme, $colorSchemeId, $save=true) {
		$svg = file_get_contents(AGS_Divi_Icons::$pluginDir.'icon-packs/multicolor/multicolor.svg');
		$replacementColors = AGS_Divi_Icons_Pro::getReplacementColors($colorScheme);
		
		$svg = str_replace(array_keys($replacementColors), array_values($replacementColors), $svg);
		
		if ($save) {
			file_put_contents(self::getIconsUploadDir().'multicolor-'.$colorSchemeId.'.svg', $svg);
		} else {
			return $svg;
		}
	}
	
	public static function cleanupMultiColorSvgs($colorSchemeIds) {
		$iconsDir = self::getIconsUploadDir();
		foreach(scandir($iconsDir) as $iconsFile) {
			if (is_file($iconsDir.$iconsFile)) {
				if ($iconsFile == 'multicolor.css') {
					continue;
				}
				foreach ($colorSchemeIds as $id) {
					if ($iconsFile == 'multicolor-'.$id.'.svg') {
						continue 2;
					}
				}
				unlink($iconsDir.$iconsFile);
			}
		}
	}
	
	public static function colorizePreviewAjax() {
		header('Content-Type: image/svg+xml');
		if (isset($_GET['colors']) && is_array($_GET['colors']) && count($_GET['colors']) == 3) {
			echo(self::colorizeSvg($_GET['colors'], null, false));
		}
		exit;
	}
	
	public static function generateMultiColorCss($colorSchemeIds) {
		$multiColorCssFile = self::getIconsUploadDir().'multicolor.css';
		if (empty($colorSchemeIds)) {
			@unlink($multiColorCssFile);
		} else {
			$css = '';
			foreach ($colorSchemeIds as $colorSchemeId) {
				$css .= '*[data-icon^=\'agsdix-smc-\'][data-icon$=\'-'.$colorSchemeId.'\']:before{background-image:url(\'multicolor-'.$colorSchemeId.'.svg\');}';
				$css .= '*[data-icon^=\'agsdix-smc-\'][data-icon$=\'-'.$colorSchemeId.'\']:after{background-image:url(\'multicolor-'.$colorSchemeId.'.svg\');}';
			}
			file_put_contents($multiColorCssFile, $css);
		}
	}
	
	public static function mceStyles($styles) {
		$styles .= parent::mceStyles($styles).','.self::$pluginDirUrl.'/icon-packs/fontawesome/css/all-agsdi.min.css';
		
		if (self::$hasMulticolorCss) {//JH good
			$styles .= ','.self::$multicolorCssUrl;
		}
		
		return $styles;
	}
	
	public static function onPluginFirstActivate() {
		if (get_option('aspengrove_icons_colors') == false) {
			$colorSchemes = array(1 => array('6C75E2', 'FEB480', 'E6E7E8'));
			self::init();
			self::colorizeSvg($colorSchemes[1], 1);
			self::generateMultiColorCss(array(1));
			update_option('aspengrove_icons_colors', $colorSchemes);
		}
	}
}