<?php

// Create Shortcode pbe_section
// Use the shortcode: [pbe_section global_module=""]
function create_pbesection_shortcode($atts) {
    // Attributes
    $atts = shortcode_atts(
        array(
            'global_module' => '',
        ),
        $atts,
        'pbe_section'
    );
    // Attributes in var
    $global_module = $atts['global_module'];

    // Your Code
    $output = do_shortcode('[et_pb_section global_module="'.$global_module.'" /]');

    return $output;
    
}
add_shortcode( 'pbe_section', 'create_pbesection_shortcode' );

?>