<?php
/*
Plugin Name: Page Builder Everywhere
Plugin URI: https://divi.space/product/page-builder-everywhere/
Description: Use the Divi Page Builder to create custom headers, footers and sidebars.
Version: 3.1.1
Author: Divi Space
Author URI: https://divi.space
License: GPL v3 or later
*/

/*

-------------------- INSTALLATION ---------------------

1. Install Plugin as normal
2. Head to the widgets page where you'll find new areas for the header and footer.
3. Add the custom 'Divi Layout' widget to any of the new areas and assign a layout to it.
4. Click on the 'where?' button to add conditional logic to your layout such as only applying
above header areas on the home page or on certain categories.
5. For more info, visit http://aspengrovestudios.helpscoutdocs.com/article/70-page-builder-everywhere

-------------------- LICENSE --------------------

Page Builder Everywhere plugin
Copyright (C) 2018  Aspen Grove Studios

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program (see license.txt file in the plugin's root
directory).  If not, see <https://www.gnu.org/licenses/>.

-------------------- ACKNOWLEDGEMENTS --------------------

Certain aspects of Page Builder Everywhere are built using code found in the following freely available resources:

1. Pippin's simple widget template - https://pippinsplugins.com/simple-wordpress-widget-template/
2. WYSIWYG Widget Blocks - https://wordpress.org/plugins/wysiwyg-widgets/
3. https://wordpress.org/plugins/widget-visibility-without-jetpack/

Page Builder Everywhere includes code that is a derivative work of code from
Gautam Thapar & Elegant Themes, licensed GPLv2.  See gpl-2.0.txt file
in the plugin root directory for license text.

Copied code in this file was modified 2018 and/or earlier by Aspen Grove
Studios and/or Divi Space to implement, customize, and maintain functionality
for the PBE plugin.

Further modifications to this file:
- 2018-12-27 to 2018-12-28: make PBE Layout Inserter load in the frontend builder
- 2018-12-28: improve loading of values for layouts select
- 2018-12-31: implement layouts module; move styling for TinyMCE into pbe-li.css file
- 2019-01-01: implement Replace Main Content widget area; load pbe-li.css in admin
- 2019-01-02: add styling for widget in Replace Main Content area
- 2019-01-09: update version
- 2019-01-10: add frontend builder check to Replace Main Content sidebar
*/


if ( ! defined( 'ABSPATH' ) ) exit;
define('DS_PBE_VERSION', '3.1.1');

require dirname(__FILE__).'/updater/updater.php';

function ds_pbe_init() {
	if (DS_PBE_has_license_key()) {
		include dirname( __FILE__ ) . '/widget-mods/widget-conditions.php';
		include dirname( __FILE__ ) . '/parts/new-widget-areas.php';
		include dirname( __FILE__ ) . '/parts/widget-body-classes.php';
		include dirname( __FILE__ ) . '/parts/pbe-li-shortcode.php';
		include dirname( __FILE__ ) . '/parts/push-widgets.php';
		include dirname( __FILE__ ) . '/parts/pbe-customizer.php';
		
		add_action( 'admin_bar_menu', 'pbe_customizer_shortcut_admin_bar', 998 );
		add_action( 'wp_head', 'pbe_css_edits');
		add_action( 'widgets_init', 'ds_pbe_widgets_init', 20); // Must have a later priority than the Divi Widget Builder plugin's widgets_init hook
		
		
		
		
		add_filter('body_class', 'ds_pbe_body_class', 99);
		
		
		
	}
}
add_action('plugins_loaded', 'ds_pbe_init');


function ds_pbe_body_class($classes) {
	if (is_active_sidebar('pbe-replace-content-wa') && !et_fb_is_enabled()) {
		$sidebarClasses = array('et_left_sidebar', 'et_right_sidebar');
		$classes = array_diff($classes, $sidebarClasses);
		$classes[] = 'et_no_sidebar';
	
		add_action('et_before_main_content', 'ds_pbe_before_main_content');
		add_action('et_after_main_content', 'ds_pbe_after_main_content');
	}
	
	return $classes;
}

function ds_pbe_before_main_content() {
	// The following lines of code (after the closing PHP tag and before the opening PHP tag) are copied from the Divi theme (see credit near the top of this file)
?><div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix"><?php // End copied code (before opening PHP tag)
	dynamic_sidebar('pbe-replace-content-wa');
	ob_start();
}

function ds_pbe_after_main_content() {
	ob_end_clean();
	// The following lines of code (after the closing PHP tag and before the opening PHP tag) are copied from the Divi theme (see credit near the top of this file)
?></div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content --><?php // End copied code (before opening PHP tag)
}

function pbe_customizer_shortcut_admin_bar() {
    
    if ( ! current_user_can( 'customize' ) ) {
        return;
    }
    
    global $wp_admin_bar;
    
    $current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    
    $customize_url = admin_url( 'customize.php?autofocus[panel]=pbe_customizer_options' ); // Direct to Customizer Panel

    $wp_admin_bar->add_menu( array(
            'parent' => 'appearance',
            'id'     => 'launch_pbe',
            'title'  => esc_html__( 'PBE Customizer' ),
            'href'   => $customize_url,
        ) );
    
}

// ------------------------------------------------------------- //

function pbe_css_edits() {

$fix_logo = get_option( 'fix_logo_size' );
$hide_main_header = get_option( 'hide_main_header' );
$hide_above_header_scroll = get_option( 'hide_above_header' );
$hide_bottom_footer = get_option( 'hide_bottom_footer' );

if ( $fix_logo == true ) {
    ?>
    <style type="text/css">
    #logo {
        vertical-align: bottom;
        max-height: 52px;
        margin-bottom: 14px;
    }  
    </style>
    <?php
}

if ( $hide_above_header_scroll == true ) {
    ?>
    <style type="text/css">
    #main-header.et-fixed-header #pbe-above-header-wa-wrap {
        display: none;
    }
    </style>
    <?php
}

if ( $hide_bottom_footer == true ) {
    ?>
    <style type="text/css">
    #footer-bottom {
        display: none;
    }
    #pbe-footer-wa-wrap {
        top: 0 !important;
    }
    </style>
    <?php
}

if ( $hide_main_header == true ) {
    ?>
    <style type="text/css">
    #main-header .container {
        display: none;
    }
    </style>
    <?php
}
?>

<style type="text/css">

/** PBE CSS **/
.page-template-page-template-blank.pbe-above-header #pbe-above-header-wa-wrap,
.page-template-page-template-blank.pbe-below-header #pbe-below-header-wa-wrap,
.page-template-page-template-blank.pbe-footer #pbe-footer-wa-wrap {
    display:none !important;
}
#pbe-above-content-wa-wrap .et_pb_widget {
    display: block;
    width: 100%;
    position: relative;
    /*margin-top: -15px;*/
    margin-bottom: 50px;
}

#pbe-above-content-wa-wrap .et_pb_section {
    z-index: 99;
}

#pbe-below-content-wa-wrap .et_pb_widget {
    display: block;
    width: 100%;
    position: relative;
    /*margin-top: -15px;*/
}

#pbe-below-content-wa-wrap .et_pb_section {
    z-index: 99;
}

#main-header .et_pb_widget, #content-area .et_pb_widget {
    width: 100%;
}

#main-header .et_pb_widget p {
    padding-bottom: 0;
}

#pbe-above-header-wa .widget-conditional-inner {
    background: #fff;
    padding: 0;
    border: none;
}

#pbe-above-header-wa select {
    background: #f1f1f1;
    box-shadow: none;
    border-radius: 3px;
    height: 40px;
    padding-left: 10px;
    padding-right: 10px;
    border: none;
}

#pbe-above-header-wa .et_pb_widget
{
    float:none;
}

#pbe-footer-wa-wrap .et_pb_widget {
    width: 100%;
    display: block;
}

.page-container form input[type=text] {
    display: block;
    margin-bottom: 20px;
    width: 100%;
    background: #f1f1f1;
    padding: 10px 20px;
    box-shadow: none;
    border: none;
    font-weight: 700;
}

.page-container form p {
    font-size: 14px;
}

.page-container form {
    padding: 10px 20px;
}

#pbe-footer-wa-wrap {
    position: relative;
    /*top: -15px;*/
}

#pbe-above-header-wa-wrap,
#pbe-below-header-wa-wrap,
#pbe-above-content-wa-wrap,
#pbe-below-content-wa-wrap,
#pbe-footer-wa-wrap {
	position: relative;
	z-index: 9;
}
/* Fixes issues with overlapping widget areas */
.pbe-above-header #main-header .container,
.pbe-below-content #main-content article,
.pbe-footer #main-footer { 
 	clear: both;
 }
 .pbe-below-content #main-content {
    float: left;
    display: block;
    width: 100%;
}
.pbe-below-content #main-footer {
    float: left;
    width: 100%;
}
</style>
    <?php
}

function ds_pbe_widgets_init() {
	if (!class_exists('Divi_Space_PB_Widget')) {
		if (is_admin()) {
			wp_enqueue_style('ds-pbe-widget-admin', plugins_url('css/divi-pb-widget-admin.css', __FILE__), array(), DS_PBE_VERSION);
		}
		include dirname( __FILE__ ) . '/parts/layout-widget.php';
		register_widget('Divi_Space_PB_Widget');
	}
}

function ds_pbe_admin_menu() {
	$theme = wp_get_theme();
	$adminPage = add_submenu_page( ($theme->Name == 'Divi' || $theme->Template == 'Divi' ? 'et_divi_options' : 'et_extra_options'),
		'Page Builder Everywhere',
		'Page Builder Everywhere',
		'install_plugins',
		'ds-page-builder-everywhere',
		'ds_pbe_admin_page'
	);
	add_action('load-'.$adminPage, 'ds_pbe_admin_page_scripts');
}
add_action('admin_menu', 'ds_pbe_admin_menu', 100);

function ds_pbe_admin_page() {
	if (DS_PBE_has_license_key()) {
		echo('<div class="wrap"><h1>Page Builder Everywhere</h1><h2>About &amp; License Key</h2><div class="wrap">');
		DS_PBE_license_key_box();
		echo('</div>');
	} else {
		DS_PBE_activate_page();
	}
}

function ds_pbe_admin_page_scripts() {
	wp_enqueue_style('ds-pbe-admin', plugins_url('css/admin.css', __FILE__), array(), DS_PBE_VERSION);
}

// Add settings link on plugin page
function ds_pbe_plugin_action_links($links) {
  array_unshift($links, '<a href="admin.php?page=ds-page-builder-everywhere">'.(DS_PBE_has_license_key() ? 'License Key Information' : 'Activate License Key').'</a>'); 
  return $links;
}
function ds_pbe_plugins_php() {
	$plugin = plugin_basename(__FILE__); 
	add_filter('plugin_action_links_'.$plugin, 'ds_pbe_plugin_action_links' );
}
add_action('load-plugins.php', 'ds_pbe_plugins_php');

// No Direct Access
if ( !defined( 'ABSPATH' ) ){
	exit;
}

define( 'PBE_LI_URL', plugin_dir_url( __FILE__ ) );

if( !class_exists( 'PBE_Layout_Inserter' ) ) {
	class PBE_Layout_Inserter {

		private static $instance;

		public static function init() {
			return self::$instance;
		}
		// Constructor Function
		public function __construct() {
			add_action( 'wp_ajax_pbe_li_list', array( $this, 'pbe_li_ajax' ) );
			
			add_action('et_builder_ready', array($this, 'onInit'));
			add_action('admin_init', array($this, 'onAdminInit'));
			
			
			
		}

		// Hooks your functions into the correct filters
		function mce_button() {
			if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
				return;
			}
			// Check if text editor is enabled
			if ( 'true' == get_user_option( 'rich_editing' ) ) {
				add_filter( 'mce_external_plugins', array( $this, 'add_mce_plugin' ) );
				add_filter( 'mce_buttons', array( $this, 'register_mce_button' ) );
			}
		}

		// Add the TinyMCE script
		function add_mce_plugin( $plugin_array ) {
			$plugin_array['pbe_li_mce_btn'] = PBE_LI_URL . 'pbe-li.js';
			return $plugin_array;
		}

		// Register post dropdown in the editor
		function register_mce_button( $buttons ) {
			array_push( $buttons, 'pbe_li_mce_btn' );
			return $buttons;
		}

		//Let's use the layouts Post type
		public static function layouts( $associative=false ) {

			global $wpdb;
			$cpt_post_status = 'publish';
	        $cpt = $wpdb->get_results( $wpdb->prepare(
	            "SELECT ID, post_title
	                FROM $wpdb->posts
	                WHERE $wpdb->posts.post_type = %s
	                AND $wpdb->posts.post_status = %s
	                ORDER BY ID DESC",
	            'et_pb_layout',
	            $cpt_post_status
	        ) );

	        $list = array();

	        foreach ( $cpt as $post ) {
				$selected = '';
				$post_id = $post->ID;
				$post_name = $post->post_title;
				if ($associative) {
					$list[$post_id] = $post_name;
				} else {
					$list[] = array(
						'text' =>	$post_name,
						'value'	=>	$post_id
					);
				}
			}
			
			return $list;
		}

		/**
		 * Function to fetch buttons
		 * @since  1.6
		 * @return string
		 */
		/*public function pbe_li_ajax() {
			// check for nonce
			check_ajax_referer( 'pbe-li-nonce', 'security' );
			$posts = ;
		}*/

		/**
		 * Function to output button list ajax script
		 * @since  1.6
		 * @return string
		 */
		public function pbe_li_list() {
			// create nonce
			global $pagenow;
			//var_dump($pagenow);
			if( $pagenow != 'admin.php' ){
				$nonce = wp_create_nonce( 'pbe-li-nonce' );
				?>
			    <script type="text/javascript">
					var ds_pbe_layouts = <?php echo(json_encode(self::layouts())); ?>;
				</script>
				<?php
			}
		}
		// Add Style

		function onInit() {
			// Following if block copied from AGS Layouts plugin and modified
			//if (function_exists('et_core_is_fb_enabled') && et_core_is_fb_enabled()) {
				add_editor_style( PBE_LI_URL . 'pbe-li.css' );
				add_action( 'wp_footer', array( $this, 'pbe_li_list' ) );
				//add_action( 'wp_head', array( $this, 'mce_button' ) );
				
				add_action('wp_enqueue_scripts', array($this, 'fbScripts'));
			//}
			
			// Layout module
			include_once(__DIR__.'/LayoutModule.php');
			
		}
		function onAdminInit() {
			add_editor_style( PBE_LI_URL . 'pbe-li.css' );
			add_action( 'admin_footer', array( $this, 'pbe_li_list' ) );
			add_action( 'admin_head', array( $this, 'mce_button' ) );
			
			wp_enqueue_style('ds-pbe-admin', PBE_LI_URL.'pbe-li.css');
		}
		
		// Following function from AGS Layouts plugin by Aspen Grove Studios
		public function fbScripts() {
			wp_enqueue_style('ds-pbe-fb', PBE_LI_URL.'pbe-li.css');
			wp_enqueue_script('ds-pbe-fb', PBE_LI_URL.'pbe-li.js', array('jquery','react-tiny-mce'));
			
			wp_localize_script('ds-pbe-fb', 'ds_pbe_fb_config', array(
				'editorCssUrl' => PBE_LI_URL.'pbe-li.css',
				'editLayoutUrl' => admin_url('post.php?action=edit&post='),
				
			));
		}
		
	} // Mce Class
}

$pbe_li_mce = new PBE_Layout_Inserter();
$pbe_li_mce->init();



?>