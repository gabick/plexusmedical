( function( $ ) {
	wp.customize.bind( 'ready', function() {
		wp.customize.panel( 'ghoster_custom_builder', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				if ( isExpanding ) {
					var current_url = wp.customize.previewer.previewUrl();
					var current_url = current_url.includes( agsdg_builder_customizer.page_url );

					if ( ! current_url ) {
						wp.customize.previewer.send( 'agsdg-open-preview', { expanded: isExpanding, pageUrl: agsdg_builder_customizer.page_url } );
					}
				} else {
					wp.customize.previewer.send( 'agsdg-back-to-home', { home_url: wp.customize.settings.url.home } );
				}
			} );
		} );

		wp.customize.section( 'builder_modal', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				handle_modal_section( { expanded: isExpanding, selector: '#agsdg-builder-modal', class: 'et-core-active' } );
			} );
		} );

		wp.customize.section( 'settings_modal', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				handle_modal_section( { expanded: isExpanding, selector: '#agsdg-settings-modal', class: 'et-core-active' } );
			} );
		} );

		wp.customize.section( 'insert_modal', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				handle_modal_section( { expanded: isExpanding, selector: '#agsdg-insert-modal', class: 'et-core-active' } );
			} );
		} );

		wp.customize.section( 'warning_modal', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				handle_modal_section( { expanded: isExpanding, selector: '#agsdg-warning-modal', class: 'et-core-active' } );
			} );
		} );

		var handle_modal_section = function( data ) {
			if ( data.expanded ) {
				wp.customize.previewer.send( 'agsdg-modal-customize-open', data );
			} else {
				wp.customize.previewer.send( 'agsdg-modal-customize-close', data );
			}
		}
	} );
})( jQuery );