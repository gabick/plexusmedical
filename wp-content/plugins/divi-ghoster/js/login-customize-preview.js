( function( $ ) {
	wp.customize.bind( 'preview-ready', function() {
		wp.customize( 'login_area_bg_image', function( value ) {
			value.bind( function( newImage ) {
				if ( '' == newImage ) {
					$( '.login' ).css( "background-image", 'none' );
				} else {
					newImage = 'url(' + newImage + ')';
					$( '.login' ).css( "background-image", newImage );
					$( '.login' ).css( "background-size", 'cover' );
				}
			} );
		});
		wp.customize( 'background_color_tint', function( value ) {
			value.bind( function( newTint ) {
				console.log( newTint );
				$( '.login .fake_login_after' ).css( "background-color", newTint );
			} );
		});
		wp.customize( 'login_area_logo_image', function( value ) {
			value.bind( function( newImage ) {
				if ( '' == newImage ) {
					$( '#login h1 a' ).css( "background-image", '' );
				} else {
					newImage = 'url(' + newImage + ')';
					$( '#login h1 a' ).css( "background-image", newImage );
				}
			} );
		});
		wp.customize( 'login_form_alignment', function( value ) {
			value.bind( function( newAlignment ) {
				$( '.login #login' ).css( "float", newAlignment );
			} );
		});
		wp.customize( 'login_background_color', function( value ) {
			value.bind( function( newColor ) {
				$( '.login' ).css( "background-color", newColor );
			} );
		});
		wp.customize( 'content_link_color', function( value ) {
			value.bind( function( newColor ) {
				$( '.login #backtoblog a, #login form p label, .login #nav a, .login h1 a' ).css( "color", newColor );
			} );
		});
		wp.customize( 'form_background_color', function( value ) {
			value.bind( function( newColor ) {
				$( '#loginform' ).css( "background-color", newColor );
			} );
		});
		wp.customize( 'login_submit_color', function( value ) {
			value.bind( function( newColor ) {
				$( '#login .button-primary' ).css( "background-color", newColor );
			} );
		});

	} );
})( jQuery );
