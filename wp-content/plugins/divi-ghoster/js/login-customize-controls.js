( function( $ ) {
	wp.customize.bind( 'ready', function() {
		wp.customize.section( 'ghoster_custom_login', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				if ( isExpanding ) {
					var current_url = wp.customize.previewer.previewUrl();
					var current_url = current_url.includes( agsdg_login_customizer.page_url );

					if ( ! current_url ) {
						wp.customize.previewer.send( 'agsdg-open-preview', { expanded: isExpanding, pageUrl: agsdg_login_customizer.page_url } );
					}
				} else {
					wp.customize.previewer.send( 'agsdg-back-to-home', { home_url: wp.customize.settings.url.home } );
				}
			} );
		} );
	} );
})( jQuery );
