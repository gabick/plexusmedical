<?php
/**
 * Template Name: Divi Ghoster Login Customizer
 *
 * Template to display the login form for customization purposes.
 * trimmed down version of wp-login.php
 */

if ( ! is_customize_preview() ) {
	if ( is_multisite() ) {
		$url = esc_url( network_home_url( '/' ) );
	} else {
		$url = esc_url( home_url( '/' ) );
	}
	wp_safe_redirect( $url );
}

?>
<!DOCTYPE html>
<html>
	<head>
		<?php
			nocache_headers();
			header( 'Content-Type: ' . get_bloginfo( 'html_type' ) . '; charset=' . get_bloginfo( 'charset' ) );
			do_action( 'login_init' );
			do_action( "login_form_login" );
			$http_post     = ( 'POST' == $_SERVER['REQUEST_METHOD'] );
			$login_link_separator = apply_filters( 'login_link_separator', ' | ' );
			$title = 'Log In';
			$action = 'login';
			$login_title = get_bloginfo( 'name', 'display' );
			/* translators: Login screen title. 1: Login screen name, 2: Network or site name */
			$login_title = sprintf( __( '%1$s &lsaquo; %2$s &#8212; WordPress' ), $title, $login_title );
			$login_title = apply_filters( 'login_title', $login_title, $title );
		?>
		<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
		<title><?php echo $login_title; ?></title>
		<?php
			wp_enqueue_style( 'login' );
			do_action( 'login_enqueue_scripts' );
			do_action( 'login_head' );
		?>
		<meta name='robots' content='noindex,noarchive' />
		<!-- <meta name='referrer' content='strict-origin-when-cross-origin' /> -->
		<meta name="viewport" content="width=device-width" />
		<?php
			$login_header_url   = __( 'https://wordpress.org/' );
			$login_header_title = __( 'Powered by WordPress' );
			$login_header_url = apply_filters( 'login_headerurl', $login_header_url );
			$login_header_title = apply_filters( 'login_headertitle', $login_header_title );
			$login_header_text = $login_header_title;
			$classes = array( 'login-action-' . $action, 'wp-core-ui' );
			if ( is_rtl() ) {
				$classes[] = 'rtl';
			}
			$classes[] = ' locale-' . sanitize_html_class( strtolower( str_replace( '_', '-', get_locale() ) ) );
			$classes = apply_filters( 'login_body_class', $classes, $action );
		?>
	</head>
	<body class="login <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
		<div class="fake_login_after">
			<?php do_action( 'login_header' ); ?>
			<div id="login">
				<h1>
					<a href="<?php echo esc_url( $login_header_url ); ?>" title="<?php echo esc_attr( $login_header_title ); ?>"><?php echo $login_header_text; ?></a>
				</h1>
				<form name="loginform" id="loginform">
					<p>
						<label for="user_login"><?php _e( 'Username or Email Address' ); ?><br />
							<input type="text" name="log" id="user_login" class="input" value="" size="20" autocapitalize="off" /></label>
					</p>
					<p>
						<label for="user_pass"><?php _e( 'Password' ); ?><br />
							<input type="password" name="pwd" id="user_pass" class="input" value="" size="20" /></label>
					</p>
					<?php
					/**
					 * Fires following the 'Password' field in the login form.
					 *
					 */
					do_action( 'login_form' );
					?>
					<p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember Me' ); ?></label></p>
					<p class="submit">
						<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e( 'Log In' ); ?>" />
					</p>
				</form>
				<p id="nav">
					<?php
						if ( get_option( 'users_can_register' ) ) :
							$registration_url = sprintf( '<a id="custom-register-link" href="%s">%s</a>', esc_url( wp_registration_url() ), __( 'Register' ) );
							echo apply_filters( 'register', $registration_url );
							echo esc_html( $login_link_separator );
						endif;
					?>
					<a id="custom-password-reset-link" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?' ); ?></a>
				</p>
			</div>
			<?php do_action( 'login_footer' ); ?>
			<div class="clear"></div>
			<?php wp_footer(); ?>
		</div>
	</body>
</html>
