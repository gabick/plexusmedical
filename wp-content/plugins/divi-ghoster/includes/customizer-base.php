<?php

class DiviGhosterCustomizerBase {

	/**
	 * @var string custom post type slug
	 */
	private static $customizer_template_post_type = 'agsdb_custom_temp';

	/**
	 * @var string custom post type rewrite slug
	 */
	private static $hidden_cpt_rewrite_slug ='agsdg_cpt';

	public static function setup() {
		self::create_template_cpt();
		self::maybe_reset_rewrites();

	}

	/**
	 * make sure the cpt used for customizer templates has rewrite entries
	 * force them to be rewritten if not
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	private static function maybe_reset_rewrites() {
		$rewrites = get_option( 'rewrite_rules' );
        if ( !empty($rewrites) ) {  //prevent array_keys() warning
		  $matches = preg_grep( '/^' . self::$hidden_cpt_rewrite_slug . '/i', array_keys( $rewrites ) );
		  if ( empty( $matches ) ) {
			 delete_option( 'rewrite_rules' );
		  }
        }
	}

	/**
	 * register the custom post type we use for keeping our customizer pages safe
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	private static function create_template_cpt() {
		register_post_type(
			self::$customizer_template_post_type,
			array(
				'label' => 'AGS Customizer Templates',
				'description' => 'a hidden cpt for assigning page templates to special customizer pages',
				'public' => true,
				'exclude_from_search' => true,
				'show_ui' => false,
				'show_in_nav_menus' => false,
				'show_in_rest' => false,
				'can_export' => false,
				'capability_type' => 'page',
				'rewrite' => array(
					'slug' => self::$hidden_cpt_rewrite_slug
				)
			)
		);
	}

	/**
	 * get the page ID that will be used for customizing the builder
	 *
	 * @return int|WP_Error
	 * @since: 3.0.0
	 *
	 */
	public static function get_customizer_template_page_id( $template_page_title, $template_context ) {
		$template_page = get_posts( array(
			'numberposts' => 1,
			'meta_key' => '_wp_page_template',
			'meta_value' => $template_page_title,
			'post_type' => self::$customizer_template_post_type
		) );

		if ( empty( $template_page ) ) {
			return self::create_builder_customizer_post( $template_page_title, $template_context );
		}

		return $template_page[0]->ID;
	}

	/**
	 * create a page that will use our template within the customizer
	 *
	 * @return int|WP_Error
	 * @since: 3.0.0
	 *
	 */
	private static function create_builder_customizer_post( $template_page_title, $template_context ) {
		add_filter( 'pre_wp_unique_post_slug', array( __CLASS__, 'override_slug' ), 10, 5 );
		$builder_template_post = wp_insert_post(
			array(
				'post_author' => 0,
				'post_status' => 'publish',
				'post_type' => self::$customizer_template_post_type,
				'post_title' => "Divi {$template_context} Template",
				'post_content' => 'This is a post used to customize the Divi builder'
			)
		);
		update_post_meta( $builder_template_post, '_wp_page_template', $template_page_title );

		remove_filter( 'pre_wp_unique_post_slug', array( __CLASS__, 'override_slug' ) );
		return $builder_template_post;
	}

	/**
	 * specify our own slug for the new page
	 *
	 * @param $default_slug
	 * @param $slug
	 * @param $post_ID
	 * @param $post_status
	 * @param $post_type
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	public static function override_slug( $default_slug, $slug, $post_ID, $post_status, $post_type ) {
		if ( self::$customizer_template_post_type !== $post_type ) {
			return $default_slug;
		}

		return $slug;
	}
}

add_action( 'after_setup_theme', array( 'DiviGhosterCustomizerBase', 'setup' ) );

