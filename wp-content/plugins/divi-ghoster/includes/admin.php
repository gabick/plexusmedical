<?php
/*
Changes by Aspen Grove Studios:
2019-01-02	Created class, partially using code from other Ghoster files and AGS Layouts class template
2019-01-03	Further refactoring of copied Ghoster code, etc.
2019-01-08	Change recommended branding image size; move license key box
2019-11-13  Styling, section and wording updates for consistency and clarity.
            Use dash instead of underscore in slug example
            Added sections for ease of understanding
2019-11-19  Complete rework of admin page with updated tabs, layout. -Anna
*/

class DiviGhosterAdmin {

    public static $ghosterUrl, $customizer_url;

	public static function setup() {
        self::$ghosterUrl       = admin_url( 'admin.php?page=divi_ghoster' );
        self::$customizer_url   = admin_url( 'customize.php' );

		add_action('admin_init', array('DiviGhosterAdmin', 'onAdminInit'));
		add_action('admin_enqueue_scripts', array('DiviGhosterAdmin', 'adminScripts'));
	}

    public static function onAdminInit() {
        register_setting('agsdg_pluginPage', 'agsdg_settings', array('DiviGhosterAdmin', 'sanitizeSettings'));

    }

	public static function menuPage() {
		(AGS_GHOSTER_has_license_key() ? self::adminPage() : AGS_GHOSTER_activate_page());
	}

	public static function adminScripts($hook) {
		if ($hook == 'toplevel_page_divi_ghoster') {
			wp_enqueue_media();
			wp_enqueue_script('agsdg_admin_page', DiviGhoster::$pluginBaseUrl.'js/admin-page.js', array('jquery'), '1.1', true);
		}
		wp_enqueue_style('dg_admin', DiviGhoster::$pluginBaseUrl.'css/admin.css');
	}




//	public static function ghosterLicense() {
//		AGS_GHOSTER_license_key_box();
//	}

	public static function ghosterLicenseSection() {
		echo '<span class="agsdg_settings_section_heading">'.__('Ghoster License', 'ags-ghoster').'</span>';
	}

	public static function sanitizeSettings($settings) {
		include_once(__DIR__.'/ultimate.php');

		// Make sure Branding Name is set
		if (empty($settings['branding_name'])) {
			add_settings_error('branding_name', 'branding_name_empty', __('The Branding Name field may not be empty.', 'ags-ghoster'));
			$settings['branding_name'] = (empty(DiviGhoster::$settings['branding_name']) ? DiviGhoster::$targetTheme : DiviGhoster::$settings['branding_name']);
		}

		// Make sure Theme Slug is set, is not the theme default, and does not contain invalid characters
		if (empty($settings['theme_slug'])) {
			add_settings_error('theme_slug', 'theme_slug_empty', __('The Theme Slug field may not be empty.', 'ags-ghoster'));
			$settings['theme_slug'] = (empty(DiviGhoster::$settings['theme_slug']) ? 'ghost_'.DiviGhoster::$targetThemeSlug : DiviGhoster::$settings['theme_slug']);
		} else if (strcasecmp($settings['theme_slug'], DiviGhoster::$targetThemeSlug) == 0) {
			add_settings_error('theme_slug', 'theme_slug_empty', __('The Theme Slug must be something other than the default', 'ags-ghoster').' &quot;'.DiviGhoster::$targetThemeSlug.'&quot;.');
			$settings['theme_slug'] = (empty(DiviGhoster::$settings['theme_slug']) ? 'ghost_'.DiviGhoster::$targetThemeSlug : DiviGhoster::$settings['theme_slug']);
		} else {
			//..$newSlug = preg_replace('/[^A-Za-z0-9_\-]+/', '', $settings['theme_slug']);
			$newSlug = preg_replace('/[^A-Za-z0-9_]+/', '', $settings['theme_slug']);
			if ($newSlug != $settings['theme_slug']) {
				add_settings_error('theme_slug', 'theme_slug_invalid_chars', __('The theme slug may only contain letters, numbers and underscores.', 'ags-ghoster'));
				$settings['theme_slug'] = $newSlug;
			}
		}

		// Handle Ultimate Ghoster setting
		if (!empty($settings['ultimate_ghoster'])) {
			$settings['ultimate_ghoster'] = 'no';

			if (get_option('permalink_structure', '') == '') {
				add_settings_error('ultimate_ghoster', 'ultimate_ghoster_plain_permalinks', __('Ultimate Ghoster cannot be enabled while your permalink structure is set to Plain. Please change your permalink structure in Settings &gt; Permalinks.', 'ags-ghoster'));
			} else if (($settings['theme_slug'] != DiviGhoster::$settings['theme_slug'] || DiviGhoster::$settings['ultimate_ghoster'] != 'yes')) {
				try {
					DiviGhosterUltimate::disable(DiviGhoster::$settings['theme_slug']);
				} catch (Exception $ex) {}
				try {
					DiviGhosterUltimate::enable($settings['theme_slug'], $settings['branding_name']);
					$settings['ultimate_ghoster'] = 'yes';
					update_option('adsdg_ultimate_theme', DiviGhoster::$targetTheme);
					add_settings_error('ultimate_ghoster', 'ultimate_ghoster_enabled', __('Settings saved; Ultimate Ghoster is enabled.', 'ags-ghoster'), 'updated');
				} catch (Exception $ex) {
					add_settings_error('ultimate_ghoster', 'ultimate_ghoster_enable_error', __('An error occurred while enabling Ultimate Ghoster; please try again. If the problem persists, you may need to re-install your theme.', 'ags-ghoster'));
				}
			}
		}

		if (empty($settings['ultimate_ghoster']) || $settings['ultimate_ghoster'] == 'no') {
			$settings['ultimate_ghoster'] = 'no';
			delete_option('adsdg_ultimate_theme');

			if (DiviGhoster::$settings['ultimate_ghoster'] == 'yes') {
				try {
					DiviGhosterUltimate::disable(DiviGhoster::$settings['theme_slug']);
					add_settings_error('ultimate_ghoster', 'ultimate_ghoster_disabled', __('Ultimate Ghoster has been disabled.', 'ags-ghoster'), 'updated');
				} catch (Exception $ex) {
					add_settings_error('ultimate_ghoster', 'ultimate_ghoster_disable_error', __('An error occurred while disabling Ultimate Ghoster. Please try enabling and disabling it again.', 'ags-ghoster'));
				}
			}
		}

		return $settings;
	}

    public static function adminPage() {
        ?>


        <!-- START OUTPUT -->
        <div id="ds-ghoster-settings-container">
            <?php settings_errors(); ?>
            <div id="ds-ghoster-settings">

                    <div id="ds-ghoster-settings-header">
                        <div class="ds-ghoster-settings-logo">
                            <img src="<?php echo(esc_url(DiviGhoster::$pluginBaseUrl.'assets/images/logo.png')); ?>">
                            <h1>Divi Ghoster</h1>
                        </div>
                        <div id="ds-ghoster-settings-header-links">
                            <!--                <a id="ds-ghoster-settings-header-link-settings" href="">Settings</a>-->
                            <a id="ds-ghoster-settings-header-link-support" href="https://support.aspengrovestudios.com/article/20-divi-ghoster-plugin" target="_blank">Support</a>
                        </div>
                    </div>

                    <ul id="ds-ghoster-settings-tabs">
                        <li class="ds-ghoster-settings-active"><a href="#branding">Branding</a></li>
                        <li><a href="#ultimate-ghoster">Ultimate Ghoster</a></li>
                        <li><a href="#additional-customization">Additional Customization</a></li>
                        <li><a href="#license">License</a></li>
                    </ul>

                    <div id="ds-ghoster-settings-tabs-content">
                        <div id="ds-ghoster-settings-branding" class="ds-ghoster-settings-active">
                            <form action='options.php' method='post' id="ds-ghoster-settings-form-save"> <?php settings_fields('agsdg_pluginPage'); do_settings_sections('agsdg_pluginPage'); ?>

                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span><h3>Branding Name</h3> (example: "Acme Web Design") </span>
                                    <input type="text" name="agsdg_settings[branding_name]"  value="<?php
                                    echo htmlspecialchars(DiviGhoster::$settings['branding_name']);
                                    ?>" class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext"><span>Help</span> (Example: "Acme Web Design"): This will replace all instances in the dashboard, page builder, etc. where the name ‘Divi’ appears. Hit ‘Save Changes’</div>
                                </div>
                            </div>
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span><h3>Branding Image</h3> (minimum 50px by 50px) </span>
                                    <input id="image-url" type="text" name="agsdg_settings[branding_image]" size="45" value="<?php
                                    echo htmlspecialchars(DiviGhoster::$settings['branding_image']);
                                    ?>" class="agsdg_settings_field" />
                                    <strong style='font-weight:600;'>&nbsp;Or&nbsp;</strong>
                                    <input id="upload-button" type="button" class="button dg-button" value="Upload Image" />
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext"><span>Help</span> Upload your desired Branding Image (36px x 36px): This will replace all instances where the Divi icon appears in the dashboard, page builder, etc. If you upload a larger image of the same scale, it will reduce, but we encourage not using large file sizes.</div>
                                </div>
                            </div>

                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span><h3>Theme URL Slug</h3> (example: acme) </span>
                                    <input type="text" size="45" name="agsdg_settings[theme_slug]" value="<?php
                                    echo htmlspecialchars(DiviGhoster::$settings['theme_slug']);
                                    ?>" class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext"><span>Help</span> Enter your Slug Text (example: acme_web_designs): This will change the default URL slug that appears in Theme Options. Example: http://yourwebsiteurl.com/wp-admin/admin.php?page=et_acme-web-designs</div>
                                </div>
                            </div>

                        </div>


                        <!-- ULTIMATE GHOSTER TAB -->
                        <div id="ds-ghoster-settings-ultimate-ghoster">
                            <div class="ds-ghoster-settings-box" style="margin-top:20px;">
                                <label>
                                    <span><h3>Enable Ultimate Divi Ghoster:</h3></span>
                                    <input name="agsdg_settings[ultimate_ghoster]" type="checkbox"<?php echo(DiviGhoster::$settings['ultimate_ghoster'] == 'yes' ? ' checked="checked"' : ''); ?>>
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext"><span>Help</span>Once “Ultimate Ghoster” is enabled and saved, the following functions will take effect:
                                        <ul> <li>The ‘Divi Ghoster’ link in the dashboard will disappear</li>
                                            <li>Divi Ghoster and other popular Divi-related plugins will not be visible in the plugins dashboard</li>
                                            <li>The Divi theme will be hidden from the source code and theme detector websites and replaced with your selected branding name</li>
                                            <li>The theme name under Appearance > Themes will be branded with your name.</li>
                                            <li>The “Role Editor” will be “Ghosted”</li></ul></div>
                                </div>
                                <hr>
                            </div>
                             <div class="ds-ghoster-settings-box">
                                <ul>
                                    <li>1. If you are using a caching plugin, <strong>be sure to clear its cache</strong> after enabling or disabling Ultimate Ghoster!</li>
                                    <li>2. Ultimate Ghoster will not work if the permalink structure is set to Plain in Settings &gt; Permalinks.</li>
                                    <li>3. Enabling Ultimate Ghoster will hide the <?php echo(DiviGhoster::$targetTheme); ?> Ghoster plugin. Please copy this URL and save it to disable this feature later: <a href="<?php
                                            echo(self::$ghosterUrl);
                                            ?> "><?php
                                                echo(self::$ghosterUrl);
                                                ?></a></li>
                                    <li style="padding-bottom:20px" >4. Enabling Ultimate Ghoster will hide &quot;Divi Switch&quot;, &quot;Divi Booster&quot;, and &quot;Aspen Footer Editor&quot; from the Divi menu and the Plugins list.</li>
                                        <li><p style="font-size:18px; font-weight:600;">If installed, they can be accessed directly at any time by visiting:</p>
                                            <ul style="padding-inline-start: 20px;">
                                                <li>For <strong>Divi Switch</strong>: <a href="<?php echo admin_url('admin.php?page=divi-switch-settings'); ?> "><?php echo admin_url('admin.php?page=divi-switch-settings'); ?></a></li>
                                                <li>For <strong>Divi Booster</strong>: <a href="<?php echo admin_url('admin.php?page=wtfdivi_settings'); ?> "><?php echo admin_url('admin.php?page=wtfdivi_settings'); ?></a></li>
                                                <li>For <strong>Aspen Footer Editor</strong>: <a href="<?php echo admin_url('admin.php?page=aspen-footer-editor'); ?> "><?php echo admin_url('admin.php?page=aspen-footer-editor'); ?></a></li>
                                            </ul>
                                        </li>
                                </ul>
                            </div>
                            </form>
                        </div>

                        <!-- ADDITIONAL CUSTOMIZATIONS TAB -->
                        <div id="ds-ghoster-settings-additional-customization">

                            <div class="ds-ghoster-settings-box">
                                                                    <label>
                                                                        <span><h3>Access front-end builder customization options:</h3>  </span>
                                                                        <?php
                                                                        $builder_customizer_url = add_query_arg(array(
                                                                            'return' => self::$ghosterUrl,
                                                                            'autofocus[panel]' => 'ghoster_custom_builder'
                                                                        ), self::$customizer_url);
                                                                        ?><a class="button button-primary" href="<?php
                                                                        echo $builder_customizer_url;
                                                                        ?>">Builder Customizer</a>
                                                                    </label>
                                <p>Customize the Divi Builder with your unique color scheme to help white label the interface even further. Note: To change the colors, Ultimate Ghoster needs to be disabled. </p>
                            </div>

                                                            <div class="ds-ghoster-settings-box">
                                                                <label>
                                                                    <span><h3>Access login page customization options:</h3>  </span>

                                                                    <?php
                                                                    $login_customizer_url = add_query_arg(array(
                                                                        'return' => self::$ghosterUrl,
                                                                        'autofocus[section]' => 'ghoster_custom_login'
                                                                    ), self::$customizer_url);
                                                                    ?><a class="button button-primary" href="<?php
                                                                    echo $login_customizer_url;
                                                                    ?>">Login Customizer</a>
                                                                </label>
                                                                <p> Customize the standard WordPress login page with your own logo, background, and colors with an easy to use interface. </p>
                                                            </div>

                        </div>

                        <!-- LICENSE TAB -->
                        <div id="ds-ghoster-settings-license">

                            <?php AGS_GHOSTER_license_key_box(); ?>
                        </div>

                    </div> <!-- close ds-ghoster-settings-tabs-content -->
                <button id='epanel-save-top' class='save-button button dg-button' name="btnSubmit" form="ds-ghoster-settings-form-save">Save Changes</button>

                <!-- settings tab -->
                <script>
                        var ds_ghoster_tabs_navigate = function() {
                        jQuery('#ds-ghoster-settings-tabs-content > div, #ds-ghoster-settings-tabs > li').removeClass('ds-ghoster-settings-active');
                        jQuery('#ds-ghoster-settings-'+location.hash.substr(1)).addClass('ds-ghoster-settings-active');
                        jQuery('#ds-ghoster-settings-tabs > li:has(a[href="'+location.hash+'"])').addClass('ds-ghoster-settings-active');
                    };

                    if (location.hash) {
                        ds_ghoster_tabs_navigate();
                    }

                    jQuery(window).on('hashchange', ds_ghoster_tabs_navigate);

                </script>


            </div> <!-- close ds-ghoster-settings -->


        </div> <!-- close ds-ghoster-settings-container -->







        <?php
    }
}

DiviGhosterAdmin::setup();