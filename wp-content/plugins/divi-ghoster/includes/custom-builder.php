<?php
/*
 * Changes by Aspen Grove Studios:
 * 2019-03-05	Created class
 * 2019-08-20 Coulter Peterson
 */

/**
 * Class DiviGhosterCustomBuilder
 * Manages the Divi Builder customization
 *
 * @since 3.0.0
 *
 * 2019-11-12   Modified $wp_customizer->add_setting(). (Commented out sanitizer filter)
 *              Divi does not sanitize colors, since they have an rgba picker.
 *              The sanitize_hex_color WP filter fails with "Invalid value." if an rgba value is passed.
 */
class DiviGhosterCustomBuilder {

	/**
	 * @var string option name where customizations are stored
	 */
	private static $builder_custom_opt_name = 'agsdg_builder_options';

	/**
	 * @var string internationalization slug
	 */
	private static $i18l_namespace = 'ags-ghoster';

	/**
	 * @var string path to builder template file
	 */
	private static $builder_template_path;

	/**
	 * @var int page ID for the builder customizer page
	 */
	private static $builder_template_page_id;

	/**
	 * @var string base url for the script directory
	 */
	private static $builder_script_base_url;

	/**
	 * @var string base url for the style directory
	 */
	private static $builder_style_base_url;

	/**
	 * @var string used as the page template instead of assigning the template path
	 */
	private static $builder_customizer_page_template_name = '__agsdg_builder__';


	/**
	 * kick things off.
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function setup() {
		if ( DiviGhoster::$settings['ultimate_ghoster'] != 'yes' ) {

			// enqueue scripts for the customizer
			add_action( 'customize_register', array( __CLASS__, 'setup_builder_customizer' ) );

			// set up the builder customization
			add_action( 'customize_preview_init', array( __CLASS__, 'customizer_preview_init' ) );
			add_action( 'customize_controls_enqueue_scripts', array( __CLASS__, 'customizer_controls_init' ) );

			// get template cpt (ensure it exists)
		}
		self::$builder_template_page_id = self::get_builder_template_page_id();
		self::$builder_template_path = DiviGhoster::$pluginDirectory . 'includes/templates/divi-builder-customizer.php';
		self::$builder_script_base_url = DiviGhoster::$pluginBaseUrl . 'js/';
		self::$builder_style_base_url = DiviGhoster::$pluginBaseUrl . 'css/';

		// add inline builder custom styles
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'create_builder_custom_inline_styles' ), 99 );
		/* Add them to the backend builder in the admin dashboard */
		add_action('admin_head', array( __CLASS__, 'return_inline_styles' ));

	}

	/**
	 * create customizer panel and sections
	 *
	 * @param $wp_customizer
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function setup_builder_customizer( $wp_customizer ) {
		$color_picker = self::get_color_picker_class();

		$builder_colors = wp_parse_args( get_option( self::$builder_custom_opt_name, array() ), self::get_default_builder_colors() );

		$wp_customizer->add_panel( 'ghoster_custom_builder', array(
			'title' => __( 'Divi Builder', self::$i18l_namespace ),
			'description' => __( 'Customize the Divi Builder', self::$i18l_namespace ),
			'priority' => 125,
			'capability' => 'edit_theme_options'
		) );

		foreach ( self::get_divi_builder_customizer_sections( $builder_colors ) AS $section_detail ) {

			$wp_customizer->add_section( $section_detail['slug'], array(
				'title' => $section_detail['title'],
				'panel' => 'ghoster_custom_builder',
				'priority' => $section_detail['priority']
			) );

			foreach ( $section_detail['settings'] AS $setting_detail ) {

				$wp_customizer->add_setting( $setting_detail['slug'], array(
					'default' => $setting_detail['default'],
					'transport' => 'postMessage',
					// 2019-11-12  Divi does not sanitize colors, since they have an rgba picker.
                    //             The sanitize_hex_color WP filter fails with "Invalid value." if an rgba value is passed.
                    //'sanitize_callback' => 'sanitize_hex_color',
					'type' => 'option'
				) );

				$wp_customizer->add_control( new $color_picker( $wp_customizer, $setting_detail['slug'], array(
					'label' => $setting_detail['label'],
					'section' => $section_detail['slug'],
				) ) );

			}

		}

	}

	/**
	 * get things in place for the preview side of the customizer
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function customizer_preview_init() {
		wp_enqueue_script( 'ags-ghost-builder-customize-preview', self::$builder_script_base_url . 'customize-preview.js', array( 'customize-preview' ), '', true );
		add_filter( 'template_include', array( __CLASS__, 'maybe_use_builder_template' ) );


		/* Add the builder-focused preview side script */
		wp_enqueue_script( 'ags-builder-customize-live-preview', self::$builder_script_base_url . 'builder-customize-preview.js', array( 'ags-ghost-builder-customize-preview' ), '', true );

		$page = get_permalink( self::$builder_template_page_id );

		$localize = array(
			'page_url' => $page
		);
		wp_localize_script( 'ags-builder-customize-live-preview', 'agsdg_builder_customizer', $localize );
	}

	/**
	 * get things in place for the control side of the customizer
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function customizer_controls_init() {
		wp_enqueue_script( 'ags-builder-customize-controls', self::$builder_script_base_url . 'builder-customize-controls.js', array( 'customize-controls' ), '', true );

		$page = get_permalink( self::$builder_template_page_id );

		$localize = array(
			'page_url' => $page
		);
		wp_localize_script( 'ags-builder-customize-controls', 'agsdg_builder_customizer', $localize );
	}

	/**
	 * if customizer preview is loading our builder customizer page, load
	 * the necessary css and return the path to our template
	 *
	 * @param $template
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	public static function maybe_use_builder_template( $template ) {
		global $post;
		if ( ! $post ) {
			return $template;
		}

		$post_template = get_post_meta( $post->ID, '_wp_page_template', true );

		if ( empty( $post_template ) ) {
			return $template;
		}

		if ( self::$builder_customizer_page_template_name !== $post_template ) {
			return $template;
		}

		if ( ! file_exists( self::$builder_template_path ) ) {
			return $template;
		}

		wp_enqueue_style(
			'agsdg-customize-builder-styles',
			self::$builder_style_base_url . 'custom-builder.css',
			array(),
			DiviGhoster::PLUGIN_VERSION
		);


		return self::$builder_template_path;

	}

	/**
	 * return the ET color picker that is available to be used in the customizer
	 * use the WP default as a fall-back
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	private static function get_color_picker_class () {

		if ( class_exists( 'ET_Color_Alpha_Control' ) ) {
			return 'ET_Color_Alpha_Control';
		} else if ( class_exists( 'ET_Divi_Customize_Color_Alpha_Control' ) ) {
			return 'ET_Divi_Customize_Color_Alpha_Control';
		}

		return 'WP_Customize_Color_Control';

	}

	/**
	 * return the default builder colors
	 *
	 * @return array
	 * @since: 3.0.0
	 *
	 */
	private static function get_default_builder_colors() {
		return array(
			'layout_bg' => '#6c2eb9',
			'layout_font' => '#ffffff',
			'section_bg' => '#2b87da',
			'section_font' => '#ffffff',
			'row_bg' => '#29c4a9',
			'row_font' => '#ffffff',
			'module_bg' => '#4c5866',
			'module_font' => '#ffffff',
			'builder_settings_bg' => '#4c5866',
			'builder_settings_font' => '#ffffff',
			'builder_settings_active_icon' => '#70c3a9',
			'settings_header_bg' => '#6c2eb9',
			'settings_header_font' => '#ffffff',
			'settings_tab_bg' => '#7e3bd0',
			'settings_tab_font' => '#ffffff',
			'settings_active_tab_bg' => '#8e42eb',
			'settings_active_tab_font' => '#ffffff',
			'settings_discard_button_bg' => '#ef5555',
			'settings_undo_button_bg' => '#7d3bcf',
			'settings_redo_button_bg' => '#2b87da',
			'settings_save_button_bg' => '#29c4a9',
			'settings_bottom_buttons_font' => '#ffffff',
			'insert_header_bg' => '#6c2eb9',
			'insert_header_font' => '#ffffff',
			'insert_tab_bg' => '#7e3bd0',
			'insert_tab_font' => '#ffffff',
			'insert_active_tab_bg' => '#8e42eb',
			'insert_active_tab_font' => '#ffffff',
			'warning_header_bg' => '#6c2eb9',
			'warning_header_font' => '#ffffff',
			'warning_discard_button_bg' => '#00c3aa',
			'warning_save_button_bg' => '#008BDA',
			'warning_bottom_buttons_font' => '#ffffff',
			//'search_help_icon_bg' => '#4c5866',
			//'search_help_icon_font' => '#ffffff',
			'save_draft_button_bg' => '#2b87da',
			'save_draft_button_font' => '#ffffff',
			'publish_button_bg' => '#29c4a9',
			'publish_button_font' => '#ffffff',
		);
	}

	/**
	 * return the individual customizer controls with their current/default settings
	 *
	 * @param $color_defaults
	 *
	 * @return array
	 * @since: 3.0.0
	 *
	 */
	private static function get_divi_builder_customizer_sections( $color_defaults ) {
		return array(
			array(
				'slug' => 'layout_controls',
				'title' => __( 'Layout Controls', self::$i18l_namespace ),
				'priority' => 5,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[layout_bg]',
						'label' => __( 'Border and Toolbar Background', self::$i18l_namespace ),
						'default' => $color_defaults['layout_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[layout_font]',
						'label' => __( 'Toolbar Text', self::$i18l_namespace ),
						'default' => $color_defaults['layout_font'],
					)
				)
			),
			array(
				'slug' => 'section_controls',
				'title' => __( 'Section Controls', self::$i18l_namespace ),
				'priority' => 10,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[section_bg]',
						'label' => __( 'Border and Toolbar Background', self::$i18l_namespace ),
						'default' => $color_defaults['section_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[section_font]',
						'label' => __( 'Toolbar Text', self::$i18l_namespace ),
						'default' => $color_defaults['section_font'],
					)
				)
			),
			array(
				'slug' => 'row_controls',
				'title' => __( 'Row Controls', self::$i18l_namespace ),
				'priority' => 15,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[row_bg]',
						'label' => __( 'Border and Toolbar Background', self::$i18l_namespace ),
						'default' => $color_defaults['row_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[row_font]',
						'label' => __( 'Toolbar Text', self::$i18l_namespace ),
						'default' => $color_defaults['row_font'],
					)
				)
			),
			array(
				'slug' => 'module_controls',
				'title' => __( 'Module Controls', self::$i18l_namespace ),
				'priority' => 20,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[module_bg]',
						'label' => __( 'Border and Toolbar Background', self::$i18l_namespace ),
						'default' => $color_defaults['module_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[module_font]',
						'label' => __( 'Toolbar Text', self::$i18l_namespace ),
						'default' => $color_defaults['module_font'],
					)
				)
			),
			array(
				'slug' => 'builder_modal',
				'title' => __( 'Builder Settings Modal', self::$i18l_namespace ),
				'priority' => 25,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[builder_settings_bg]',
						'label' => __( 'Settings Header Background', self::$i18l_namespace ),
						'default' => $color_defaults['builder_settings_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[builder_settings_font]',
						'label' => __( 'Settings Header Text', self::$i18l_namespace ),
						'default' => $color_defaults['builder_settings_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[builder_settings_active_icon]',
						'label' => __( 'Toolbar Active Icon', self::$i18l_namespace ),
						'default' => $color_defaults['builder_settings_active_icon'],
					),
				)
			),
			array(
				'slug' => 'settings_modal',
				'title' => __( 'Settings Modal', self::$i18l_namespace ),
				'priority' => 30,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_header_bg]',
						'label' => __( 'Header Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_header_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_header_font]',
						'label' => __( 'Header Text', self::$i18l_namespace ),
						'default' => $color_defaults['settings_header_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_tab_bg]',
						'label' => __( 'Tabs Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_tab_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_tab_font]',
						'label' => __( 'Tabs Font', self::$i18l_namespace ),
						'default' => $color_defaults['settings_tab_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_active_tab_bg]',
						'label' => __( 'Active Tab Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_active_tab_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_active_tab_font]',
						'label' => __( 'Active Tab Text', self::$i18l_namespace ),
						'default' => $color_defaults['settings_active_tab_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_discard_button_bg]',
						'label' => __( 'Discard Changes Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_discard_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_undo_button_bg]',
						'label' => __( 'Undo Changes Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_undo_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_redo_button_bg]',
						'label' => __( 'Redo Changes Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_redo_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_save_button_bg]',
						'label' => __( 'Save Changes Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['settings_save_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[settings_bottom_buttons_font]',
						'label' => __( 'Settings Bottom Button Icons', self::$i18l_namespace ),
						'default' => $color_defaults['settings_bottom_buttons_font'],
					),
				)
			),
			array(
				'slug' => 'insert_modal',
				'title' => __( 'Insert Modal', self::$i18l_namespace ),
				'priority' => 35,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[insert_header_bg]',
						'label' => __( 'Header Background', self::$i18l_namespace ),
						'default' => $color_defaults['insert_header_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[insert_header_font]',
						'label' => __( 'Header Text', self::$i18l_namespace ),
						'default' => $color_defaults['insert_header_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[insert_tab_bg]',
						'label' => __( 'Tabs Background', self::$i18l_namespace ),
						'default' => $color_defaults['insert_active_tab_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[insert_tab_font]',
						'label' => __( 'Tabs Text', self::$i18l_namespace ),
						'default' => $color_defaults['insert_active_tab_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[insert_active_tab_bg]',
						'label' => __( 'Active Tab Background', self::$i18l_namespace ),
						'default' => $color_defaults['insert_active_tab_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[insert_active_tab_font]',
						'label' => __( 'Active Tab Text', self::$i18l_namespace ),
						'default' => $color_defaults['insert_active_tab_font'],
					),
				)
			),
			array(
				'slug' => 'warning_modal',
				'title' => __( 'Warning Modal', self::$i18l_namespace ),
				'priority' => 40,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[warning_header_bg]',
						'label' => __( 'Header Background', self::$i18l_namespace ),
						'default' => $color_defaults['warning_header_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[warning_header_font]',
						'label' => __( 'Header Text', self::$i18l_namespace ),
						'default' => $color_defaults['warning_header_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[warning_discard_button_bg]',
						'label' => __( 'Discard Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['warning_discard_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[warning_save_button_bg]',
						'label' => __( 'Save Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['warning_save_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[warning_bottom_buttons_font]',
						'label' => __( 'Bottom Button Text', self::$i18l_namespace ),
						'default' => $color_defaults['warning_bottom_buttons_font'],
					),
				)
			),
			array(
				'slug' => 'action_buttons',
				'title' => __( 'Action Buttons', self::$i18l_namespace ),
				'priority' => 45,
				'settings' => array(
					array(
						'slug' => self::$builder_custom_opt_name . '[save_draft_button_bg]',
						'label' => __( 'Save Draft Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['save_draft_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[save_draft_button_font]',
						'label' => __( 'Save Draft Button Text', self::$i18l_namespace ),
						'default' => $color_defaults['save_draft_button_font'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[publish_button_bg]',
						'label' => __( 'Publish Button Background', self::$i18l_namespace ),
						'default' => $color_defaults['publish_button_bg'],
					),
					array(
						'slug' => self::$builder_custom_opt_name . '[publish_button_font]',
						'label' => __( 'Publish Button Text', self::$i18l_namespace ),
						'default' => $color_defaults['publish_button_font'],
					),
				)
			)
		);
	}

	/**
	 * make the customized builder styles available to the page
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function create_builder_custom_inline_styles() {
		$custom_builder_options = get_option( self::$builder_custom_opt_name, array() );

		if ( empty( $custom_builder_options ) ) {
			return;
		}

		$custom_builder_options = wp_parse_args( $custom_builder_options, self::get_default_builder_colors() );

		$custom_css = self::build_custom_css_output( $custom_builder_options );

		wp_add_inline_style( 'agsdg-customize-builder-styles', $custom_css );
		wp_add_inline_style( 'et-frontend-builder', $custom_css );  
	}

	public static function return_inline_styles() {
		$custom_builder_options = get_option( self::$builder_custom_opt_name, array() );

		if ( empty( $custom_builder_options ) ) {
			return;
		}

		$custom_builder_options = wp_parse_args( $custom_builder_options, self::get_default_builder_colors() );

		$custom_css = self::build_custom_css_output( $custom_builder_options );

		echo '<style>' .
				$custom_css .
			'</style>';
	}

	/**
	 * get the page ID that will be used for customizing the builder
	 *
	 * @return int|WP_Error
	 * @since: 3.0.0
	 *
	 */
	private static function get_builder_template_page_id() {
		return DiviGhosterCustomizerBase::get_customizer_template_page_id( self::$builder_customizer_page_template_name, 'Builder' );
	}

	/**
	 * build the css output for the builder customizations
	 *
	 * @param $custom_colors
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	private static function build_custom_css_output( $custom_colors ) {
		$css_output = array();
		foreach ( $custom_colors AS $option_name => $color ) {
			$selectors = self::get_css_selectors( $option_name );

			if ( empty( $selectors ) || empty( $selectors[0]['selector'] ) ) {
				continue;
			}

			foreach ( $selectors AS $selector_props ) {
				if  ( isset( $selector_props['important'] ) && $selector_props['important'] ) {
					$color .= ' !important';
				}
				$css_output[] = sprintf( '%s { %s: %s; }', $selector_props['selector'], $selector_props['property'], $color );
			}
		}

		return implode( "\n", $css_output );
	}

	/**
	 * return the selectors used to customize the builder style
	 *
	 * @param $option
	 *
	 * @return array
	 * @since: 3.0.0
	 *
	 */
	private static function get_css_selectors( $option ) {
		switch ( $option ) {
			case "layout_bg":
				return array(
					array(
						'selector'  => '#et_pb_layout .hndle, .et-db #et-boc #et_pb_layout .hndle .et-fb-button, .et-db #et-boc #et_pb_layout .hndle .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar > .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar > .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar .et-fb-page-settings-bar__column--main .et-fb-button, #et_pb_layout_controls, #et_pb_layout_controls .et-pb-layout-buttons, #et_pb_layout_controls .et-pb-layout-buttons:hover, .et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li.et_fb_fullwidth',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "layout_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon svg, .et-db #et-boc .et-fb-page-settings-bar__toggle-button ~ .et-fb-button-group .et-fb-icon svg, .et-db #et-boc #et_pb_layout .et-fb-page-settings-bar .et-fb-button .et-fb-icon svg',
						'property' => 'fill'
					),
					array(
						'selector' => '#et_pb_layout .hndle',
						'property' => 'color'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child:before, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child:after',
						'property' => 'background'
					)
				);
				break;
			case "section_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header, #et-boc #et-fb-app .et-fb-skeleton--section > .et-fb-skeleton__header .et-fb-button, #et-boc #et-fb-app .et_pb_section .et-fb-section-button-wrap--add .et-fb-button, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button:hover, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button-group, .et-db #et-boc .et-fb-outline--section, .et-pb-controls, .et-db #et-boc .et_pb_section>.et-pb-draggable-spacing__sizing, .et-db #et-boc .et_pb_section>.et-pb-draggable-spacing__spacing, .et-db #et-boc .et_pb_section>.et-pb-draggable-spacing__spacing .et-pb-draggable-spacing__hint, .et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li, .et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li:after, .et-db #et-boc .et-core-control-toggle--on, .et-db #et-boc .et-fb-settings-custom-select-wrapper.et-fb-settings-option-select-active .select-option-item-hovered, .et_pb_yes_no_button.et_pb_on_state',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "section_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-section-button-wrap--add .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et-fb-component-settings--section .et-fb-button-group .et-fb-icon svg',
						'property' => 'fill'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header',
						'property' => 'color'
					)
				);
				break;
			case "row_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header, .et-db #et-boc .et-fb-outline--row, .et-db #et-boc #et-fb-app .et-fb-row-button-wrap--add .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--row .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--row .et-fb-button-group, .et_pb_row .et-pb-controls, .et-db #et-boc .et_pb_row>.et-pb-draggable-spacing__sizing, .et-db #et-boc .et_pb_row>.et-pb-draggable-spacing__spacing, .et-db #et-boc .et_pb_row>.et-pb-draggable-spacing__spacing .et-pb-draggable-spacing__hint, .et-db #et-boc .et-fb-columns-layout li .column-block-wrap .column-block, .et_pb_layout_column, .et-pb-column-layouts li:hover .et_pb_layout_column',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "row_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et-fb-row-button-wrap--add .et-fb-icon svg, .et-db #et-boc #et-fb-app .et-fb-component-settings--row > .et-fb-button-group .et-fb-icon svg',
						'property' => 'fill'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header',
						'property' => 'color'
					)
				);
				break;
			case "module_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header, .et-db #et-boc #et-fb-app .et-fb-component-settings--module .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--module .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-module-button-wrap--add .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-skeleton--row .et_pb_column_empty .et-fb-button, .et-db #et-boc #et-fb-app .et_pb_row > .et_pb_column_empty .et-fb-button, .et_pb_module_block, .et-db #et-boc .et-pb-draggable-spacing__sizing',
						'property' => 'background'
					)
				);
				break;
			case "module_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et_pb_module .et-fb-icon svg, .et-db #et-boc .et_pb_row > .et_pb_column_empty .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--row .et_pb_column_empty .et-fb-icon svg',
						'property' => 'fill'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header',
						'property' => 'color'
					)
				);
				break;
			case "builder_settings_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header .et-fb-button, .et-db #et-boc .et-fb-button-group--responsive-mode, .et-db #et-boc .et-fb-button-group--builder-mode, .et-db #et-boc #et-fb-app .et-fb-button-group--responsive-mode .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-button-group--builder-mode .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-button-group--responsive-mode .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-button-group--builder-mode .et-fb-button:hover',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "builder_settings_font":
				return array(
					array(
						'selector' => '.et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header .et-fb-button svg, .et-db #et-boc .et-fb-button-group--responsive-mode .et-fb-icon svg, .et-db #et-boc .et-fb-button-group--builder-mode .et-fb-icon svg, .et-db #et-boc .et-fb-button-group--responsive-mode .et-fb-button--app-modal path',
						'property' => 'fill'
					),
					array(
						'selector' => '.et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header',
						'property' => 'color'
					)
				);
				break;
			case "builder_settings_active_icon":
				return array(
					array(
						'selector' => '.et-fb-preview--wireframe #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--wireframe svg, .et-fb-preview--zoom #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--zoom-out svg, .et-fb-preview--desktop #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--desktop svg, .et-fb-preview--tablet #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--tablet svg, .et-fb-preview--phone #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--phone svg',
						/* may re-add: .agsdb_custom_temp-template #et-boc #et-fb-app .et-fb-page-settings-bar__column--left .et-fb-icon--hover svg */
						'property' => 'fill'
					)
				);
				break;
			case "settings_header_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__header .et-fb-button, .et-core-modal-header, .et-pb-settings-heading, .et_pb_prompt_modal h3',
						'property' => 'background'
					)
				);
				break;
			case "settings_header_font":
				return array(
					array(
						'selector' => '.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__header .et-fb-button svg',
						'property' => 'fill'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-modal__header',
						'property' => 'color'
					)
				);
				break;
			case "settings_tab_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-tabs__list, .et-db #et-boc .et-fb-modal .et-fb-tabs__list:hover, .et-db #et-boc .et-l .et-fb-tabs__item:hover, .et-pb-options-tabs-links, .et-pb-options-tabs-links li a:hover, .et-pb-options-tabs-links, .et-pb-options-tabs-links li a:hover, .et-core-tabs, .et-core-tabs.ui-widget-header',
						'property' => 'background',
						'important' => 'true'
					)
				);
				break;
			case "settings_tab_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-modal__content .et-fb-tabs__item',
						'property' => 'color'
					)
				);
				break;
			case "settings_active_tab_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-tabs__item--active, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active:hover, .et-db #et-boc .et-l .et-fb-tabs__item--active:hover, .et-core-tabs li.ui-state-active a, .et-core-tabs a:hover, .et-pb-options-tabs-links li.et-pb-options-tabs-links-active a, .et-pb-options-tabs-links li a:hover',
						'property' => 'background',
						'important' => 'true'
					)
				);
				break;
			case "settings_active_tab_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-tabs__item--active',
						'property' => 'color'
					)
				);
				break;
			case "settings_discard_button_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--danger',
						'property' => 'background'
					)
				);
				break;
			case "settings_undo_button_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--primary-alt, a.et-pb-modal-save-template, a.et-pb-modal-save-template:hover',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "settings_redo_button_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--info, a.et-pb-modal-preview-template, a.et-pb-modal-preview-template:hover',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "settings_save_button_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--block.et-fb-button--success, .et-db #et-boc .et_fb_save_module_modal a.et-fb-save-library-button, a.et-pb-modal-save, a.et-pb-modal-save:hover, .et_pb_prompt_modal .et_pb_prompt_buttons input.et_pb_prompt_proceed, .et-db #et-boc #et-fb-app .et-fb-button--block.et-fb-button--success',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "settings_bottom_buttons_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal .et-fb-modal__footer .et-fb-icon svg',
						'property' => 'fill'
					)
				);
				break;
			case "insert_header_bg":
				return array(
					array(
						'selector'  => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading, .et-db #et-boc #et-fb-app .et-fb-modal-add-module-container .et-fb-settings-heading .et-fb-button, .et-db #et-boc .et-fb-modal-add-module-container.et-fb-modal-settings--inversed:after',
						'property'  => 'background',
						'important' => 'true'
					)
				);
				break;
			case "insert_header_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading',
						'property' => 'color'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading .et-fb-icon svg',
						'property' => 'fill'
					)
				);
				break;
			case "insert_tab_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav',
						'property' => 'background'
					)
				);
				break;
			case "insert_tab_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav-item a',
						'property' => 'color'
					)
				);
				break;
			case "insert_active_tab_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav-item--active a, .et-db #et-boc .et-fb-settings-tabs-nav-item:hover a',
						'property' => 'background'
					)
				);
				break;
			case "insert_active_tab_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav .et-fb-settings-tabs-nav-item--active a',
						'property' => 'color'
					)
				);
				break;
			/*  */
			case "warning_header_bg":
				return array(
					array(
						'selector' => '.et-builder-exit-modal .et-core-modal-header',
						'property' => 'background'
					)
				);
				break;
			case "warning_header_font":
				return array(
					array(
						'selector' => '.et-builder-exit-modal .et-core-modal-title, .et-builder-exit-modal .et-core-modal-close, .et-builder-exit-modal .et-core-modal-close:hover, .et-builder-exit-modal .et-core-modal-close:before',
						'property' => 'color'
					)
				);
				break;
			case "warning_discard_button_bg":
				return array(
					array(
						'selector' => '.et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action-secondary',
						'property' => 'background'
					)
				);
				break;
			case "warning_save_button_bg":
				return array(
					array(
						'selector' => '.et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action:not(.et-core-modal-action-secondary)',
						'property' => 'background'
					)
				);
				break;
			case "warning_bottom_buttons_font":
				return array(
					array(
						'selector' => '.et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action',
						'property' => 'color',
						'important' => true,
					)
				);
				break;
			case "save_draft_button_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft',
						'property' => 'background',
						'important' => true,
					)
				);
				break;
			case "save_draft_button_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft',
						'property' => 'color',
						'important' => true,
					)
				);
				break;
			case "publish_button_bg":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish',
						'property' => 'background',
						'important' => true,
					)
				);
				break;
			case "publish_button_font":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish',
						'property' => 'color',
						'important' => true,
					)
				);
				break;
			default:
				return array();
				break;
		}
	}

}

add_action( 'init', array( 'DiviGhosterCustomBuilder', 'setup' ) );
