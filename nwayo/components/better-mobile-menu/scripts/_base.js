//-------------------------------------
//-- Site - Base
//-------------------------------------

(() => {
	'use strict';

	const local = {};


	//-- Cache data instantly
	local.cache = () => {
		// Env
		// app.env.isUniquePage = app.env.pageId === 'UID';
		// app.env.isTypePage   = _.includes(app.env.pageTags, 'TYPE-ID');
		// app.env.isOneOfThese = !!_.intersection(app.env.pageTags, ['ID1', 'ID2']).length;

	};


	//-- Cache data once DOM is loaded
	local.cacheDOM = () => {

		//

	};


	//-- Bind events once DOM is loaded
	local.bind = () => {

		//

	};


	//-- Subscribe to topics
	local.subscribe = () => {
		// pinki.message.subscribe('foo.bar',  () => {});
	};


	//-- Execute once DOM is loaded
	local.start = () => {
		local.openFirstChildLink();
		local.addOpenSubmenuLinkToMobile();
		local.openSubMenuOnClick();
		local.removeBodyScrollOnMobile();
	};


	//-- Execute once page is loaded
	local.delayedStart = () => {
		//
	};

	local.addOpenSubmenuLinkToMobile = () => {
		const mobileMenu =  $('#mobile_menu');

		mobileMenu.find('li.menu-item-has-children > a').each(function() {
			$(this).after('<div class="mobile-open-submenu">+</div>');
		});

	};

	local.openFirstChildLink = () => {
		const menuLiItemHref = $('.gabicktofirstchild > a');
		menuLiItemHref.on('click', (event) => {
			event.preventDefault();
			const firstChildLink = $(event.currentTarget).parent().find('.sub-menu').first('li').find('a').attr('href');
			location.href = firstChildLink;
		});
	};

	local.openSubMenuOnClick = () => {
		$('.mobile-open-submenu').on('click', function() {
			const parent = $(this).closest('li');
			$(parent).toggleClass('visible');

		});
	};

	local.removeBodyScrollOnMobile = () => {
		$('.mobile_nav').on('click', function() {
			if ($(this).hasClass('opened')) {
				$('body').addClass('body-noscroll');
			} else {
				$('body').removeClass('body-noscroll');
			}
		});
	};

	// Outline
	local.cache();
	local.subscribe();

	// DOM Ready
	pinki.vow.when(DOM_PARSED).then(() => {
		local.cacheDOM();
		local.bind();
		local.start();
	});

	// Document loaded
	pinki.vow.when(DOCUMENT_LOADED).then(() => {
		local.delayedStart();
	});

})();
