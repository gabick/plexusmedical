<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'plexus-refonte' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'EA`h+rQ1rT+v=?kUu_?st,$#([1*sMn#WxA,Od<G3/8(-L+m#xZc]D5D~<>Wc:YQ' );
define( 'SECURE_AUTH_KEY',  '0;n&c?FXs$C-KgB#2JS,|ecb1A_CJ.C]xZezx0iX+F `Duj$Wv[v)}2V&A9,L<~G' );
define( 'LOGGED_IN_KEY',    'gKBT6Rp=c1%zSKD3,?|@y<4)Zfb 9W2{d#_.zfA/otXUnn$H%jOwx/=Q5~dv9Lr>' );
define( 'NONCE_KEY',        ']h@{Pf0J$FzbluLRI+B/*|xo+|KMP@*`3%8xQrPa$YBn@b-Fba5v^d3&v.F,etYi' );
define( 'AUTH_SALT',        'gJ3LbuwBx/[*N:$0D^}}IOb=$zp~VbcC+cpFo`JykW}|Q{%?<9]DYeciIz1x!N%e' );
define( 'SECURE_AUTH_SALT', '4ui.TUj7#5+jsy{M7&rQOVPx`Y QaV/Zx,~m/jonIv<TFoV4?dwI;EgoAR81Zd!6' );
define( 'LOGGED_IN_SALT',   '?&&&{_ jAM> w{,n&G`[wXQr^,GpGvXq(W4I+7mX<Dicp]IK_`2+E-gIps4K$0No' );
define( 'NONCE_SALT',       '+Gvo>P:/jp2R9Y&h%Q=pxpS.=J32NCl=%WO[}F#@7(ZH@RbQeX9RTz@Q2W|u_J@q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wplx_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
